# README
#### Fit+ | 25-July-2019 | Version: Release Candidate

## What is this repository for?

### App Summary
This repo is for the Fit+ App, a project by Marcos Felipe Costa da Silva and Tommy Olsen.

Fit+ is changing the world by changing its fitness. Fit+ makes tracking your health easier and more meaningful. By working seamlessly with Apple Health, Fit+ allows its users to see their health data in a way that they understand.

## How do I get set up?

### Minimum Software Requirements
- CocoaPods 1.7.2
- Xcode 10.2.1

### Minimum Device Requirements
- iPhone 6
- iPad Mini 2
- Apple Watch Series 1

### Clone the Project
After navigating to the desired location whithin terminal, clone the project from BitBucket.
``` 
git clone https://TJOlsenater2@bitbucket.org/mediumhotcoffee/gym-companion.git
```

### Update CocoaPods
Navigate to the project's 'gym-companion' folder within terminal and update/install CocoaPods.
```
pod install
```

### Duplicate Lib File
Installing the CocoaPods may create a duplicate lib file within the project. Open the Xcode workspace folder located in the gym-companion-app folder. Navigate to the project settings and go into the Watch Kit Extension target. Click on the "Build Phases" tab.

Under "Link Binary With Library," if there is a lib file, delete it. The file will have a '.a' extension.

### Bundle Identifiers
At this point, if you wish to test on a simulator, you should be able to build and run the project. However, to test on a physical device, the app's bundle identifier must be unique. You will also need to make sure you are deploying from your own team or developer account with a registered device.

So, once again in the project settings, navigate to the phone app target titled, "gym-companion-app." Under the "General" tab, select your own team that has a device registered to it. Change the Bundle Identifier to something unique, such as _com.fitplus.test-yourname_. Press 'return.'

Now navigate to the WatchKit App target. Under the "General" tab, select your own team that has a device registered to it. Change the Bundle Identifier to match exactly the phone target Bundle Identifier you just defined, but with a '.watchkitapp' extension. For example, _com.fitplus.test-yourname.watchkitapp_. Press 'return.'

Now, navigate to the WatchKit Extension targer. Under the "General" tab, select your own team that has a device registered to it. Change the Bundle Identifier to match exactly the WatchKit App target Bundle Identifier defined earlier, but with an added '.watchkitextension' extension. For example, _com.fitplus.test-yourname.watchkitapp.watchkitextension_. Press 'return.'

### Pointing Bundle Identifiers
The final step is to make sure our WatchKit App and WatchKit Extension are now pointing to the correct Bundle Identifiers. To do this, look at the folders on the left hand side of Xcode. Expand the folder titled, 'gym-companion-app WatchKit App'. Open the 'info.plist' file in this folder. Set the WKCompanionAppBundleIdentifier to match exactly the phone app Bundle Identifier created earler. For example, _com.fitplus.test-yourname_. Press 'return.'

Now, expand the folder titled, 'gym-companion-app WatchKit Extension.' Open the 'info.plist' file in this folder. Expand 'NSExtension.' Under 'NSExtension,' expand 'NSExtensionAttributes.' Under 'NSExtensionAttributes,' set the 'WKAppBundleIdentifier' to exactly the WatchKit App Bundle Identifier defined earlier. For example, _com.fitplus.test-yourname.watchkitapp_. Press 'return.'

**Congratulations!** You should now be able to run and test the app on a physical device.

### Developer Account
For more information about creating a developer account and registering devices, see [Create an Apple Developer Account](https://developer.apple.com/support/account/) and [Register a Device to an Apple Developer Account](https://help.apple.com/developer-account/#/dev40df0d9fa).

### Already Cloned The Project
If you have previously cloned the project and want to now pull the beta, pull the beta branch.

Create a local gold branch
```
git branch gold-release
```

Checkout your local gold branch
```
git checkout gold-release
```

Alternatively, you could create and checkout a local gold branch at the same time
```
git checkout -b gold-release
```

Pull the origin gold branch
```
git pull origin gold-release
```

You should now be able to run and test the app as normal.

## Client Pitch
[Pitch Video](https://youtu.be/dB4Im-_4pmA)

## Prototype
[iPad Video Walkthrough](https://youtu.be/Mw3tvYFhWTM)
[Apple Watch Video Walkthrough](https://youtu.be/vsNeJ-yNrj0)
[iPhone Video Walkthrough](https://youtu.be/hrgwq3QWzCc)

## Alpha 
Create, edit, and delete routines on iPad and iPhone. Account management on iPad and iPhone.

[Video Walkthrough](https://youtu.be/b0_TUJ9vlz4)

## Beta 
Features added in the beta version

- Gym Bags
- Select icon for gym bag item
- Watch timers
- Phoe timers
- HealthKit compatibility

[Video Walkthrough](https://youtu.be/lLS38lYgdpI)
[Video Walkthrough 2](https://youtu.be/sRcwrOdKqeM)

## Release Candidate
Features added in the release candidate version

- Trainee and Trainer connection
- Pay to unlock app
- Gym attendance
- More workout details
- Browsing features
- Watch HIIT timer
- Other timers now functioning
- Notification reminder to go to the gym
- Push routines from trainer to trainee

[Video Walkthrough](https://youtu.be/5eboyHuItQw)

## Gold Release
Features added to the gold release

- Icon images show up in list to make selection easier
- Tablet UI added for all screens
- Can browse exercises and gym bag items
- Routines list and gym bags list now show icon images as well
- Rest time for routines created on the phone can now be timed on the watch
- Profile image bugs have been fixed
- Portuguese is now supported
- HIIT timer results
- Settings screen
- Gym attendance
- Several UI/UX enhancements

[Promotional Video](https://youtu.be/e-fmxUqTb-A)
[Video Walkthrough](https://youtu.be/uqVf_CevyT0)

## Contribution guidelines

Coming later
- Writing tests
- Code review
- Other guidelines

## Who do I talk to?

- Repo owners/admins: Marcos Silva and Tommy Olsen
    - mcostadasilva@student.fullsail.edu
    - tjolsenater@gmail.com
