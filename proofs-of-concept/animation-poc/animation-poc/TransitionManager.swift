//
//  TransitionManager.swift
//  animation-poc
//
//  Created by Marcos Silva on 6/11/19.
//  Copyright © 2019 Marcos Silva. All rights reserved.
//

import UIKit

class TransitionManager: NSObject, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {
    
    //MARK: - UIViewControllerAnimatedTransitioning protocol methods
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        //Performs the animation of the transition
        
        //Get reference to our fromView, toView, and the container view that
        //we should perform the transition in
        let container = transitionContext.containerView
        let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from)!
        let toView = transitionContext.view(forKey: UITransitionContextViewKey.to)!
        
        //Set up from 2D transforms that we'll use in the animation
        let offScreenRight = CGAffineTransform(translationX: container.frame.width, y: 0)
        let offScreenLeft = CGAffineTransform(translationX: -container.frame.width, y: 0)
        
        //Start the toView to the right of the screen
        toView.transform = offScreenRight
        
        //Add both views to our view controller
        container.addSubview(toView)
        container.addSubview(fromView)
        
        //Get the duration of the animation
        let duration = self.transitionDuration(using: transitionContext)
        
        //Perform the animation. For this example, just slid both fromView and
        //toView to the left at the same time meaning fromView is pushed off the
        //screen and toView slides into view        
        UIView.animate(withDuration: duration, animations: {
            fromView.transform = offScreenLeft
            toView.transform = .identity
        }) { (finished) in
            //Tell our transitionContext object that we've finished animating
            transitionContext.completeTransition(true)
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        //Returns the duration of the transition
        return 0.5
    }
    
    //MARK: - UIViewControllerTransitioningDelegate protocol methods
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        //Return the animator when presenting a ViewController
        //Remember that an animator (or animation controller) is any object that
        //adheres to the UIViewControllerAnimatedTransitioning protocol
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        //Return the animator used when dismissing from a ViewController
        return self
    }
    
}
