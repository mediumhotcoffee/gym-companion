//
//  TimerIC.swift
//  RunningMan WatchKit Extension
//
//  Created by Marcos Silva on 7/13/19.
//  Copyright © 2019 Marcos Silva. All rights reserved.
//

import WatchKit
import Foundation


class TimerIC: WKInterfaceController {

    //MARK: - Outlets
    @IBOutlet private weak var countdownTimer: WKInterfaceTimer!
    @IBOutlet private weak var caloriesLabel: WKInterfaceLabel!
    @IBOutlet private weak var playButton: WKInterfaceButton!
    @IBOutlet private weak var pauseButton: WKInterfaceButton!
    
    //MARK: - Variables
    private var isPaused = false
    private var endTime: Date!
    private var timePointPaused: Date?
    private var calorieTimer: Timer!
    private var calories = 0.0
    private var calorieBurnRate = 0.2
    
    //MARK: - Lifecycle methods
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        // Configure interface objects here.
        
        if let runTime = context as? Int {
            print(runTime)
            
            endTime = Date(timeIntervalSinceNow: TimeInterval(runTime * 60))
            countdownTimer.setDate(endTime)
            countdownTimer.start()
        }
        
        playButton.setAlpha(0.4)
        
        startCalorieTimer()
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    //MARK: - Actions
    @IBAction func playButtonTapped() {
        if isPaused {
            countdownTimer.start()
            playButton.setAlpha(0.4)
            pauseButton.setAlpha(1.0)
            isPaused = false
            
            //Increase end time
            if let timePaused = timePointPaused {
                let durationPaused = Date().timeIntervalSince(timePaused)
                endTime = Date(timeInterval: durationPaused, since: endTime)
                countdownTimer.setDate(endTime)
                countdownTimer.start()
                startCalorieTimer()
            }
        }
    }
    
    @IBAction func pauseButtonTapped() {
        if !isPaused {
            countdownTimer.stop()
            calorieTimer.invalidate()
            playButton.setAlpha(1.0)
            pauseButton.setAlpha(0.4)
            isPaused = true
            timePointPaused = Date()
        }
    }
    
    @IBAction func stopButtonTapped() {
        popToRootController()
    }
    
    //MARK: - Custom methods
    @objc func updateCalories() {
        calories += calorieBurnRate
        caloriesLabel.setText("Calories: \(Int(calories))")
    }
    
    func startCalorieTimer() {
        calorieTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCalories), userInfo: nil, repeats: true)
    }
    
}
