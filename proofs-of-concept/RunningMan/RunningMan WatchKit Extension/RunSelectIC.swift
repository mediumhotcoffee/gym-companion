//
//  InterfaceController.swift
//  RunningMan WatchKit Extension
//
//  Created by Marcos Silva on 7/13/19.
//  Copyright © 2019 Marcos Silva. All rights reserved.
//

import WatchKit
import Foundation


class RunSelectIC: WKInterfaceController {

    //MARK: - Outlets
    @IBOutlet weak var startButton: WKInterfaceButton!
    @IBOutlet weak var slider: WKInterfaceSlider!
    
    //MARK: - Variables
    var selectedRunTime = 5
    
    //MARK: - Lifecycle methods
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        //Like like viewDidLoad
        // Configure interface objects here.
        
    }
    
    override func willActivate() {
        //Like iOS viewWillAppear
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didAppear() {
        //Like iOS viewDidAppear
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    //MARK: - Actions
    @IBAction func runTimeChanged(_ value: Float) {
        selectedRunTime = Int(value)
        startButton.setTitle("Start \(selectedRunTime) min run")
    }
    
    //MARK: - Navigation
    override func contextForSegue(withIdentifier segueIdentifier: String) -> Any? {
        if segueIdentifier == "toTimer" {
            return selectedRunTime
        } else {
            print("\n\nERROR on segue\n\n")
        }
        
        return nil
    }
    
}
