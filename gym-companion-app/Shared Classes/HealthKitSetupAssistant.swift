//
//  HealthKitSetupAssistant.swift
//  HRMonitor2
//
//  Created by Marcos Felipe Costa da Silva on 3/18/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation
import HealthKit

class HealthKitSetupAssistant: NSObject {
    
    //MARK: - Properties
    //Class that helps us setup HealthKit authorization
    let healthKitStore = HKHealthStore()
    
    //MARK: - Initializers
    //Singleton pattern to prevent the creation of another instance of this
    //class (not really Swifty, but useful in this case)
    static let sharedInstance = HealthKitSetupAssistant()
    private override init() { }
    
    //MARK: - Custom Methods
    func authorizeHealthKit(_ completion: @escaping ((_ success: Bool, _ error: Error?) -> Void)) {
        
        //Check if HealthKit is available on the device
        guard HKHealthStore.isHealthDataAvailable() else {
            completion(false, HealthKitSetupError.notAvailableOnDevice)
            
            //TODO: Show this error to the user
            
            return
        }
        
        //Prepare HK data types that will be used
        //We will get the date of birth to calculate the optimal heart rate for
        //the user later
        guard let dateOfBirth = HKObjectType.characteristicType(forIdentifier: .dateOfBirth),
            let height = HKObjectType.quantityType(forIdentifier: .height),
            let calories = HKObjectType.quantityType(forIdentifier: .activeEnergyBurned) else {
            //There is not much else we can do at this point
            //The data is simply not available
            completion(false, HealthKitSetupError.dataTypeNotAvailable)
            
            return
        }
        
        //Define the types we want to read and share in the app
        let typesToShare: Set = [
            HKSampleType.workoutType(), //Workouts
            HKQuantityType.quantityType(forIdentifier: .bodyFatPercentage)!, //Body Fat
            HKQuantityType.quantityType(forIdentifier: .bodyMass)!, //Weight
            HKQuantityType.quantityType(forIdentifier: .bodyMassIndex)! //BMI
        ]
        
        let typesToRead: Set = [
            .workoutType(), //Workouts
            dateOfBirth, //DOB
            HKQuantityType.quantityType(forIdentifier: .heartRate)!, //Heart Rate
            HKQuantityType.quantityType(forIdentifier: .bodyFatPercentage)!, //Body Fat
            HKQuantityType.quantityType(forIdentifier: .bodyMass)!, //Weight
            HKQuantityType.quantityType(forIdentifier: .bodyMassIndex)!, //BMI
            height, //Height
            calories //Calories
        ]
        
        //Request authorization
        HKHealthStore().requestAuthorization(toShare: typesToShare, read: typesToRead) { (success, error) in
            if success {
                print("HealthKitSetupAssistant: Authorization was successful")
            }
            
            //If error occurred in getting HealthKit authorization, print error
            if error != nil {
                print(
                    "HealthKitSetupAssistant:\n"
                        + "An error occurred while we were trying to get authorization.\n"
                        + "ERROR:\n"
                        + error.debugDescription
                )
            }
            
            completion(success, error)
        }
    }
    
}

//MARK: - Enums
private enum HealthKitSetupError: Error {
    //Errors that might occur during the setup of HealthKit
    case notAvailableOnDevice //E.g. the device is an iPad
    case dataTypeNotAvailable
}
