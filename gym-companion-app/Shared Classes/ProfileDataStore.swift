//
//  ProfileDataStore.swift
//  HRMonitor2
//
//  Created by Marcos Felipe Costa da Silva on 3/19/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation
import HealthKit

class ProfileDataStore {
    
    class func getAge() throws -> Int? {
        let healthKitStore = HKHealthStore()
        var age: Int
        
        do {
            let dateOfBirthComponent = try healthKitStore.dateOfBirthComponents()
            
            //Calculate DOB if possible
            let today: Date = Date()
            let calendar: Calendar = Calendar.current
            let todayDateComponents: DateComponents = calendar.dateComponents([.year], from: today)
            let thisYear: Int = todayDateComponents.year!
            
            age = thisYear - dateOfBirthComponent.year!
            
            return age
        } catch {
            //An error will be thrown if the user has not provided their age on
            //the Health app on their phone
            print("ProfileDataStore.getAge()\n"
                + "ERROR:\n"
                + error.localizedDescription)
        }
        
        return nil
    }
    
    class func loadWorkouts(completion: @escaping ([HKWorkout]?, Error?) -> Void) {
        //Get all workouts stored in HealthKit
        
        //Sort workouts by date and time
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        
        //Create query with custom predicates above
        let query = HKSampleQuery(
            sampleType: .workoutType(), //Any workout
            predicate: nil, //All dates
            limit: HKObjectQueryNoLimit, //Do not limit query results
            sortDescriptors: [sortDescriptor]) { (query, samples, error) in
                DispatchQueue.main.async {
                    guard let samples = samples as? [HKWorkout], error == nil else {
                        //Return nothing if there is an error
                        completion(nil, error)
                        return
                    }

                    completion(samples, nil)
                }
            }
        
        HKHealthStore().execute(query)
    }
    
}
