//
//  WatchRoutineUtil.swift
//  gym-companion-app WatchKit Extension
//
//  Created by Tommy Kent Olsen, Jr on 8/1/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

class WatchRoutineUtil {
    
    static func getJSONExerciseCount(exerciseJSON: [String:Any]) -> Int {
        
        if let elements = exerciseJSON["exercises"] as? [[String:Any]] {
            
            return elements.count
        }
        
        return 0
    }
}
