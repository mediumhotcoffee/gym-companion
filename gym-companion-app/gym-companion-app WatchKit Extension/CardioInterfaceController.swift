//
//  CardioInterfaceController.swift
//  gym-companion-app WatchKit Extension
//
//  Created by Marcos Silva on 7/13/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import WatchKit
import Foundation
import HealthKit

class CardioInterfaceController: WKInterfaceController {

    //MARK: - Views
    //Timer
    @IBOutlet weak var timer: WKInterfaceTimer!
    //Label
    @IBOutlet weak var heartRateLabel: WKInterfaceLabel!
    //Buttons
    @IBOutlet weak var startPauseWorkoutButton: WKInterfaceButton!
    @IBOutlet weak var stopWorkoutButton: WKInterfaceButton!
    
    //MARK: - HealthKit Variables
    //HealthKit
    let healthKitSetupAssistant: HealthKitSetupAssistant = HealthKitSetupAssistant.sharedInstance
    var healthStore: HKHealthStore!
    //Workout
    var workoutConfiguration: HKWorkoutConfiguration!
    var workoutSession: HKWorkoutSession!
    var workoutBuilder: HKLiveWorkoutBuilder!
    
    //MARK: - Other Variables
    //Age
    var userAge: Int?
    //Heart rate
    var maxHeartRate: Int?
    var heartRateLowerBound: Int?
    var heartRateUpperBound: Int?
    //Workout status
    var isWorkoutInProgress: Bool = false
    
    //MARK: - Lifecycle Methods
    override func awake(withContext context: Any?) {
        //Configure interface objects here
        super.awake(withContext: context)
        
        //Disable Start/Pause button unless HealthKit is authorized
        startPauseWorkoutButton.setEnabled(false)
        
        //Set the BPM label text
        heartRateLabel.setText("Start")
        
        //Request autohrization on the Apple Watch
        healthKitSetupAssistant.authorizeHealthKit { (success, error) in
            if success {
                //Enable Start/Pause button if we have HealthKit access
                self.startPauseWorkoutButton.setEnabled(true)
                
                //Set the base color for the heart rate indicator
                self.heartRateLabel.setTextColor(UIColor.green)
                
                //Get age and define bounds
                self.getAgeAndBounds()
            } else {
                //If we do not have HealthKit access, let the user know
                let okAction = WKAlertAction(title: "OK", style: .default, handler: {
                    //Handle OK action
                    self.dismiss()
                })
                let actions = [okAction]
                
                self.presentAlert(withTitle: "No access to HealthKit", message: "Please enable access to HealthKit to use the Cardio Tracker", preferredStyle: .actionSheet, actions: actions)
            }
        }
        
        //Setup the Workout Configuration
        workoutConfiguration = HKWorkoutConfiguration()
        //`other` just means that we don't know what activity the user is
        //performing
        workoutConfiguration?.activityType = .other
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    //MARK: - Actions
    @IBAction func stopButtonTapped() {
        //If the workout is paused, stop the workout
        if !isWorkoutInProgress {
            //End the workout session
            endWorkoutSession()
            
            //Go back to previous screen
            popToRootController()
        }
    }
    
    @IBAction func startPauseButtonTapped() {
        if isWorkoutInProgress {
            //If the workout is in progress, pause the workout
            startPauseWorkoutButton.setTitle("Continue")
            
            //Allow the user to stop the workout
            stopWorkoutButton.setEnabled(true)
            stopWorkoutButton.setAlpha(1.0)
            
            //Pause the workout session
            pauseWorkoutSession()
            
            //Stop the timer on the watch
            startTimer(false)
        } else {
            //If the workout paused, start workout
            startPauseWorkoutButton.setTitle("Pause")
            
            //Block the user from stopping the workout
            stopWorkoutButton.setEnabled(false)
            stopWorkoutButton.setAlpha(0.5)
            
            //Start session and builder
            startWorkoutSessionAndBuilder()
            
            //Start the timer on the watch
            startTimer(true)
        }
        
        //Flip the state of our tracker
        isWorkoutInProgress.toggle()
    }
    
    //MARK: - Workout methods
    func startWorkoutSessionAndBuilder() {
        //Creates a workout session
        do {
            //Instantiate the workout session and builder
            workoutSession = try HKWorkoutSession(
                healthStore: healthKitSetupAssistant.healthKitStore,
                configuration: workoutConfiguration
            )
            
            //Retrieve builder associated with the workout session
            workoutBuilder = workoutSession?.associatedWorkoutBuilder()
            
            //Setup delegates and data sources for session and builder
            workoutSession.delegate = self
            
            workoutBuilder.delegate = self
            workoutBuilder.dataSource = HKLiveWorkoutDataSource(
                healthStore: healthKitSetupAssistant.healthKitStore,
                workoutConfiguration: workoutConfiguration);
        } catch {
            //An invalid workout configuration will throw
        print("InterfaceController.startWorkoutSessionAndBuilder()\n"
                + "ERROR:\n"
                + error.localizedDescription)
            
            return
        }
        
        //Start the workout session
        let startDate = Date()
        workoutSession.startActivity(with: startDate)
        
        //Start collecting data about the workout session
        workoutBuilder.beginCollection(withStart: startDate) { (success, error) in
            if let error = error {
                print("InterfaceController.startWorkoutSessionAndBuilder()\n"
                    + "ERROR:\n"
                    + error.localizedDescription)
            }
        }
    }
    
    func pauseWorkoutSession() {
        //Pause the workout session
        
        //FIXME: Pausing has to stop the timer and keep it running later
        //Right now it resets the timer altogether
        
        workoutSession.pause()
    }
    
    func endWorkoutSession() {
        //End the workout session and stop the builder
        
        //End the session
        workoutSession.end()
        
        //Stop collecting data from the builder
        workoutBuilder.endCollection(withEnd: Date()) { (success, error) in
            //Save the workout
            self.workoutBuilder.finishWorkout(completion: { (workout, error) in
                if let error = error {
                    print("InterfaceController.endWorkoutSession()\n"
                        + "ERROR:\n"
                        + error.localizedDescription)
                }
            })
        }
    }
    
    //MARK: - HealthKit methods (not related to workouts)
    private func getAgeAndBounds() {
        //Retrieve user's age from HealthKit
        do {
            userAge = try ProfileDataStore.getAge()
        } catch {
            print("InterfaceController.getAge()\n"
                + "ERROR:\n"
                + error.localizedDescription)
        }
        
        //Define lower and upper bounds
        if let userAge = userAge {
            //get max heart rate
            maxHeartRate = Int(208 - (Double(userAge) * 0.7))
            
            //Define upper and lower hear rate bounds
            heartRateLowerBound = Int(Double(maxHeartRate!) * 0.6)
            heartRateUpperBound = Int(Double(maxHeartRate!) * 0.85)
        }
    }
    
    //MARK: - UI methods
    func updateHeartRateLabel(to hearRate: String) {
        //Updates the heart rate label
        heartRateLabel.setText(hearRate)
    }
    
    //MARK: - Track workout time
    func startTimer(_ yes: Bool) {
        //Starts and stop the timer on the screen
        
        //Reset the timer's time
        self.timer.setDate(Date())
        
        if yes {
            DispatchQueue.main.async {
                self.timer.start()
            }
        } else {
            DispatchQueue.main.async {
                //In any other case, stop the timer
                self.timer.stop()
            }
        }
    }
    
}

//MARK: - Extensions
extension CardioInterfaceController: HKWorkoutSessionDelegate {
    
    func workoutSession(_ workoutSession: HKWorkoutSession, didChangeTo toState: HKWorkoutSessionState, from fromState: HKWorkoutSessionState, date: Date) {
        //Manage changes in the state of the workout session
        switch toState {
        case .prepared:
            print("Workout prepared")
        case .running:
            print("Workout started")
        case .stopped:
            print("Workout stopped")
        case .paused:
            print("Workout paused")
        case .ended:
            print("Workout ended")
        case .notStarted:
            print("Workout has not started yet")
        @unknown default:
            //Handle future cases
            print("ERROR: New unhandled workout session state")
        }
    }
    
    func workoutSession(_ workoutSession: HKWorkoutSession, didFailWithError error: Error) {
        //Manage failures
        print("Workout session failed with error:\n"
            + "ERROR:\n"
            + error.localizedDescription)
    }
    
}

extension CardioInterfaceController: HKLiveWorkoutBuilderDelegate {
    
    func workoutBuilder(_ workoutBuilder: HKLiveWorkoutBuilder, didCollectDataOf collectedTypes: Set<HKSampleType>) {
        //Every time samples are collected by the builder this method is called
        for type in collectedTypes {
            //Get the quantity samples
            guard let quantityType = type as? HKQuantityType else {
                return
            }
            
            let statistics = workoutBuilder.statistics(for: quantityType)?.mostRecentQuantity()
            
            let heartRateUnit = HKUnit(from: "count/min")
            let heartRate = Int(statistics?.doubleValue(for: heartRateUnit) ?? 0)
            let heartRateString = String(heartRate) + " bpm"
            
            //Define the color of the heart rate label
            if let upperBound = heartRateUpperBound, let lowerBound = heartRateLowerBound {
                if heartRate > upperBound {
                    heartRateLabel.setTextColor(.red)
                } else if heartRate < lowerBound {
                    heartRateLabel.setTextColor(.yellow)
                } else {
                    heartRateLabel.setTextColor(.green)
                }
            }
            
            //Update the UI with the heart rate
            updateHeartRateLabel(to: heartRateString)
        }
    }
    
    func workoutBuilderDidCollectEvent(_ workoutBuilder: HKLiveWorkoutBuilder) {
        //TODO: Find out what this does
    }
    
}
