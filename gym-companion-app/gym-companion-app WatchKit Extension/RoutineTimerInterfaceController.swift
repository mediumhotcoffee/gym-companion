//
//  RoutineTimerInterfaceController.swift
//  gym-companion-app WatchKit Extension
//
//  Created by Marcos Silva on 7/14/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import WatchKit
import Foundation

class RoutineTimerInterfaceController: WKInterfaceController {

    //MARK: - Outlets
    @IBOutlet weak var exerciseNameLabel: WKInterfaceLabel!
    @IBOutlet weak var setCountLabel: WKInterfaceLabel!
    @IBOutlet weak var screenTimer: WKInterfaceTimer!
    @IBOutlet weak var startRestButton: WKInterfaceButton!
    
    //MARK: - Variables
    private var endTime: Date!
    private var backgroundTimer: Timer!
    private var setRestTime: Int = 0
    private var remainingSeconds: Int = 0
    private var exerciseIndex: Int = 0
    
    private var exercises: [(name:String,reps:Int,rest:Int,sets:Int,currentSet:Int)] = []
    
    //MARK: - Lifecycle Methods
    override func awake(withContext context: Any?) {
        //Configure interface objects here
        super.awake(withContext: context)
        
        //Disable Start Rest button
        startRestButton.setEnabled(false)
        
        if let exerciseJson = context as? [String:Any], let exercises = exerciseJson["exercises"] as? [[String:Any]] {
            
            for exercise in exercises {
                
                guard let name = exercise["name"] as? String,
                    let reps = exercise["reps"] as? Int,
                    let rest = exercise["restTime"] as? Int,
                    let sets = exercise["sets"] as? Int
                else {return}
                
                self.exercises += [(name,reps,rest,sets,1)]
            }
        }
        
        if exercises.count > 0 {
            
            exerciseNameLabel.setText(exercises[0].name)
            
            //Calculate remaining seconds at start
            setRestTime = exercises[0].rest
            remainingSeconds = setRestTime
            
            //Set the amount of time that the timer should do
            endTime = Date(timeIntervalSinceNow: TimeInterval(setRestTime + 1))
            screenTimer.setDate(endTime)
            
            setCountLabel.setText("Set: \(exercises[0].currentSet) of \(exercises[exerciseIndex].sets)")
            
            exercises[0].currentSet += 1
            
            //Start timer to track screen timer
            backgroundTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateSetTimer), userInfo: nil, repeats: true)
            
            //Start the screen timers
            screenTimer.start()
        }
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        
        //Stop timers when leaving screen
        if let backgroundTimer = backgroundTimer {
            screenTimer.stop()
            backgroundTimer.invalidate()
        }
    }

    //MARK: - Actions
    @IBAction func startRestTapped() {
        //Disable start button for the duration of the timer
        startRestButton.setEnabled(false)
        
        //Start timer to track screen timer again
        backgroundTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateSetTimer), userInfo: nil, repeats: true)
        
        //Reset screen timer (timer always keeps running once it is set, so
        //this is necessary)
        endTime = Date(timeIntervalSinceNow: TimeInterval(setRestTime + 1))
        screenTimer.setDate(endTime)
        
        exercises[exerciseIndex].currentSet += 1
        
        //Start the screen timer again
        screenTimer.start()
    }
    
    //MARK: - Custom methods
    @objc func updateSetTimer(timer: Timer) {
        //Decrement remaining seconds
        remainingSeconds -= 1
        
        print(remainingSeconds)
        
        if remainingSeconds == 0 {
            //Give haptic feedback to user
            WKInterfaceDevice.current().play(.success)
            
            //Stop the internal timer if there is no remaining time
            timer.invalidate()
            
            //Stop the screen timer if there is no remaining time
            screenTimer.stop()
            
            if exerciseIndex < exercises.count - 1 {
                
                exerciseIndex += 1
            }
            else {
                
                exerciseIndex = 0
            }
            
            exerciseNameLabel.setText(exercises[exerciseIndex].name)
            
            setRestTime = exercises[exerciseIndex].rest
            
            //Reset screen timer
            endTime = Date(timeIntervalSinceNow: TimeInterval(setRestTime + 1))
            screenTimer.setDate(endTime)
            
            //Reset remaining seconds
            remainingSeconds = setRestTime
            
            //Increase set count
            setCountLabel.setText("Set: \(exercises[exerciseIndex].currentSet) of \(exercises[exerciseIndex].sets)")
            
            //Reenable start button
            startRestButton.setEnabled(true)
        }
    }
}
