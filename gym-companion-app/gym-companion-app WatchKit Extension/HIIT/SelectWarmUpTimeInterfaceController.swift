//
//  SelectWarmUpTimeInterfaceController.swift
//  gym-companion-app WatchKit Extension
//
//  Created by Marcos Silva on 7/24/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import WatchKit
import Foundation

class SelectWarmUpTimeInterfaceController: WKInterfaceController {

    //MARK: - Variables
    private var warmUpTime: Float? = 30.0
    private var hiitTimer: HiitTimer?
    
    //MARK: - Views
    //Timer
    @IBOutlet weak private var warmUpTimer: WKInterfaceTimer!
    //Slider
    @IBOutlet weak private var warmUpSlider: WKInterfaceSlider!
    
    //MARK: - Lifecycle Methods
    override func awake(withContext context: Any?) {
        //Configure interface objects here
        super.awake(withContext: context)
        
        //Setup default value for slider
        warmUpSlider.setValue(1)
        
        //Get data from previous screen
        if let hiitTimerReceived = context as? HiitTimer {
            hiitTimer = hiitTimerReceived
        }
    }

    override func willActivate() {
        //This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        //Set the timer to the default value
        if let warmUpTime = warmUpTime {
            warmUpTimer.setDate(Date(timeIntervalSinceNow: TimeInterval(warmUpTime + 1)))
        }
    }

    override func didDeactivate() {
        //This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    //MARK: - Actions
    @IBAction private func backTapped() {
        //Go back to previous screen
        pop()
    }
    
    @IBAction private func warmUpTimeChanged(_ value: Float) {
        //Handle changes to the slider
        hiitTimer?.warmUpTime = Int(value * 30)
        
        warmUpTimer.setDate(Date(timeIntervalSinceNow: TimeInterval(value * 30 + 1)))
    }
    
    //MARK: - Navigation
    override func contextForSegue(withIdentifier segueIdentifier: String) -> Any? {
        if segueIdentifier == "toSelectHiitCoolDownTime" {
            return hiitTimer
        }
        
        return nil
    }
    
}
