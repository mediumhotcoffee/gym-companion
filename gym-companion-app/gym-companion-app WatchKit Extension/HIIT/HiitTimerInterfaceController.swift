//
//  HiitTimerInterfaceController.swift
//  gym-companion-app WatchKit Extension
//
//  Created by Marcos Silva on 7/24/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import WatchKit
import Foundation


class HiitTimerInterfaceController: WKInterfaceController {

    //MARK: - Variables
    private var hiitTimerToStart: HiitTimer?
    private var timer: Timer?
    private var currentHiitSection: HiitSection = .warmUp
    //Times
    private var remainingTime: Int = 0
    private var warmUpTime: Int = 0
    private var highIntensityTime: Int = 0
    private var lowIntensityTime: Int = 0
    private var coolDownTime: Int = 0
    //Sessions
    private var numberOfHighIntensitySessionsToGo: Int = 0
    //Pause status
    private var shouldPause: Bool = true
    
    //MARK: - Views
    //Timer
    @IBOutlet weak var hiitScreenTimer: WKInterfaceTimer!
    //Button
    @IBOutlet weak var pauseStartButton: WKInterfaceButton!
    //Label
    @IBOutlet weak var sessionTypeLabel: WKInterfaceLabel!
    
    //MARK: - Lifecycle Methods
    override func awake(withContext context: Any?) {
        //Configure interface objects here
        super.awake(withContext: context)
        
        //Get data from previous screen
        if let hiitTimerReceived = context as? HiitTimer {
            //Get HIIT timer object containing all times and session numbers
            hiitTimerToStart = hiitTimerReceived
            
            //Set the amount of time that the timer should do for warm up
            if let hiitTimerToStart = hiitTimerToStart,
                let warmUpTime = hiitTimerToStart.warmUpTime,
                let highIntensityTime = hiitTimerToStart.highIntensityTime,
                let lowIntensityTime = hiitTimerToStart.lowIntensityTime,
                let coolDownTime = hiitTimerToStart.coolDownTime,
                let numberOfHighIntensitySessions = hiitTimerToStart.numberOfHighIntensitySessions {
                //Get times from HIIT timer object
                self.warmUpTime = warmUpTime
                self.highIntensityTime = highIntensityTime
                self.lowIntensityTime = lowIntensityTime
                self.coolDownTime = coolDownTime

                //Debug values
                //self.warmUpTime = 5
                //self.highIntensityTime = 5
                //self.lowIntensityTime = 5
                //self.coolDownTime = 5
                //remainingTime = 5
                
                //Get number of sessions from HIIT timer object
                self.numberOfHighIntensitySessionsToGo = numberOfHighIntensitySessions
                
                //Set remaining time
                remainingTime = warmUpTime
                
                hiitScreenTimer.setDate(Date(timeIntervalSinceNow: TimeInterval(remainingTime + 1)))
            }
            
            //Start timer to track screen timer
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateSetTimer), userInfo: nil, repeats: true)
        }
    }

    override func willActivate() {
        //This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        //This method is called when watch view controller is no longer visible
        super.didDeactivate()
        
        //Stop timer when leaving the screen
        timer?.invalidate()
        hiitScreenTimer.stop()
    }

    //MARK: - Actions
    @IBAction func endTapped() {
        //Go back to initial screen
        
        //Cancel action
        let cancelAction = WKAlertAction(title: "Cancel", style: .cancel) {}
        
        //End action
        let okAction = WKAlertAction(title: "End", style: .destructive) {
            self.popToRootController()
        }
        
        //Show alert
        presentAlert(withTitle: "End Timer", message: "Do you want to end the HIIT Timer", preferredStyle: .alert, actions: [cancelAction, okAction])
    }
    
    @IBAction func pauseStartTapped() {
        //Pause or resume the timer
        if shouldPause {
            //Pause the timer
            timer?.invalidate()
            pauseStartButton.setTitle("Start")
        } else {
            //Resume the timer
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateSetTimer), userInfo: nil, repeats: true)
            pauseStartButton.setTitle("Pause")
        }
        
        //Flip pause status
        shouldPause.toggle()
    }
    
    //MARK: - Custom methods
    @objc func updateSetTimer(timer: Timer) {
        //Decrement remaining seconds
        remainingTime -= 1
        hiitScreenTimer.setDate(Date(timeIntervalSinceNow: TimeInterval(remainingTime + 1)))
        
        //Session done
        if remainingTime == 0 {
            //Give haptic feedback to user
            WKInterfaceDevice.current().play(.success)
            
            //Stop the internal timer if there is no remaining time
            timer.invalidate()
            
            //Update UI
            shouldPause.toggle()
            pauseStartButton.setTitle("Start")
            
            //Go to next HIIT session
            switch currentHiitSection {
            case .warmUp:
                //On warm up section done
                
                //Jump to high intensity section
                updateSectionLabel(.highIntensity)
                currentHiitSection = .highIntensity
                remainingTime = highIntensityTime
                
                //Update time on screen
                hiitScreenTimer.setDate(Date(timeIntervalSinceNow: TimeInterval(remainingTime + 1)))
            case .highIntensity:
                //On high intensity section done
                
                //Update number of remaining high intensity sections
                numberOfHighIntensitySessionsToGo -= 1
                
                if numberOfHighIntensitySessionsToGo == 0 {
                    //Go to cool down
                    
                    //Jump to cool down section
                    updateSectionLabel(.coolDown)
                    currentHiitSection = .coolDown
                    remainingTime = coolDownTime
                    
                    //Update time on screen
                    hiitScreenTimer.setDate(Date(timeIntervalSinceNow: TimeInterval(remainingTime + 1)))
                } else {
                    //Go to low intensity section
                    
                    //Jump to low intensity section
                    updateSectionLabel(.lowIntensity)
                    currentHiitSection = .lowIntensity
                    remainingTime = lowIntensityTime
                    
                    //Update time on screen
                    hiitScreenTimer.setDate(Date(timeIntervalSinceNow: TimeInterval(remainingTime + 1)))
                }
            case .lowIntensity:
                //Go to high intensity section
                updateSectionLabel(.highIntensity)
                
                //Jump to high intensity section
                updateSectionLabel(.highIntensity)
                currentHiitSection = .highIntensity
                remainingTime = highIntensityTime
                
                //Update time on screen
                hiitScreenTimer.setDate(Date(timeIntervalSinceNow: TimeInterval(remainingTime + 1)))
            case .coolDown:
                //End timer and go to results
                
                //TODO: End the timer here showing the results to the user
                
                //End action
                let okAction = WKAlertAction(title: "Done", style: .default) {
                    self.popToRootController()
                }
                
                //Show alert
                presentAlert(withTitle: "Workout Finished", message: "Good job!", preferredStyle: .alert, actions: [okAction])
            }
        }
    }
    
    private func updateSectionLabel(_ section: HiitSection) {
        //Update the section on the label
        switch section {
        case .warmUp:
            sessionTypeLabel.setText("Warm Up")
        case .lowIntensity:
            sessionTypeLabel.setText("Low")
        case .highIntensity:
            sessionTypeLabel.setText("High")
        case .coolDown:
            sessionTypeLabel.setText("Cool Down")
        }
    }
    
}
