//
//  HiitTimer.swift
//  gym-companion-app WatchKit Extension
//
//  Created by Marcos Felipe Costa da Silva on 7/25/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

class HiitTimer {
    
    //MARK: - Properties
    //Times
    var coolDownTime: Int?
    var warmUpTime: Int?
    var lowIntensityTime: Int?
    var highIntensityTime: Int?
    //Sessions
    var numberOfHighIntensitySessions: Int?
    
    //MARK: - Initializers
    init(coolDownTime: Int?, warmUpTime: Int?, lowIntensityTime: Int?, highIntensityTime: Int?, numberOfHighIntensitySessions: Int?) {
        self.coolDownTime = coolDownTime
        self.warmUpTime = warmUpTime
        self.lowIntensityTime = lowIntensityTime
        self.highIntensityTime = highIntensityTime
        self.numberOfHighIntensitySessions = numberOfHighIntensitySessions
    }
    
}

//MARK: - Extensions
extension HiitTimer: CustomStringConvertible {
    
    var description: String {
        return "\nHigh Intensity Sessions: \(numberOfHighIntensitySessions ?? 0)"
        + "\nCool Down Time: \(coolDownTime ?? 0)"
        + "\nWarm Up Time: \(warmUpTime ?? 0)"
        + "\nLow Intensity Time: \(lowIntensityTime ?? 0)"
        + "\nHigh Intensity Time: \(highIntensityTime ?? 0)"
    }
    
}
