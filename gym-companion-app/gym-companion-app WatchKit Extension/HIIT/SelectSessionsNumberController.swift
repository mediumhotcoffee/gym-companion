//
//  HiitInterfaceController.swift
//  gym-companion-app WatchKit Extension
//
//  Created by Marcos Silva on 7/13/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import WatchKit
import Foundation

class SelectSessionsNumberController: WKInterfaceController {

    //MARK: - Variables
    private var hiitTimer: HiitTimer?
    private var numberOfHighIntensitySessions: Int?
    
    //MARK: - Views
    //Labels
    @IBOutlet weak var sessionNumbersLabel: WKInterfaceLabel!
    //Slider
    @IBOutlet weak var sessionsSlider: WKInterfaceSlider!
    
    //MARK: - Lifecycle Methods
    override func awake(withContext context: Any?) {
        //Configure interface objects here.
        super.awake(withContext: context)
    }

    override func willActivate() {
        //This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        //Set the slider to the default value
        sessionsSlider.setValue(2.0)
        
        //Set the time to the default value
        numberOfHighIntensitySessions = 2
        
        //Update the label
        adjustNumberOfSessionsLabel(highIntensitySessions: 2.0)
    }

    override func didAppear() {
        super.didAppear()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    //MARK: - Actions
    @IBAction func cancelTapped() {
        //Go back to previous screen
        self.popToRootController()
    }
    
    @IBAction func numberOfSessionChanged(_ value: Float) {
        adjustNumberOfSessionsLabel(highIntensitySessions: value)
    }
    
    //MARK: - Navigation
    override func contextForSegue(withIdentifier segueIdentifier: String) -> Any? {
        if segueIdentifier == "toSelectHiitWarmUpTime" {
            hiitTimer = HiitTimer(coolDownTime: 30, warmUpTime: 30, lowIntensityTime: 30, highIntensityTime: 30, numberOfHighIntensitySessions: numberOfHighIntensitySessions)
            
            return hiitTimer
        }
        
        return nil
    }
    
    //MARK: - Custom Methods
    func adjustNumberOfSessionsLabel(highIntensitySessions: Float) {
        numberOfHighIntensitySessions = Int(highIntensitySessions)
        
        if let num = numberOfHighIntensitySessions {
            sessionNumbersLabel.setText("\(num)Hi | \(num - 1)Lo")
        }
    }
    
}
