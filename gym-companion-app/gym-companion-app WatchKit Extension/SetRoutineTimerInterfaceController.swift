//
//  RoutineInterfaceController.swift
//  gym-companion-app WatchKit Extension
//
//  Created by Marcos Silva on 7/13/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class SetRoutineTimerInterfaceController: WKInterfaceController {
    
    fileprivate let session: WCSession? = WCSession.isSupported() ? WCSession.default : nil

    //MARK: - Outlets
    @IBOutlet weak var selectLabel: WKInterfaceLabel!
    @IBOutlet weak var routinePicker: WKInterfacePicker!
    @IBOutlet weak var startButton: WKInterfaceButton!
    
    //MARK: - Variables
    var selectedRoutine: [String:Any]?
    private var routinesJSON: [String:Any]?
    var items: [WKPickerItem] = []
    
    //MARK: - Lifecycle Methods
    override func awake(withContext context: Any?) {
        //Configure interface objects here
        super.awake(withContext: context)
        startButton.setEnabled(false)
        selectLabel.setText("Loading...")
        routinePicker.focus()
        isTableScrollingHapticFeedbackEnabled = true
    }
    
    override init() {
        super.init()
        
        setUpSession()
    }

    override func willActivate() {
        //This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        //This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
}

extension SetRoutineTimerInterfaceController {
    
    //MARK: Custom Methods
    private func getRoutines() {
        
        let getRoutines: String = "getRoutines"
        let newRoutines: String = "newRoutines"
        
        let getRoutinesMessage: [String:Bool] = [getRoutines:true]
        
        if let session = session, session.isReachable {
            
            session.sendMessage(getRoutinesMessage, replyHandler: {
                replyData in
                
                DispatchQueue.main.async {
                    
                    if let data = replyData[newRoutines] as? Data {
                        
                        do {
                            
                            if let routinesJSON = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [String:Any] {
                                
                                self.routinesJSON = routinesJSON
                                
                                self.setPickerItems()
                                self.selectLabel.setText("Select Routine")
                                self.startButton.setEnabled(true)
                            }
                            else {
                                print("Error: Something went wrong. getPlayers(), InterfaceController.")
                            }
                        }
                        catch {
                            print("Couldn't unarchive players. getPlayers(), InterfaceController.\r\n\(error.localizedDescription)")
                        }
                    } // End if let data = replyData[]
                    else {
                        self.selectLabel.setText("No Routines")
                    }
                } // End DispatchQueue
            }) // End session.sendMessage()
        } // End if let session = session, session.isReachable
        else {
            selectLabel.setText("No Routines")
        }
    } // End func getRoutines()
    
    private func setPickerItems()
    {
        guard let json = routinesJSON,
            let routines = json["routines"] as? [String:Any]
        else { return}
        
        for (key, value) in routines {
            
            let item: WKPickerItem = WKPickerItem()
            item.title = key
            items += [item]
            
            if let json = value as? [String:Any] {
                
                item.caption = "\(WatchRoutineUtil.getJSONExerciseCount(exerciseJSON: json)) Exercises"
            }
        }
        items.sort(by: { $0.title! < $1.title! })
        routinePicker.setItems(items)
        pickerItemPicked(index: 0)
    }
    
    // Sets up the session.
    private func setUpSession()
    {
        
        session?.delegate = self
        session?.activate()
        print("Sesion delegate set and Session active.")
        
        if let session = session, session.isReachable {
            getRoutines()
        }
    }
}

extension SetRoutineTimerInterfaceController {
    
    @IBAction func pickerItemPicked(index: Int) {
        
        if let json = routinesJSON, let routines = json["routines"] as? [String:Any], let selectedTitle = items[index].title, let selectedJSON = routines[selectedTitle] as? [String:Any] {
            
            selectedRoutine = selectedJSON
        }
    }
}

extension SetRoutineTimerInterfaceController {
    
    //MARK: - Navigation
    override func contextForSegue(withIdentifier segueIdentifier: String) -> Any? {
        if segueIdentifier == "toRoutineTimer" {
            
            return selectedRoutine
        }
        
        return nil
    }
}

extension SetRoutineTimerInterfaceController: WCSessionDelegate {
    
    // When the session is activated, gets the heroes.
    @available(watchOS 2.2, *)
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?)
    {
        
        getRoutines()
    }
}
