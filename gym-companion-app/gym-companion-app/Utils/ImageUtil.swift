//
//  ImageUtil.swift
//  gym-companion-app
//
//  Created by Marcos Felipe Costa da Silva on 7/22/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation
import UIKit
import FirebaseStorage
import FirebaseAuth

class ImageUtil {
    
    static func resize(_ image: UIImage, withWidth: Float = 400.0, withHeight: Float = 400.0) -> UIImage {
        //Resize image to 400x400 pixels by default
        var actualHeight = Float(image.size.height)
        var actualWidth = Float(image.size.width)
        let maxHeight: Float = withHeight
        let maxWidth: Float = withWidth
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compressionQuality: Float = 0.5
        
        //Add 50% compression
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //Adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            } else if imgRatio > maxRatio {
                //Adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            } else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        
        let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img?.jpegData(compressionQuality: CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        
        return UIImage(data: imageData!) ?? UIImage()
    }
    
    static func getNewImageForAccount(from viewController: UIViewController, sender: UIButton) {
        //Get a picture for the account from either the camera or the gallery
        guard let viewController = viewController as? UIViewController & UIImagePickerControllerDelegate & UINavigationControllerDelegate else {
            print("ERROR: VC was not compatible")
            return
        }
        
        let alert = UIAlertController(title: "Add Picture",
                                      message: "Would you like to take a selfie or select an existing picture?",
                                      preferredStyle: .actionSheet)
        
        //Open camera action
        alert.addAction(UIAlertAction(title: "Take Selfie",
                                      style: .default,
                                      handler: { (action) -> Void in
                                        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                                            let imagePickerController = UIImagePickerController()
                                            imagePickerController.delegate = viewController
                                            
                                            //Setup camera
                                            imagePickerController.sourceType = .camera
                                            imagePickerController.cameraCaptureMode = .photo
                                            imagePickerController.cameraFlashMode = .off
                                            imagePickerController.allowsEditing = true
                                            
                                            //Show camera
                                            viewController.present(imagePickerController, animated: true)
                                        }
        }))
        
        //Open gallery action
        alert.addAction(UIAlertAction(title: "Choose Existing",
                                      style: .default,
                                      handler: { (action) -> Void in
                                        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
                                            let imagePicker = UIImagePickerController()
                                            imagePicker.delegate = viewController
                                            
                                            //Setup gallery
                                            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                                            imagePicker.allowsEditing = true
                                            
                                            //Show gallery
                                            viewController.present(imagePicker, animated: true)
                                        }
        }))
        
        //Cancel button
        alert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""),
                                      style: .cancel))
        
        if let popoverController = alert.popoverPresentationController {
            
            popoverController.sourceView = sender
            popoverController.sourceRect = sender.bounds
        }

        
        //Show alert
        viewController.present(alert, animated: true)
    }
    
    //MARK: - Firebase Operations
    static func saveImageToFirebaseStorage(storageReference: StorageReference, picture: UIImage?, userID: String) {
        //Get, reduce, and compress image
        guard let jpegDataToSave: Data = ImageUtil.resize(picture!).jpegData(compressionQuality: 0.5) else {
            return
        }
        
        //Save image to Firebase Storage
        let userPictureReference = storageReference.child("user-pictures/\(userID).jpg")
        
        //Upload the file to the Firebase Storage path
        userPictureReference.putData(jpegDataToSave, metadata: nil) { (metadata, error) in
            guard let metadata = metadata else {
                //TODO: Handle error
                return
            }
        }
    }
    
    static func deleteImageOnFirebaseStorage(storageReference: StorageReference, button: UIButton, userId: String) {
        //Deletes a picture from the database
        
        //Create a Firebase Storage reference to the file we want to delete
        let userPictureReference = storageReference.child("user-pictures/\(userId).jpg")
        
        //Download in memory with a maximum allowed size of 1MB
        //(1 * 1024 * 1024 bytes)
        userPictureReference.delete(completion: { (error) in
            if let error = error {
                //An error occurred when deleting image from Firebase Storage
                print(error.localizedDescription)
            }
            
            //Reset image shown to the user
            button.setImage(UIImage(named: "profilePic"), for: .normal)
        })
    }
    
    //MARK: - Load on Views
    static func loadUserPictureOn(button: UIButton, userId: String) {
        //Check whether a user is logged in and display the appropriate profile icon
        
        //Create a Firebase Storage reference to the file we want to download
        let pictureReference = Storage.storage().reference(withPath: "user-pictures/\(userId).jpg")
        
        //Download in memory with a maximum allowed size of 1MB
        //(1 * 1024 * 1024 bytes)
        pictureReference.getData(maxSize: 1 * 1024 * 1024) { (data, error) in
            if let error = error {
                //An error occurred when getting image from Firebase Storage
                button.setImage(UIImage(named: "noAccountIcon"), for: .normal)
                print(error.localizedDescription)
            } else {
                // Data for "images/island.jpg" is returned
                let userPicture = UIImage(data: data!)
                
                //Display picture to the user
                button.contentMode = .scaleToFill
                button.setImage(userPicture, for: .normal)
                button.layer.cornerRadius = button.frame.size.width / 2
                button.clipsToBounds = true
            }
        }
    }
    
    static func loadUserPictureOn(imageView: UIImageView, userId: String) {
        //Check whether a user is logged in and display the appropriate profile icon
        
        //Create a Firebase Storage reference to the file we want to download
        let pictureReference = Storage.storage().reference(withPath: "user-pictures/\(userId).jpg")
        
        //Download in memory with a maximum allowed size of 1MB
        //(1 * 1024 * 1024 bytes)
        pictureReference.getData(maxSize: 1 * 1024 * 1024) { (data, error) in
            if let error = error {
                //An error occurred when getting image from Firebase Storage
                imageView.image = nil
                imageView.backgroundColor = .clear
                print(error.localizedDescription)
            } else {
                //Data is returned
                
                //Display picture to the user
                imageView.isHidden = false
                imageView.contentMode = .scaleToFill
                imageView.image = UIImage(data: data!)
                imageView.layer.cornerRadius = imageView.frame.size.width / 2
                imageView.clipsToBounds = true
            }
        }
    }
    
}
