//
//  ValidationHelper.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 6/30/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

class ValidationHelper {
    
    static func textFieldIsNotEmpty(_ sender: UITextField, setIcon: Bool) -> Bool {
        
        guard let textViewText = sender.text, !textViewText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty
            else {
            
            sender.text = sender.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                
                if setIcon {
                    
                    sender.setIcon(UIImage(named: "exclamationIcon"))
                }
            
            return false
        }
        
        if setIcon {
            
            sender.leftViewMode = .never
        }
        
        return true
    }
    
    static func textFieldInputIsInt(_ sender: UITextField, setIcon: Bool, maxDigits: Int) -> Bool {
        
        guard let textViewText = sender.text, Int(textViewText) != nil, textViewText.count < maxDigits + 1
            else {
                
                sender.text = sender.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                
                if let tooManyDigits = sender.text, tooManyDigits.count > maxDigits {
                    
                    sender.text = String(tooManyDigits.prefix(maxDigits))
                }
                
                if setIcon {
                    
                    sender.setIcon(UIImage(named: "exclamationIcon"))
                }
                
                return false
        }
        
        if setIcon {
            
            sender.leftViewMode = .never
        }
        
        return true
    }
    
    static func buttonText(isNot text: String, for button: UIButton) -> Bool {
        
        if button.titleLabel?.text == text {
            
            return false
        }
        
        return true
    }
    
    static func emphasizeInvalidButton(button: UIButton) {
        
        UIView.transition(with: button, duration: 1.5, options: .transitionCrossDissolve, animations: { button.isHighlighted = true }) { (isHighlighted) in
            
            removeInvalidButtonEmphasis(button: button)
        }
    }
    
    static func removeInvalidButtonEmphasis(button: UIButton) {
        
        UIView.transition(with: button, duration: 1.0, options: .transitionCrossDissolve, animations: { button.isHighlighted = false })
    }
}
