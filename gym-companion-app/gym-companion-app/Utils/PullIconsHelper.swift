//
//  PullIconsHelper.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/15/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseStorage

class PullIconsHelper {
    
    static func pullIconLocations(from folder: String, delegate: PullIconsDelegate, listType: ListType) {
        
        let ref: DatabaseReference = Database.database().reference()
        
        let bagItemIconsReference: DatabaseReference = ref.child("icons").child(listType.stringValue)
        
        bagItemIconsReference.observeSingleEvent(of: .value) {
            (iconsSnapShot) in
            
            if let iconsArray = iconsSnapShot.value as? [String] {
                
                delegate.iconLocationsPulled(iconLocations: iconsArray)
            }
        }
    }
    
    static func pullIconImages(iconLocations: [String], storageReference: StorageReference, delegate: PullIconsDelegate) {
        
        for location in iconLocations {
            
            let imageReference: StorageReference = storageReference.child(location)
            
            imageReference.getData(maxSize: 4 * 1024 * 1024) {
                (data, error) in
                
                if let error = error {
                    
                    print("Error pulling image: \(error.localizedDescription)")
                    return
                }
                
                if let data = data, let image = UIImage(data: data) {
                    
                    delegate.iconImagesPulled((key: location, image: image))
                }
            }
        }
    }
    
    static func pullSingleIconImage(location: String, storageReference: StorageReference, delegate: PullIconsDelegate) {
        
        let imageReference: StorageReference = storageReference.child(location)
        
        imageReference.getData(maxSize: 4 * 1024 * 1024) {
            (data, error) in
            
            if let error = error {
                
                print("Error pulling image: \(error.localizedDescription)")
                return
            }
            
            if let data = data, let image = UIImage(data: data) {
                
                delegate.singleIconImagePulled(iconImage: image)
            }
        }
    }
}
