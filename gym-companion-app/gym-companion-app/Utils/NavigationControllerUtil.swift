//
//  NavigationControllerUtil.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 6/24/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

class NavigationControllerUtil: NSObject {
    
    var currentViewController: UIViewController?
    
    static func setUpLargeTitle(largeTitleImageView: UIImageView, navigationController: UINavigationController?, navigationItem: UINavigationItem) {
        
        // Make transparent background for navigation controller.
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        // Makes large text white.
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        //Hide the back button
        navigationItem.hidesBackButton = true
        
        // Initial setup for image for Large NavBar state since the the screen always has Large NavBar once it gets opened.
        // This adds the profile image to the NavBar.
        guard let navigationBar = navigationController?.navigationBar else { return }
        navigationBar.addSubview(largeTitleImageView)
        largeTitleImageView.layer.cornerRadius = Const.ImageSizeForLargeState / 2
        largeTitleImageView.clipsToBounds = true
        largeTitleImageView.translatesAutoresizingMaskIntoConstraints = false
        
        //Setup constraints
        NSLayoutConstraint.activate([
            largeTitleImageView.rightAnchor.constraint(equalTo: navigationBar.rightAnchor, constant: -Const.ImageRightMargin),
            largeTitleImageView.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: -Const.ImageBottomMarginForLargeState),
            largeTitleImageView.heightAnchor.constraint(equalToConstant: Const.ImageSizeForLargeState),
            largeTitleImageView.widthAnchor.constraint(equalTo: largeTitleImageView.heightAnchor)
            ])
    }
    
    // WARNING: These constraints may need to be changed
    private struct Const {
        // Image height/width for Large NavBar state
        static let ImageSizeForLargeState: CGFloat = 50
        // Margin from right anchor of safe area to right anchor of Image
        static let ImageRightMargin: CGFloat = 16
        // Margin from bottom anchor of NavBar to bottom anchor of Image for Large NavBar state
        static let ImageBottomMarginForLargeState: CGFloat = 12
        // Margin from bottom anchor of NavBar to bottom anchor of Image for Small NavBar state
        static let ImageBottomMarginForSmallState: CGFloat = 6
        // Image height/width for Small NavBar state
        static let ImageSizeForSmallState: CGFloat = 32
        // Height of NavBar for Small state. Usually it's just 44
        static let NavBarHeightSmallState: CGFloat = 44
        // Height of NavBar for Large state. Usually it's just 96.5 but if you have a custom font for the title, please make sure to edit this value since it changes the height for Large state of NavBar
        static let NavBarHeightLargeState: CGFloat = 96.5
    }
    
}
