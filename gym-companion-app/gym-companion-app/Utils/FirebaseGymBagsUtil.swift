//
//  FirebaseGymBagsUtil.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/12/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

class FirebaseGymBagsUtil {
    
    static func pushNewGymBagToUser(userID: String, gymBag: GymBag, delegate: ShareObjectDelegate) {
        
        let ref: DatabaseReference = Database.database().reference()
        let usersRef: DatabaseReference = ref.child(FirebaseDatabaseRoot.users.stringValue)
        let userRef: DatabaseReference = usersRef.child(userID)
        
        userRef.observeSingleEvent(of: .value, with: {
            (userSnapshot) in
            
            if let userValue = userSnapshot.value as? NSDictionary,
                let userObject = User(fromNSDictionary: userValue) {
                
                userObject.bags += [gymBag.bagID]
                userRef.setValue(userObject.asDict)
                
                delegate.sent(true, object: gymBag, to: userObject)
            }
        }) {
            (error) in
            
            print(error.localizedDescription)
        }
    }
    
    static func pullBagIDs(delegate: PullGymBagsDelegate) {
        
        let ref: DatabaseReference = Database.database().reference()
        
        // Get the current user.
        if let currentUser = Auth.auth().currentUser {
            
            // Drill into the "users" route and then into the current user in the database.
            ref.child(FirebaseDatabaseRoot.users.stringValue)
                .child(currentUser.uid).observeSingleEvent(of: .value, with: {
                    (userSnapshot) in
                    
                    // Make sure the user at this location is valid.
                    if let userValue = userSnapshot.value as? NSDictionary {
                        
                        // Try to instantiate a User object from the NSDictionary object pulled down from Firebase.
                        if let currentUserObject = User(fromNSDictionary: userValue) {
                            
                            delegate.gymBagIDsPulled(gymBagIDs: currentUserObject.bags)
                        }
                    }
                }) {
                    (error) in
                    print(error.localizedDescription)
            }
        }
        else {
            
            // User not logged in, so no user bagIDs.
            delegate.gymBagIDsPulled(gymBagIDs: [])
        }
    }
    
    static func pullBrowsableGymBags(delegate: PullGymBagsDelegate) {
        
        let ref: DatabaseReference = Database.database().reference()
        
        // Drill into the browsable bags.
        ref.child(FirebaseDatabaseRoot.browsableBags.stringValue)
            .observeSingleEvent(of: .value, with: {
                (bagsSnapshot) in
                
                var bags: [GymBag] = []
                
                for child in bagsSnapshot.children {
                    
                    if let bagSnapshot = child as? DataSnapshot, let bagValue = bagSnapshot.value as? NSDictionary, let bagObject =
                        GymBag(fromNSDictionary: bagValue, bagID: bagSnapshot.key) {
                        
                        bags += [bagObject]
                    }
                }
                
                delegate.gymBagObjectsPulled(gymBags: bags, browseObjectType: .browsable, totalToPull: bags.count)
            }) {
                (error) in
                
                print("\r\r----------------------------\rError pulling Browsable Routines: \(error.localizedDescription)\r----------------------------\r\r")
        }
    }
    
    static func pullGymBagObjects(delegate: PullGymBagsDelegate, bagIDs: [String]) {
        
        let ref: DatabaseReference = Database.database().reference()
        
        // Loop through all the bagIDs pulled from the Firebase user.
        for id in bagIDs {
            
            // Drill into the "bags" root, then into this routine.
            ref.child(FirebaseDatabaseRoot.gymBags.stringValue)
                .child(id).observeSingleEvent(of: .value) {
                    (bagSnapshot) in
                    
                    // Make sure the bag at this location is valid.
                    if let bagDictionary = bagSnapshot.value as? NSDictionary {
                        
                        // Attempt to instantiate a GymBag object from the NSDictionary pulled from Firebase.
                        if let bagObject = GymBag(fromNSDictionary: bagDictionary, bagID: bagSnapshot.key) {
                            
                            delegate.gymBagObjectsPulled(gymBags: [bagObject], browseObjectType: .user, totalToPull: bagIDs.count)
                        }
                    }
            }
        }
    }
    
    static func saveNewGymBagToGymBagsRoot(delegate: SaveGymBagDelegate, bagIconID: String?, bagName: String, bagItems: [BagItem]) {
        
        let ref: DatabaseReference = Database.database().reference()
        let newBagRef: DatabaseReference = ref.child("gymBags").childByAutoId()
        
        if let bagID = newBagRef.key, let currentUser = Auth.auth().currentUser {
            
            let bag: GymBag = GymBag(bagID: bagID, creatorID: currentUser.uid, bagIconID: bagIconID, bagName: bagName, bagItems: bagItems)
            
            newBagRef.setValue(bag.asDict) {
                (error:Error?, errorRef:DatabaseReference) in
                
                if let error = error {
                    
                    print("Data could not save new bag to Firebase gymBags root: \(error).")
                } else {
                    
                    print("Data saved successfully to Firebase gymBags root!")
                    delegate.savedBagToGymBagsRoot(true, gymBag: bag)
                }
            }
        }
    }
    
    static func saveNewGymBagToGymBagsRoot(gymBag: GymBag, delegate: SaveGymBagDelegate) {
        
        let ref: DatabaseReference = Database.database().reference()
        let bagsRef: DatabaseReference = ref.child(FirebaseDatabaseRoot.gymBags.stringValue)
        
        let newBagRef: DatabaseReference = bagsRef.childByAutoId()
        
        if let key = newBagRef.key {
            
            gymBag.bagID = key
            
            if let currentUser = Auth.auth().currentUser {
                
                gymBag.creatorID = currentUser.uid
                
                newBagRef.setValue(gymBag.asDict) {
                    (error:Error?, errorRef:DatabaseReference) in
                    
                    if let error = error {
                        
                        print("Data could not be saved: \(error.localizedDescription).")
                    } else {
                        
                        print("Data saved successfully!")
                        delegate.savedBagToGymBagsRoot(true, gymBag: gymBag)
                    }
                }
            }
            else {
                
                // User is not signed in.
                // TODO: Alert user that for the alpha, saving locally is not yet up and running. To create routines, they must create an account and be signed in.
            }
        }
    }
    
    static func saveGymBagNewToUserRoot(delegate: SaveGymBagDelegate, gymBag: GymBag) {
        
        let ref: DatabaseReference = Database.database().reference()
        let usersRef: DatabaseReference = ref.child("users")
        
        if let currentUser = Auth.auth().currentUser {
            
            let currentUserRef: DatabaseReference = usersRef.child(currentUser.uid)
            
            currentUserRef.observeSingleEvent(of: .value, with: { (userSnapshot) in
                
                if let userValue = userSnapshot.value as? NSDictionary {
                    print(userValue.description)
                    if let currentUserObject = User(fromNSDictionary: userValue) {
                        
                        currentUserObject.bags += [gymBag.bagID]
                        currentUserRef.setValue(currentUserObject.asDict) {
                            (error:Error?, ref:DatabaseReference) in
                            
                            if let error = error {
                                
                                print("Data could not be saved: \(error).")
                            } else {
                                
                                print("Data saved successfully!")
                                delegate.savedBagToUserRoot(true, gymBag: gymBag)
                            }
                        }
                    }
                }
            }) { (error) in
                print("\r\n\r\n------------------ CreateWorkoutsViewController.swift | getUserObject -> Description: \(error.localizedDescription) ------------------\r\n\r\n")
            }
        }
    }
    
    static func pushGymBagUpdateToFirebase(delegate: SaveGymBagDelegate, bagID: String, bagIconID: String?, bagName: String, bagItems: [BagItem]) {
        
        let ref: DatabaseReference = Database.database().reference()
        let gymBagsRef: DatabaseReference = ref.child("gymBags")
        if let currentUser = Auth.auth().currentUser {
            
            let gymBag: GymBag = GymBag(bagID: bagID, creatorID: currentUser.uid, bagIconID: bagIconID, bagName: bagName, bagItems: bagItems)
            
            gymBagsRef.child(gymBag.bagID).setValue(gymBag.asDict) {
                (error:Error?, ref:DatabaseReference) in
                
                if let error = error {
                    
                    print("Data could not be updated to \(gymBag.bagID) on gymBags root: \(error).")
                } else {
                    
                    print("Data updated successfully to \(gymBag.bagID) on gymBags root!")
                    delegate.bagUpdated(true, gymBag: gymBag)
                }
            }
        }
    }
    
    static func deleteFromFirebaseUserRoot(bagID: String) {
        
        let ref: DatabaseReference = Database.database().reference()
        
        if let currentUser = Auth.auth().currentUser {
            
            let currentUserRef: DatabaseReference = ref.child(FirebaseDatabaseRoot.users.stringValue).child(currentUser.uid)
            
            currentUserRef.observeSingleEvent(of: .value, with: {
                (userSnapshot) in
                
                if let userValue = userSnapshot.value as? NSDictionary {
                    
                    if let currentUserObject = User(fromNSDictionary: userValue) {
                        
                        var bagIDs: [String] = currentUserObject.bags
                        
                        bagIDs.removeAll { $0 == bagID }
                        currentUserObject.bags = bagIDs
                        
                        currentUserRef.setValue(currentUserObject.asDict)
                        
                        // This could be used later if I want to set up the empty view on this screen.
                        //                        if self.exerciseIDs.count < 1 {
                        //                            self.setUpEmptyView()
                        //                        }
                    }
                }
            }) {
                (error) in
                print(error.localizedDescription)
            }
        }
    }
    
    static func deleteFromFirebaseGymBagsRoot(bagID: String) {
        
        let ref: DatabaseReference = Database.database().reference()
        ref.child(FirebaseDatabaseRoot.gymBags.stringValue).child(bagID).setValue(nil)
    }
}
