//
//  TimerFormatterUtil.swift
//  gym-companion-app
//
//  Created by Marcos Felipe Costa da Silva on 7/12/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

class TimerFormatterUtil {
    
    //This class helps to format an Int into a time String
    
    static func formatToTime(this num: Int) -> String {
        let minutesNum: Int = num / 60
        let secondsNum: Int = num % 60
        
        func zeroFormatter(num: Int) -> String {
            if num < 10 {
                return "0\(num)"
            } else {
                return "\(num)"
            }
        }
        
        let minutesText = zeroFormatter(num: minutesNum)
        let secondsText = zeroFormatter(num: secondsNum)
        
        return "\(minutesText):\(secondsText)"
    }
    
}
