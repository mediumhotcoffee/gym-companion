//
//  FirebaseAccountsUtil.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/25/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

class FirebaseAccountsUtil {
    
    static func getCurrentUser(delegate: FirebaseAccountDelegate) {
        
        if let currentUser = Auth.auth().currentUser {
            
            let ref: DatabaseReference = Database.database().reference()
            let usersRef: DatabaseReference = ref.child(FirebaseDatabaseRoot.users.stringValue)
            
            usersRef.child(currentUser.uid).observeSingleEvent(of: .value, with: {
                (currentUserSnapshot) in
                
                if let userValue = currentUserSnapshot.value as? NSDictionary,
                    let userObject = User(fromNSDictionary: userValue) {
                    
                    delegate.userPulled(user: userObject, current: true)
                }
            }, withCancel: {
                (error) in
                
                print(error.localizedDescription)
            })
        }
    }
    
    static func getUser(userID: String, delegate: FirebaseAccountDelegate) {
        
        let ref: DatabaseReference = Database.database().reference()
        let usersRef: DatabaseReference = ref.child(FirebaseDatabaseRoot.users.stringValue)
        
        usersRef.child(userID).observeSingleEvent(of: .value, with: {
            (currentUserSnapshot) in
            
            if let userValue = currentUserSnapshot.value as? NSDictionary,
                let userObject = User(fromNSDictionary: userValue) {
                
                delegate.userPulled(user: userObject, current: false)
            }
        }, withCancel: {
            (error) in
            
            print(error.localizedDescription)
        })
    }
    
    static func upgradePaidStatus(paid: Bool) {
        
        let ref: DatabaseReference = Database.database().reference()
        let usersRef: DatabaseReference = ref.child(FirebaseDatabaseRoot.users.stringValue)
        
        if let currentUser = Auth.auth().currentUser {
            
            usersRef.child(currentUser.uid).child("paid").setValue(paid)
        }
    }
    
    static func addTraineeToTrainer(trainerID: String, traineeID: String) {
        
        let ref: DatabaseReference = Database.database().reference()
        let usersRef: DatabaseReference = ref.child(FirebaseDatabaseRoot.users.stringValue)
        let trainerRef: DatabaseReference = usersRef.child(trainerID)
        
        trainerRef.observeSingleEvent(of: .value, with: { (trainerSnapshot) in
            
            if let trainerValue = trainerSnapshot.value as? NSDictionary, let trainerObject = User(fromNSDictionary: trainerValue) {
                
                if !trainerObject.trainees.contains(traineeID) {
                    
                    trainerObject.trainees += [traineeID]
                    trainerRef.setValue(trainerObject.asDict)
                }
            }
        }, withCancel: {
            (error) in
            
            print(error.localizedDescription)
        })
    }
}
