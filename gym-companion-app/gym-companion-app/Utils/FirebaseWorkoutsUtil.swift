//
//  FirebaseWorkoutsUtil.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/15/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class FirebaseWorkoutsUitl {
    
    static func pushNewRoutineToUser(userID: String, routine: Routine, delegate: ShareObjectDelegate) {
        
        let ref: DatabaseReference = Database.database().reference()
        let usersRef: DatabaseReference = ref.child(FirebaseDatabaseRoot.users.stringValue)
        let userRef: DatabaseReference = usersRef.child(userID)
        
        userRef.observeSingleEvent(of: .value, with: {
            (userSnapshot) in
            
            if let userValue = userSnapshot.value as? NSDictionary,
                let userObject = User(fromNSDictionary: userValue) {
                
                userObject.routines += [routine.routineID]
                userRef.setValue(userObject.asDict)
                
                delegate.sent(true, object: routine, to: userObject)
            }
        }) {
            (error) in
            
            print(error.localizedDescription)
        }
    }
    
    static func updateExercise(delegate: SaveExerciseDelegate, exercise: Exercise, nameTextField: UITextField?, restTimeTextField: UITextField?, repsTextField: UITextField?, setsTextField: UITextField?, weightTextField: UITextField?, selectedCategories: [WorkoutCategory]) {
        
        if let name = nameTextField?.text,
            let restTimeString = restTimeTextField?.text,
            let restTime = Int(restTimeString),
            let repsString = repsTextField?.text,
            let reps = Int(repsString),
            let setsString = setsTextField?.text,
            let sets = Int(setsString),
            let weightString = weightTextField?.text {
            
            let ref: DatabaseReference = Database.database().reference()
            let exercisesRef: DatabaseReference = ref.child(FirebaseDatabaseRoot.exercises.stringValue)
            
            exercise.name = name
            exercise.restTimeSeconds = restTime
            exercise.reps = reps
            exercise.sets = sets
            exercise.weight = Int(weightString)
            exercise.categories = selectedCategories
            
            exercisesRef.child(exercise.exerciseID).setValue(exercise.asDict) { (error, errorRef) in
                
                if let error = error {
                    
                    print(error.localizedDescription)
                    return
                }
                
                delegate.updatedExercise(true)
            }
        }
    }
    
    static func saveNewRoutineToRoutineRoot(routine: Routine, delegate: SaveRoutineDelegate) {
        
        let ref: DatabaseReference = Database.database().reference()
        let routinesRef: DatabaseReference = ref.child(FirebaseDatabaseRoot.routines.stringValue)
        
        let newRoutineRef: DatabaseReference = routinesRef.childByAutoId()
        
        if let key = newRoutineRef.key {
            
            routine.routineID = key
            
            if let currentUser = Auth.auth().currentUser {
                
                routine.creatorID = currentUser.uid
                
                newRoutineRef.setValue(routine.asDict) {
                    (error:Error?, errorRef:DatabaseReference) in
                    
                    if let error = error {
                        
                        print("Data could not be saved: \(error.localizedDescription).")
                    } else {
                        
                        print("Data saved successfully!")
                        delegate.savedRoutineToRoutinesRoot(true)
                    }
                }
            }
            else {
                
                // User is not signed in.
                // TODO: Alert user that for the alpha, saving locally is not yet up and running. To create routines, they must create an account and be signed in.
            }
        }
    }
    
    static func saveNewRoutineToUserRoot(routine: Routine, delegate: SaveRoutineDelegate) {
        
        let ref: DatabaseReference = Database.database().reference()
        let usersRef: DatabaseReference = ref.child("users")
        
        if let currentUser = Auth.auth().currentUser {
            
            let currentUserRef: DatabaseReference = usersRef.child(currentUser.uid)
            
            currentUserRef.observeSingleEvent(of: .value, with: { (userSnapshot) in
                
                if let userValue = userSnapshot.value as? NSDictionary {
                    
                    if let currentUserObject = User(fromNSDictionary: userValue) {
                        
                        currentUserObject.routines += [routine.routineID]
                        currentUserRef.setValue(currentUserObject.asDict) {
                            (error:Error?, ref:DatabaseReference) in
                            
                            if let error = error {
                                
                                print("Data could not be saved: \(error).")
                            } else {
                                
                                print("Data saved successfully!")
                                delegate.savedRoutineToUserRoot(true)
                            }
                        }
                    }
                }
            }) { (error) in
                print("\r\n\r\n------------------ CreateWorkoutsViewController.swift | getUserObject -> Description: \(error.localizedDescription) ------------------\r\n\r\n")
            }
        }
    }
    
    static func saveNewBrowsableExerciseToExerciseRoot(delegate: SaveExerciseDelegate, exerciseID: String) {
        
        let ref: DatabaseReference = Database.database().reference()
        let exercisesRef: DatabaseReference = ref.child(FirebaseDatabaseRoot.exercises.stringValue)
        let browsableExercisesRef: DatabaseReference = ref.child(FirebaseDatabaseRoot.browsableExercises.stringValue)
        let oldExerciseRef: DatabaseReference = browsableExercisesRef.child(exerciseID)
        let newExerciseRef: DatabaseReference = exercisesRef.childByAutoId()
        
        oldExerciseRef.observeSingleEvent(of: .value) {
            (exerciseSnapshot) in
            
            if let exerciseValue = exerciseSnapshot.value as? NSDictionary, let exerciseObject = Exercise(fromNSDictionary: exerciseValue, exerciseID: exerciseSnapshot.key) {
                
                if let key = newExerciseRef.key, let currentUser = Auth.auth().currentUser {
                    
                    exerciseObject.exerciseID = key
                    exerciseObject.creatorID = currentUser.uid
                    
                    newExerciseRef.setValue(exerciseObject.asDict) {
                        (error:Error?, errorRef:DatabaseReference) in
                        
                        if let error = error {
                            
                            print("Data could not be saved: \(error).")
                        } else {
                            
                            delegate.savedExerciseToExerciseRoot(exercise: exerciseObject, true)
                        }
                    }
                }
            }
        }
    }
    
    static func saveNewUserExerciseToExerciseRoot(delegate: SaveExerciseDelegate, exerciseID: String) {
        
        let ref: DatabaseReference = Database.database().reference()
        let exercisesRef: DatabaseReference = ref.child(FirebaseDatabaseRoot.exercises.stringValue)
        let oldExerciseRef: DatabaseReference = exercisesRef.child(exerciseID)
        let newExerciseRef: DatabaseReference = exercisesRef.childByAutoId()
        
        oldExerciseRef.observeSingleEvent(of: .value) {
            (exerciseSnapshot) in
            
            if let exerciseValue = exerciseSnapshot.value as? NSDictionary, let exerciseObject = Exercise(fromNSDictionary: exerciseValue, exerciseID: exerciseSnapshot.key) {
                
                if let key = newExerciseRef.key, let currentUser = Auth.auth().currentUser {
                    
                    exerciseObject.exerciseID = key
                    exerciseObject.creatorID = currentUser.uid
                    
                    newExerciseRef.setValue(exerciseObject.asDict) {
                        (error:Error?, errorRef:DatabaseReference) in
                        
                        if let error = error {
                            
                            print("Data could not be saved: \(error).")
                        } else {
                            
                            delegate.savedExerciseToExerciseRoot(exercise: exerciseObject, true)
                        }
                    }
                }
            }
        }
    }
    
    static func saveNewExerciseToExerciseRoot(ref: DatabaseReference, delegate: SaveExerciseDelegate, nameTextField: UITextField?, restTimeTextField: UITextField?, repsTextField: UITextField?, setsTextField: UITextField?, weightTextField: UITextField?, selectedCategories: [WorkoutCategory]) {
        
        let exercisesRef: DatabaseReference = ref.child("exercises")
        let newExerciseRef: DatabaseReference = exercisesRef.childByAutoId()
        
        if let key = newExerciseRef.key {
            
            if let currentUser = Auth.auth().currentUser,
                let name = nameTextField?.text,
                let restTimeString = restTimeTextField?.text,
                let restTime = Int(restTimeString),
                let repsString = repsTextField?.text,
                let reps = Int(repsString),
                let setsString = setsTextField?.text,
                let sets = Int(setsString),
                let weightString = weightTextField?.text {
                
                let exercise: Exercise = Exercise(exerciseID: key, creatorID: currentUser.uid, name: name, categories: selectedCategories, weight: Int(weightString), restTimeSeconds: restTime, sets: sets, reps: reps)
                
                newExerciseRef.setValue(exercise.asDict) {
                    (error:Error?, errorRef:DatabaseReference) in
                    
                    if let error = error {
                        
                        print("Data could not be saved: \(error).")
                    } else {
                        
                        delegate.savedExerciseToExerciseRoot(exercise: exercise, true)
                    }
                }
            }
            else {
                
                // User is not signed in.
                // TODO: Save Locally?
            }
        }
    }
    
    static func saveNewExerciseToUserRoot(delegate: SaveExerciseDelegate, exercise: Exercise) {
        
        let ref: DatabaseReference = Database.database().reference()
        let usersRef: DatabaseReference = ref.child("users")
        
        if let currentUser = Auth.auth().currentUser {
            
            let currentUserRef: DatabaseReference = usersRef.child(currentUser.uid)
            
            currentUserRef.observeSingleEvent(of: .value, with: {
                (userSnapshot) in
                
                if let userValue = userSnapshot.value as? NSDictionary {
                    
                    if let currentUserObject = User(fromNSDictionary: userValue) {
                        
                        currentUserObject.exercises += [exercise.exerciseID]
                        currentUserRef.setValue(currentUserObject.asDict) {
                            (error:Error?, ref:DatabaseReference) in
                            
                            if let error = error {
                                
                                print("Data could not be saved: \(error).")
                            } else {
                                
                                print("Data saved successfully!")
                                delegate.savedExerciseToUserRoot(true)
                            }
                        }
                    }
                }
            }) { (error) in
                print("\r\n\r\n------------------ CreateExerciseViewController.swift | getUserObject -> Description: \(error.localizedDescription) ------------------\r\n\r\n")
            }
        }
    }

    static func pullBrowsableRoutines(delegate: PullRoutinesDelegate) {
        
        let ref: DatabaseReference = Database.database().reference()
        
        // Drill into the browsable routines.
        ref.child(FirebaseDatabaseRoot.browsableRoutines.stringValue)
            .observeSingleEvent(of: .value, with: {
            (routinesSnapshot) in
            
            var routines: [Routine] = []
                
            for child in routinesSnapshot.children {
                
                if let routineSnapshot = child as? DataSnapshot, let routineValue = routineSnapshot.value as? NSDictionary, let routineObject = Routine(fromNSDictionary: routineValue, routineID: routineSnapshot.key) {
                    
                    routines += [routineObject]
                }
            }
                
                delegate.routineObjectsPulled(routines: routines, browseObjectType: .browsable, totalToPull: routines.count)
        }) {
            (error) in
            
            print("\r\r----------------------------\rError pulling Browsable Routines: \(error.localizedDescription)\r----------------------------\r\r")
        }
    }
    
    static func pullUserRoutinesIDs(delegate: PullRoutinesDelegate) {
        
        let ref: DatabaseReference = Database.database().reference()
        
        // Get the current user.
        if let currentUser = Auth.auth().currentUser {
            
            // Drill into the "users" route and then into the current user in the database.
            ref.child("users").child(currentUser.uid).observeSingleEvent(of: .value, with: {
                (userSnapshot) in
                
                // Make sure the user at this location is valid.
                if let userValue = userSnapshot.value as? NSDictionary {
                    
                    // Try to instantiate a User object from the NSDictionary object pulled down from Firebase.
                    if let currentUserObject = User(fromNSDictionary: userValue) {
                        
                        print(currentUserObject.firstName)
                        print(userSnapshot.key)
                        print(currentUserObject.routines)
                        delegate.routineIDsPulled(routineIDs: currentUserObject.routines)
                    }
                }
            }) {
                (error) in
                print(error.localizedDescription)
            }
        }
        else {
            
            delegate.routineIDsPulled(routineIDs: [])
        }
    }
    
    static func pullRoutineExercises(routine: Routine, delegate: PullRoutineExercisesDelegate) {
        
        let ref: DatabaseReference = Database.database().reference()
        let exercisesRef: DatabaseReference = ref.child(FirebaseDatabaseRoot.exercises.stringValue)
        
        for id in routine.exercises {
            
            let exerciseRef: DatabaseReference = exercisesRef.child(id)
            
            exerciseRef.observeSingleEvent(of: .value, with: {
                (exerciseSnapshot) in
                
                if let exerciseValue = exerciseSnapshot.value as? NSDictionary, let key = exerciseRef.key, let exerciseObject = Exercise(fromNSDictionary: exerciseValue, exerciseID: key) {
                    
                    delegate.routineExerciseObjectsPulled(routine: routine, exercises: [exerciseObject], browseObjectType: .user, totalToPull: routine.exercises.count)
                }
            }, withCancel: { (error) in print(error.localizedDescription) })
        }
    }
    
    static func pullUserRoutines(routineIDs: [String], delegate: PullRoutinesDelegate) {
        
        let ref: DatabaseReference = Database.database().reference()
        
        
        // Loop through all the routineIDs pulled from the Firebase user.
        for id in routineIDs {
            
            // Drill into the "routines" root, then into this routine.
            ref.child("routines").child(id).observeSingleEvent(of: .value) {
                (routineSnapshot) in
                
                // Make sure the routine at this location is valid.
                if let routineDictionary = routineSnapshot.value as? NSDictionary {
                    
                    // Attempt to instantiate a Routine object from the NSDictionary pulled from Firebase.
                    if let routineObject = Routine(fromNSDictionary: routineDictionary, routineID: routineSnapshot.key) {
                        
                        delegate.routineObjectsPulled(routines: [routineObject], browseObjectType: .user, totalToPull: routineIDs.count)
                    }
                }
            }
        }
    }
    
    static func pullBrowsableExercises(delegate: PullExercisesDelegate) {
        
        let ref: DatabaseReference = Database.database().reference()
        ref.child(FirebaseDatabaseRoot.browsableExercises.stringValue).observeSingleEvent(of: .value) {
            (exerciseSnapshot) in
            
            for child in exerciseSnapshot.children {
                
                if let childSnapshot = child as? DataSnapshot,
                    let exerciseValue = childSnapshot.value as? NSDictionary, let exerciseObject = Exercise(fromNSDictionary: exerciseValue, exerciseID: childSnapshot.key) {
                    
                    delegate.exerciseObjectsPulled(exercises: [exerciseObject], browseObjectType: .browsable, totalToPull: Int(exerciseSnapshot.childrenCount))
                }
            }
        }
    }
    
    static func pullBrowsableExercises(exerciseIDs: [String], delegate: PullExercisesDelegate) {
        
        let ref: DatabaseReference = Database.database().reference()
        
        if exerciseIDs.count < 1 {
            
            delegate.exerciseObjectsPulled(exercises: [], browseObjectType: .user, totalToPull: 0)
        }
        
        for id in exerciseIDs {
            
            ref.child(FirebaseDatabaseRoot.browsableExercises.stringValue).child(id).observeSingleEvent(of: .value) { (exerciseSnapshot) in
                
                print(exerciseSnapshot)
                
                if let exerciseValue = exerciseSnapshot.value as? NSDictionary, let exerciseObject = Exercise(fromNSDictionary: exerciseValue, exerciseID: exerciseSnapshot.key) {
                    
                    delegate.exerciseObjectsPulled(exercises: [exerciseObject], browseObjectType: .user, totalToPull: exerciseIDs.count)
                }
            }
        }
    }
    
    static func pullUserExerciseIDs(delegate: PullExercisesDelegate) {
        
        let ref: DatabaseReference = Database.database().reference()
        
        // Get the current user.
        if let currentUser = Auth.auth().currentUser {
            
            // Drill into the "users" route and then into the current user in the database.
            ref.child("users").child(currentUser.uid).observeSingleEvent(of: .value, with: {
                (userSnapshot) in
                
                // Make sure the user at this location is valid.
                if let userValue = userSnapshot.value as? NSDictionary {
                    
                    // Try to instantiate a User object from the NSDictionary object pulled down from Firebase.
                    if let currentUserObject = User(fromNSDictionary: userValue) {
                        
                        delegate.exerciseIDsPulled(exerciseIDs: currentUserObject.exercises)
                    }
                }
            }) {
                (error) in
                print(error.localizedDescription)
            }
        }
        else {
            
            delegate.exerciseIDsPulled(exerciseIDs: [])
        }
    }
    
    static func pullUserExercises(exerciseIDs: [String], delegate: PullExercisesDelegate) {
        
        let ref: DatabaseReference = Database.database().reference()
        
        if exerciseIDs.count < 1 {
            
            delegate.exerciseObjectsPulled(exercises: [], browseObjectType: .user, totalToPull: 0)
        }
        
        for id in exerciseIDs {
            
            ref.child(FirebaseDatabaseRoot.exercises.stringValue).child(id).observeSingleEvent(of: .value) { (exerciseSnapshot) in
                
                if let exerciseValue = exerciseSnapshot.value as? NSDictionary, let exerciseObject = Exercise(fromNSDictionary: exerciseValue, exerciseID: exerciseSnapshot.key) {
                    
                    delegate.exerciseObjectsPulled(exercises: [exerciseObject], browseObjectType: .user, totalToPull: exerciseIDs.count)
                }
            }
        }
    }
}
