//
//  ComingSoonAlert.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 6/26/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

class AlertHelper {
    
    // Use this method to get a UIAlertController that alerts the user that the feature is not yet functional.
    // Remember to show the alert after getting it.
    //   -- E.g. self.present(alert, animated: true, completion: nil)
    static func getComingSoonAlert() -> UIAlertController {
        
        let alertController = UIAlertController(title: "Coming Soon", message: "We are currently in Beta stage. This feature is coming soon.", preferredStyle: .alert)
        
        let okButton = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            print("OK button tapped")
        })
        
        alertController.addAction(okButton)
        
        return alertController
    }
    
    // Use this method to get a UIAlertController that alerts the user that the featuer's limits have been met and they must upgrade their account if they wish to suprass said limit.
    // Remember to show the alert after getting it.
    //   -- E.g. self.present(alert, animated: true, completion: nil)
    static func getLimitAlert(listType: ListType, delegate: FirebaseAccountDelegate) -> UIAlertController {
        
        var title: String
        var message: String
        
        switch listType {
        case .routine:
            title = "Routine Limit Reached"
            message = "To add more than 3 routines, please upgrade your account."
        case .exercise:
            title = "Exercise Limit Reached"
            message = "To add more than 5 exercises to a routine, please upgrade your account."
        case .bag:
            title = "Gym Bag Limit Reached"
            message = "To add more than 1 gym bag, please upgrade your account."
        case .bagItem:
            title = "Bag Item Limit Reached"
            message = "To add more than 3 items to a gym bag, please upgrade your account."
        case .trainees:
            title = "Trainees Limit Reached"
            message = "To add more than 3 trainees, please upgrade your account."
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let upgradeButton = UIAlertAction(title: "Upgrade Account", style: .default, handler: {
            (action) -> Void in
            
            delegate.upgradeTapped(sender: action)
        })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alertController.addAction(upgradeButton)
        alertController.addAction(cancelButton)
        
        return alertController
    }
    
    static func getSentAlert(sent: Bool, traineeFirstName: String) -> UIViewController {
        
        let sharedView: SharedModalViewController = SharedModalViewController()
        sharedView.modalPresentationStyle = .overFullScreen
        sharedView.setSentMessage(traineeFirstName: traineeFirstName)
        
        return sharedView
    }
    
    // Use this method to get a list of image locations for icons in firebase storage.
    static func getListActionSheet(screenHeight: CGFloat, selectMultiple: Bool, tableViewHandler: IconActionSheetTableViewHandler, actionSheetDelegate: UITableViewActionSheetDelegate) -> UIAlertController {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let margin:CGFloat = 8.0
        let rect = CGRect(x: margin, y: margin, width: alertController.view.bounds.size.width - margin * 4.0, height: screenHeight * 0.15)
        
        let alertTableView: UITableView = UITableView(frame: rect)
        alertTableView.delegate = tableViewHandler
        alertTableView.dataSource = tableViewHandler
        alertTableView.allowsMultipleSelection = selectMultiple
        alertTableView.backgroundColor = UIColor.clear
        
        alertController.view.addSubview(alertTableView)
        alertTableView.reloadData()
        
        let addAction = UIAlertAction(title: "Select", style: .default) {
            (action) in
            
            if let selectedIndexPaths = alertTableView.indexPathsForSelectedRows {
                
                actionSheetDelegate.addToList(selectedIndexPaths: selectedIndexPaths)
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        
        alertController.addAction(addAction)
        alertController.addAction(cancelAction)
        
        let height:NSLayoutConstraint = NSLayoutConstraint(item: alertController.view!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: screenHeight * 0.3)
        
        alertController.view.addConstraint(height)
        
        return alertController
    }
    
    // Use this method to get a list of image locations for icons in firebase storage.
    static func getTraineeListActionSheet(screenHeight: CGFloat, selectMultiple: Bool, traineeIDs: [String], alertTableView: UITableView, tableViewHandler: TraineeListActionSheetTableViewHandler, actionSheetDelegate: UITableViewActionSheetDelegate, sender: UIButton) -> UIAlertController {
        
        let alertController = UIAlertController(title: "Share", message: "Select a trainee to share this with.", preferredStyle: UIAlertController.Style.actionSheet)
        
        alertTableView.allowsMultipleSelection = selectMultiple
        alertTableView.backgroundColor = UIColor.clear
        alertTableView.delegate = tableViewHandler
        alertTableView.dataSource = tableViewHandler
        
        alertController.view.addSubview(alertTableView)
        alertTableView.reloadData()
        
        let sendAction = UIAlertAction(title: "Send", style: .default) {
            (action) in
            
            if let selectedIndexPaths = alertTableView.indexPathsForSelectedRows {
                
                actionSheetDelegate.addToList(selectedIndexPaths: selectedIndexPaths)
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        
        alertController.addAction(sendAction)
        alertController.addAction(cancelAction)
        
        let height:NSLayoutConstraint = NSLayoutConstraint(item: alertController.view!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: screenHeight * 0.45)
        
        if let popoverController = alertController.popoverPresentationController {
            
            popoverController.sourceView = sender
            popoverController.sourceRect = sender.bounds
        }
        
        alertController.view.addConstraint(height)
        
        return alertController
    }
    
    static func getAddOptionsActionSheet(listType: ListType, sender: UIButton, delegate: AddOptionsActionSheetDelegate) -> UIAlertController {
        
        var title: String = ""
        var message: String = ""
        var browse: String = ""
        var create: String = ""
        
        // Set the text of the action sheet and options based on list type.
        switch listType {
        case .bag:
            
            title = "Add Bag"
            message = "Browse our ready-to-go bags, or create a custom one?"
            browse = "Browse Bags"
            create = "Create Bag"
        case .bagItem:
            
            title = "Add Bag Item"
            message = "Browse our ready-to-go bag items, or create a custom one?"
            browse = "Browse Bag Items"
            create = "Create Bag Item"
        case .exercise:
            
            title = "Add Exercise"
            message = "Browse our ready-to-go exercises, or create a custom one?"
            browse = "Browse Exercises"
            create = "Create Exercise"
        case .routine:
            
            title = "Add Routine"
            message = "Browse our ready-to-go routines, or create a custom one?"
            browse = "Browse Routines"
            create = "Create Routine"
        case .trainees:
            print("AlertHelper.swift | getAddOptionsActionSheet(listType, sender, delegate)\rError: Unexpected list type. \(listType.stringValue) shouldn't be passed here.")
        }
        
        // Create the Action Sheet.
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        // Create a browse option.
        //  -- If browse selected, alert the delegate.
        let browseButton = UIAlertAction(title: browse, style: .default, handler: { (action) -> Void in
            
            delegate.browseSelected(sender: sender)
        })
        
        // Create a create option.
        //  -- If create selected, alert the delegate.
        let  createButton = UIAlertAction(title: create, style: .default, handler: { (action) -> Void in
            
            delegate.createSelected(sender: sender)
        })
        
        // Create a cancel option.
        //  -- If cancel selected, simply dismiss the action sheet.
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        // Add the options to the action sheet.
        //TODO: Change colors of buttons to match mockups.
        alertController.addAction(browseButton)
        alertController.addAction(createButton)
        alertController.addAction(cancelButton)
        
        // If the device is a tablet, the alert controller must be show as a pop-over, which needs a source and rect to display correctly.
        // Without this, the app will crash on iPad.
        // However, none of this is need, nor should be done, for phone versions, which is also taken care of here.
        if let popoverController = alertController.popoverPresentationController {
            
            popoverController.sourceView = sender
            popoverController.sourceRect = sender.bounds
        }
        
        // Return the created Action Sheet.
        return alertController
    }
    
    static func getSignInToAddListsAlert(sender: UIButton, delegate: FirebaseAccountDelegate) -> UIAlertController {
        
        // Create the alert.
        let alertController = UIAlertController(title: "Sign In", message: "To add lists, sign in or create an account.", preferredStyle: .alert)
        
        // Create an OK option.
        //  -- If OK is selected, take the user to the sign in screen.
        let okButton = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            
            delegate.signInSelected(action)
        })
        
        // Create a cancel option.
        //  -- If cancel is selected, simply dismiss the alert.
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        //TODO: Change colors of buttons to match mockups.
        alertController.addAction(okButton)
        alertController.addAction(cancelButton)
        
        if let popoverController = alertController.popoverPresentationController {
            
            popoverController.sourceView = sender
            popoverController.sourceRect = sender.bounds
        }
        
        return alertController
    }
}
