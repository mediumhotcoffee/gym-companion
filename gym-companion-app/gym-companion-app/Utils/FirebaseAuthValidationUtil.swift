//
//  FirebaseAuthValidationUtil.swift
//  gym-companion-app
//
//  Created by Marcos Silva on 6/30/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth

class FirebaseAuthValidationUtil {
    
    /*
     Performs Firebase Auth specific validation
     */
    
    //MARK: - Properties
    var formViewController: UIViewController
    
    //MARK: - Initializers
    init(formViewController: UIViewController) {
        self.formViewController = formViewController
    }
    
    //MARK: - Methods
    func notifyOfFirebaseAuthError(_ typeOfError: Error) {
        //Notifies the user that some info is missing or wrong
        print(typeOfError.localizedDescription)
        
        var message = "Authentication error. Please try again"
        
        if let errorCode = AuthErrorCode(rawValue: typeOfError._code) {
            switch errorCode {
            //First name errors
            case .invalidEmail:
                message = "Email is invalid. Please try again"
            case .emailAlreadyInUse:
                message = "Email is invalid. Please try again"
            case .operationNotAllowed:
                message = "Authentication is not enabled. Please contact developer"
            case .weakPassword:
                message = "Password too weak. Please try again"
            case .userDisabled:
                message = "User disabled. Please try another user"
            case .userNotFound:
                message = "User not found. Please try another email"
            case .wrongPassword:
                message = "Wrong password. Please try again"
            case .missingEmail:
                message = "No email. Please provide one and try again"
            case .networkError:
                message = "Network error. Please check your connection and try again"
            default:
                message = "Unknown error. Please contact the developer"
                print("\nERROR:\n"
                    + "\(errorCode.self)\n"
                    + "\(errorCode.rawValue)\n")
            }
        }
        
        let errorAlertController = UIAlertController(title: "Authentication Error", message: message, preferredStyle: .alert)
        
        //OK button
        let okButton = UIAlertAction(title: "OK", style: .default, handler: nil)
        errorAlertController.addAction(okButton)
        
        //Display alert
        formViewController.present(errorAlertController, animated: true, completion: nil)
    }
    
    static func checkIfSignedIn() -> Bool {
        
        return Auth.auth().currentUser == nil ? false : true
    }
}
