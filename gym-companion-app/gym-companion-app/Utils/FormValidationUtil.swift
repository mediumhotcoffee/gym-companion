//
//  FormValidationUtil.swift
//  gym-companion-app
//
//  Created by Marcos Silva on 6/30/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation
import UIKit

class FormValidationUtil {
    
    /*
    Performs validation on a TextField and retuns the proper value (e.g.
    String)
    */
    
    //MARK: - Properties
    //ViewController
    var formViewController: UIViewController
    //TextFields
    var firstNameTextField: UITextField?
    var lastNameTextField: UITextField?
    var emailTextField: UITextField?
    var passwordTextField: UITextField?
    var confirmPasswordTextField: UITextField?
    
    //MARK: - Initializers
    init(formViewController: UIViewController, firstNameTextField: UITextField, lastNameTextField: UITextField, emailTextField: UITextField, passwordTextField: UITextField, confirmPasswordTextField: UITextField) {
        //Sign Up version
        
        //ViewController
        self.formViewController = formViewController
        
        //TextFields
        self.firstNameTextField = firstNameTextField
        self.lastNameTextField = lastNameTextField
        self.emailTextField = emailTextField
        self.passwordTextField = passwordTextField
        self.confirmPasswordTextField = confirmPasswordTextField
    }
    
    init(formViewController: UIViewController, emailTextField: UITextField) {
        //Email only version
        
        //ViewController
        self.formViewController = formViewController
        
        //TextFields
        self.firstNameTextField = nil
        self.lastNameTextField = nil
        self.emailTextField = emailTextField
        self.passwordTextField = nil
        self.confirmPasswordTextField = nil
    }
    
    init(formViewController: UIViewController, emailTextField: UITextField, passwordTextField: UITextField) {
        //Login version
        
        //ViewController
        self.formViewController = formViewController
        
        //TextFields
        self.firstNameTextField = nil
        self.lastNameTextField = nil
        self.emailTextField = emailTextField
        self.passwordTextField = passwordTextField
        self.confirmPasswordTextField = nil
    }
    
    //MARK: - Methods
    private func getAndValidateFirstName(from textField: UITextField) throws -> String  {
        //Get first name
        guard let firstNameToValidate = textField.text else {
            throw FormError.firstNameMissing
        }
        
        //Check if first name exists
        guard !firstNameToValidate.isEmpty else {
            throw FormError.firstNameMissing
        }
        
        //Check if first name is long enough
        guard firstNameToValidate.count >= 2 else {
            throw FormError.firstNameTooShort
        }
        
        return firstNameToValidate
    }
    
    private func getAndValidateLastName(from textField: UITextField) throws -> String  {
        //Get last name
        guard let lastNameToValidate = textField.text else {
            throw FormError.lastNameMissing
        }
        
        //Check if last name exists
        guard !lastNameToValidate.isEmpty else {
            throw FormError.lastNameMissing
        }
        
        //Check if last name is long enough
        guard lastNameToValidate.count >= 2 else {
            throw FormError.lastNameTooShort
        }
        
        return lastNameToValidate
    }
    
    private func getAndValidateEmail(from textField: UITextField) throws -> String  {
        //Get email
        guard let emailToValidate = textField.text else {
            throw FormError.emailMissing
        }
        
        //Check if email is not empty
        guard !emailToValidate.isEmpty else {
            throw FormError.emailMissing
        }
        
        if !emailToValidate.contains("@") || !emailToValidate.contains(".") {
            throw FormError.emailNotWellFormatted
        }
        
        return emailToValidate
    }
    
    private func getAndValidatePassword(fromPassword passwordTextField: UITextField, fromConfirmPassword confirmPasswordTextField: UITextField) throws -> String  {
        //Gets and compares two passwords
        
        //Password
        //Get password
        guard let passwordToValidate = passwordTextField.text else {
            throw FormError.passwordMissing
        }
        //Check if password is valid
        guard !passwordToValidate.isEmpty else {
            throw FormError.passwordMissing
        }
        //Check if password is too short (less than 8 chars)
        guard passwordToValidate.count >= 8 else {
            throw FormError.passwordTooShort
        }
        
        //Confirm password
        //Get confirm password
        guard let confirmPasswordToValidate = confirmPasswordTextField.text else {
            throw FormError.confirmPasswordMissing
        }
        //Check if confirm password is valid
        guard !confirmPasswordToValidate.isEmpty else {
            throw FormError.confirmPasswordMissing
        }
        //Check if confirm password is too short (less than 8 chars)
        guard confirmPasswordToValidate.count >= 8 else {
            throw FormError.confirmPasswordTooShort
        }
        //Verify if passwords match
        guard passwordToValidate == confirmPasswordToValidate else {
            throw FormError.passwordMissmatch
        }
        
        return passwordToValidate
    }
    
    private func getAndValidatePassword(fromPassword passwordTextField: UITextField) throws -> String  {
        //Gets a single password
        //Useful for login screens
        
        //Get password
        guard let passwordToValidate = passwordTextField.text else {
            throw FormError.passwordMissing
        }
        //Check if password is valid
        guard !passwordToValidate.isEmpty else {
            throw FormError.passwordMissing
        }
        //Check if password is too short (less than 8 chars)
        guard passwordToValidate.count >= 8 else {
            throw FormError.passwordTooShort
        }
        
        return passwordToValidate
    }
    
    func firstNameExtractor() -> String? {
        var firstName: String? = nil
        
        do {
            try firstName = getAndValidateFirstName(from: firstNameTextField!)
            return firstName
        } catch FormError.firstNameMissing {
            notifyOfFormError(.firstNameMissing)
            return nil
        } catch FormError.firstNameTooShort {
            notifyOfFormError(.firstNameTooShort)
            return nil
        } catch {
            print("Unexpected error: \(error)")
            return nil
        }
    }
    
    func lastNameExtractor() -> String? {
        var lastName: String? = nil
        
        do {
            try lastName = getAndValidateLastName(from: lastNameTextField!)
            return lastName
        } catch FormError.lastNameMissing {
            notifyOfFormError(.lastNameMissing)
            return nil
        } catch FormError.lastNameTooShort {
            notifyOfFormError(.lastNameTooShort)
            return nil
        } catch {
            print("Unexpected error: \(error)")
            return nil
        }
    }
    
    func emailExtractor() -> String? {
        var email: String? = nil
        
        do {
            try email = getAndValidateEmail(from: emailTextField!)
            return email
        } catch FormError.emailMissing {
            notifyOfFormError(.emailMissing)
            return nil
        } catch FormError.emailNotWellFormatted {
            notifyOfFormError(.emailNotWellFormatted)
            return nil
        } catch {
            print("\nERROR:\n"
                + "\nFormValidationUtil.emailExtractor()\n"
                + "\nError: \(error)")
            return nil
        }
    }
    
    func passwordExtractor() -> String? {
        var password: String? = nil
        
        if confirmPasswordTextField != nil {
            do {
                try password = getAndValidatePassword(fromPassword: passwordTextField!, fromConfirmPassword: confirmPasswordTextField!)
                return password
            } catch FormError.passwordMissing {
                notifyOfFormError(.passwordMissing)
                return nil
            } catch FormError.passwordTooShort {
                notifyOfFormError(.passwordTooShort)
                return nil
            } catch FormError.confirmPasswordMissing {
                notifyOfFormError(.confirmPasswordMissing)
                return nil
            } catch FormError.confirmPasswordTooShort {
                notifyOfFormError(.confirmPasswordTooShort)
                return nil
            } catch FormError.passwordMissmatch {
                notifyOfFormError(.passwordMissmatch)
                return nil
            } catch {
                print("Unexpected error: \(error)")
                return nil
            }
        } else {
            do {
                try password = getAndValidatePassword(fromPassword: passwordTextField!)
                return password
            } catch FormError.passwordMissing {
                notifyOfFormError(.passwordMissing)
                return nil
            } catch FormError.passwordTooShort {
                notifyOfFormError(.passwordTooShort)
                return nil
            } catch {
                print("Unexpected error: \(error)")
                return nil
            }
        }
    }
    
    func notifyOfFormError(_ typeOfError: FormError) {
        //Notifies the user that some info is missing or wrong
        var title = "Form Error"
        var message = ""
        
        switch typeOfError {
        //First name errors
        case .firstNameMissing:
            title = "First Name Missing"
            message = "Please enter your first name"
        case .firstNameTooShort:
            title = "First Name Too Short"
            message = "Your first name has to be longer than 2 letters. Please try again"
        //Last name errors
        case .lastNameMissing:
            title = "Last Name Missing"
            message = "Please enter your last name"
        case .lastNameTooShort:
            title = "Last Name Too Short"
            message = "Your last name has to be longer than 2 letters. Please try again"
        //Email errors
        case .emailMissing:
            title = "Email Missing"
            message = "Please enter an email"
        case .emailNotWellFormatted:
            title = "Email Error"
            message = "Email invalid. Please try again"
        //Password errors
        case .passwordMissing:
            title = "Password Missing"
            message = "Please enter a password"
        case .passwordTooShort:
            title = "Password Too Short"
            message = "Password too short (less than 8 characters). Please try again"
        case .confirmPasswordMissing:
            title = "Confirm Password"
            message = "Please confirm your password"
        case .confirmPasswordTooShort, .passwordMissmatch:
            title = "Password Missmatch"
            message = "Passwords do not match. Please try again"
        }
        
        let errorAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        //OK button
        let okButton = UIAlertAction(title: "OK", style: .default, handler: nil)
        errorAlertController.addAction(okButton)
        
        //Display alert
        formViewController.present(errorAlertController, animated: true, completion: nil)
    }

}

public enum FormError: Error {
    
    //Contains errors that can occur when filling out a form
    
    //First name errors
    case firstNameMissing
    case firstNameTooShort
    
    //Last name errors
    case lastNameMissing
    case lastNameTooShort
    
    //Email errors
    case emailMissing
    case emailNotWellFormatted
    
    //Password errors
    case passwordMissing
    case passwordTooShort
    case confirmPasswordMissing
    case confirmPasswordTooShort
    case passwordMissmatch
    
}
