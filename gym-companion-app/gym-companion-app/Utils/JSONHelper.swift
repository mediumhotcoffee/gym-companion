//
//  JSONHelper.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/30/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

class JSONHelper {
    
    private let folderName: String = "json"
    private let fileName: String = "routineJson"
    private let fileExtension: String = ".json"
    
    var routineExercises: [String:[Exercise]]
    var routines: [Routine]
    
    init() {
        
        routineExercises = [:]
        routines = []
    }
    
    func saveCurrentUserRoutines() {
        
        FirebaseWorkoutsUitl.pullUserRoutinesIDs(delegate: self)
    }
    
    func readCurrentUserRoutinesJSON() -> [String:Any] {
        
        print("Reading file... ")
        
        let fileManager = FileManager.default
        
        if let documentDirectory: URL = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let folderURL: URL =  documentDirectory.appendingPathComponent(folderName)
            let jsonFileURL: URL = folderURL.appendingPathComponent("\(fileName)\(fileExtension)")
            
            do {
                
                let jsonReadData: Data = try Data(contentsOf: jsonFileURL)
                
                if jsonReadData.count > 0 {
                    
                    do {
                        
                        if let routinesJSON  = try JSONSerialization.jsonObject(with: jsonReadData, options: .mutableContainers) as? [String: Any] {
                            
                            print("Routines JSON Parsed.\r\n\(routinesJSON.debugDescription)")
                            
                            return routinesJSON
                        }
                    }
                    catch {
                        print("Couldn't parse routines. readJsonData(), JsonHelper.\r\n\(error.localizedDescription)")
                    }
                }
            }
            catch let error as NSError {
                print("Couldn't read json file.\r\n\(error.localizedDescription)")
            }
        }
        
        return [:]
    }
    
    private func createFolder() -> String {
        
        let fileManager = FileManager.default
        
        if let documentDirectory: URL = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let folderPath: URL =  documentDirectory.appendingPathComponent(folderName)
            
            if !fileManager.fileExists(atPath: folderPath.path) {
                
                do {
                    
                    try fileManager.createDirectory(atPath: folderPath.path, withIntermediateDirectories: true, attributes: nil)
                }
                catch {
                    print("Couldn't create document directory. createFolder, JsonHelper.\r\n\(error)")
                }
            }
            
            return folderPath.path
        }
        
        return "Error"
    }
    
    private func getJSONObject() -> [String:Any] {
        
        var routinesDictionary: [String:Any] = [:]
        
        for (key, value) in routineExercises {
            
            var exerciseDictionaryArray: [[String:Any]] = []
            for exercise in value {
                print("\t\(exercise.name)")
                
                var exerciseDictionary: [String:Any] = [:]
                exerciseDictionary["name"] = exercise.name
                exerciseDictionary["sets"] = exercise.sets
                exerciseDictionary["reps"] = exercise.reps
                exerciseDictionary["restTime"] = exercise.restTimeSeconds
                
                exerciseDictionaryArray += [exerciseDictionary]
            }
            
            let routineExercisesDictionary: [String:Any] = ["exercises":exerciseDictionaryArray]
            routinesDictionary[key] = routineExercisesDictionary
        }
        
        return ["routines":routinesDictionary]
    }
    
    private func saveFile(jsonData: Data, folderPath: String) {
        
        let folderURL: URL = URL(fileURLWithPath: folderPath)
        let fileURL: URL = folderURL.appendingPathComponent("\(fileName)\(fileExtension)")
        
        do {
            try jsonData.write(to: fileURL)
        }
        catch {
            print("Couldn't create json file. saveFile(folderPath:), JsonHelper.\r\n\(error.localizedDescription)")
        }
    }
}

extension JSONHelper: PullRoutinesDelegate, PullRoutineExercisesDelegate {
   
    func routineIDsPulled(routineIDs: [String]) {
        
        FirebaseWorkoutsUitl.pullUserRoutines(routineIDs: routineIDs, delegate: self)
    }
    
    func routineObjectsPulled(routines: [Routine], browseObjectType: BrowseObjectType, totalToPull: Int) {
        
        for routine in routines {
            
            FirebaseWorkoutsUitl.pullRoutineExercises(routine: routine, delegate: self)
        }
    }
    
    func routineExerciseObjectsPulled(routine: Routine, exercises: [Exercise], browseObjectType: BrowseObjectType, totalToPull: Int) {
        
        if let previousExercises = routineExercises[routine.name] {
            
            routineExercises[routine.name] = previousExercises + exercises
        }
        else {
            
            routineExercises[routine.name] = exercises
        }
        
        let routinesJSON: [String:Any] = getJSONObject()
        
        do {
            
            let jsonData = try JSONSerialization.data(withJSONObject: routinesJSON, options: [.prettyPrinted])
            
            // Save json to a file. Overwrite if already exists.
            let folderPath: String = createFolder()
            saveFile(jsonData: jsonData, folderPath: folderPath)
        }
        catch {
            print(error.localizedDescription)
        }
    }
}
