//
//  AppDelegate.swift
//  gym-companion-app
//
//  Created by Marcos Felipe Costa da Silva on 6/5/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import Firebase
import WatchConnectivity

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var routinesJSON: [String:Any] = JSONHelper().readCurrentUserRoutinesJSON()
    
    var session: WCSession? {
        
        didSet{
            
            if let session = session {
                
                session.delegate = self
                session.activate()
            }
        }
    }
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //Setup Firebase Core
        FirebaseApp.configure()
        
        // Create a session to handle watch data.
        session = WCSession.isSupported() ? WCSession.default : nil
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

// Extends the AppDelegate to conform the WCSessionDelegate protocol.
extension AppDelegate: WCSessionDelegate {
    
    func sessionDidDeactivate(_ session: WCSession) {
        // Nothing needs to happen here, but it is required.
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        // Nothing needs to happen here, but it is required.
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        // Nothing needs to happen here, but it is required.
    }
    
    // Returns a list of heros through the session.
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        
        DispatchQueue.main.async {
            
            let getRoutines: String = "getRoutines"
            let setRoutines: String = "setRoutines"
            let newRoutines: String = "newRoutines"
            
            // Checks to see if the message was the getPlayers message.
            // If so, will need to return the players.
            if (message[getRoutines] as? Bool) != nil {
                
                self.routinesJSON = JSONHelper().readCurrentUserRoutinesJSON()
                
                // Archives the routines and send them back through the replyHandler.
                do {
                    
                    let data =  try NSKeyedArchiver.archivedData(withRootObject: self.routinesJSON as [String:Any], requiringSecureCoding: false)
                    replyHandler([newRoutines: data])
                }
                catch {
                    print("Error archiving routines. session(didReceiveMessage:), AppDelegate extension.")
                }
            }
            else if let routinessData = (message[setRoutines] as? Data) {
                
                DispatchQueue.main.async {
                    
                    do {
                        
                        if let routines = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(routinessData) as? [String:Any] {
                            
                            self.routinesJSON = routines
                            JSONHelper().saveCurrentUserRoutines()
                            //JsonHelper.writeJsonFile(with: routines)
                            replyHandler([newRoutines: Data([1])])
                        }
                    }
                    catch {
                        print("Couldn't unarchive routines.\r\n\(error.localizedDescription)")
                    }
                }
            }
        }
    }
}
