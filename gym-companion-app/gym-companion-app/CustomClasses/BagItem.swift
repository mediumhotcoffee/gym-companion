//
//  BagItem.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/10/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

class BagItem {
    
    //MARK: - Properties
    var itemIconID: String?
    var itemName: String
    
    //MARK: - Computed Properties
    var asDict: [String:Any] {
        
        return [
            
            "itemIconID": itemIconID ?? "none",
            "itemName": itemName
        ]
    }
    
    //MARK: - Initializers
    init(itemIconID: String?, itemName: String) {
        
        self.itemIconID = itemIconID
        self.itemName = itemName
    }
    
    init?(fromNSDictionary bagItemValue: NSDictionary) {
        
        guard let itemIconID = bagItemValue["itemIconID"] as? String,
            let itemName = bagItemValue["itemName"] as? String
        else {
                
                print("\r\n\r\n------------------ GymBag.swift | init?(bagValue: NSDictionary, bagID: String) -> Error parsing NSDictionary object \(bagItemValue.description) to GymBag object. ------------------\r\n\r\n")
                return nil
        }
        
        self.itemIconID = itemIconID
        self.itemName = itemName
    }
}
