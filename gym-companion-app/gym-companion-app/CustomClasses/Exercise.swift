//
//  Exercise.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 6/27/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

class Exercise {
    
    //MARK: - Properties
    var exerciseID: String
    var creatorID: String
    var name: String
    var categories: [WorkoutCategory]
    var weight: Int?
    var restTimeSeconds: Int
    var sets: Int
    var reps: Int
    
    //MARK: - Computed properties
    var asDict: [String:Any] {
        return [
            "creatorID": creatorID,
            "exerciseName": name,
            "categories": categoriesStringArray,
            "weight": weight ?? "none",
            "restTimeSeconds": restTimeSeconds,
            "sets": sets,
            "reps": reps
        ]
    }
    
    var categoriesStringArray: [String] {
        var categoriesArray: [String] = []
        
        for category in categories {
            categoriesArray += [category.categoryName]
        }
        
        return categoriesArray
    }
    
    //MARK: - Initializers
    init(exerciseID: String, creatorID: String, name: String, categories: [WorkoutCategory], weight: Int?, restTimeSeconds: Int, sets: Int, reps: Int) {
        self.exerciseID = exerciseID
        self.creatorID = creatorID
        self.name = name
        self.categories = categories
        self.weight = weight
        self.restTimeSeconds = restTimeSeconds
        self.sets = sets
        self.reps = reps
    }
    
    init?(fromNSDictionary exerciseValue: NSDictionary, exerciseID: String) {
        //Firebase initializer
        guard let creatorID = exerciseValue["creatorID"] as? String,
            let name = exerciseValue["exerciseName"] as? String,
            let categoriesStringArray = exerciseValue["categories"] as? [String],
            let restTime = exerciseValue["restTimeSeconds"] as? Int,
            let reps = exerciseValue["reps"] as? Int,
            let sets = exerciseValue["sets"] as? Int
        else {
                
            print("\r\n\r\n------------------ Exercise.swift | init?(exerciseValue: NSDictionary, exerciseID: String) -> Error parsing NSDictionary object \(exerciseID) to Exercise object. ------------------\r\n\r\n")
                return nil
        }
        
        self.exerciseID = exerciseID
        self.creatorID = creatorID
        self.name = name
        
        self.categories = []
        for categoryString in categoriesStringArray {
            
            if let validCategory = WorkoutCategory.getCategoryFromString(categoryString: categoryString) {
                
                self.categories += [validCategory]
            }
        }
        
        self.restTimeSeconds = restTime
        self.reps = reps
        self.sets = sets
        
        // Weight and Status
        if let weight = exerciseValue["weight"] as? Int {
            
            self.weight = weight
        }
    }
    
}
