//
//  RoutinesTableViewHandler.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/25/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

class CollectionObjectsTableViewHandler: NSObject {
    
    var delegate: TableViewCellObjectDelegate
    var collectionObjects: [Any]
    var singleItemNames: [String:String]
    var cellImages: [String:UIImage]
    var isTrainer: Bool
    
    init(delegate: TableViewCellObjectDelegate, collectionObjects: [Any], singleItemNames: [String:String], cellImages: [String:UIImage], isTrainer: Bool) {
        
        self.delegate = delegate
        self.collectionObjects = collectionObjects
        self.singleItemNames = singleItemNames
        self.cellImages = cellImages
        self.isTrainer = isTrainer
    }
    
    private func getSingleItemNames(nameIDs: [String]) -> String {
        
        var exerciseNamesString = ""
        
        for (index, id) in nameIDs.enumerated() {
            
            if let name = singleItemNames[id] {
                
                if index == nameIDs.count - 1 {
                    
                    exerciseNamesString += name
                }
                else {
                    
                    exerciseNamesString += "\(name) | "
                }
            }
        }
        
        return exerciseNamesString
    }
    
    private func getSingleItemNames(bagItems: [BagItem]) -> String {
        
        var itemNames: String = ""
        
        for (index, item) in bagItems.enumerated() {
            
            if index == bagItems.count - 1 {
                
                itemNames += item.itemName
            }
            else {
                
                itemNames += "\(item.itemName) | "
            }
        }
        
        return itemNames
    }
    
    private func getRoutineCell(indexPath: IndexPath, tableView: UITableView) -> UICollectionObjectTableViewCell? {
        
        if let routine = collectionObjects[indexPath.row] as? Routine,
            let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.collectionCell.stringValue, for: indexPath) as? UICollectionObjectTableViewCell {
            
            cell.collectionTitleLabel.text = routine.name
            cell.contentsLabel.text = getSingleItemNames(nameIDs: routine.exercises)
            
            if let iconID = routine.routineIconID, let image = cellImages[iconID] {
                
                cell.iconImageView.image = image
            }
            
            return cell
        }
        
        return nil
    }
    
    private func getShareableRoutineCell(indexPath: IndexPath, tableView: UITableView) -> UIShareableCollectionObjectTableViewCell? {
        
        if let routine = collectionObjects[indexPath.row] as? Routine,
            let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.collectionCell.stringValue, for: indexPath) as? UIShareableCollectionObjectTableViewCell  {
            
            cell.titleLabel.text = routine.name
            cell.contentsLabel.text = getSingleItemNames(nameIDs: routine.exercises)
            cell.delegate = delegate
            cell.indexPath = indexPath
            
            if let iconID = routine.routineIconID, let image = cellImages[iconID] {
                
                cell.cellImageView.image = image
            }
            
            return cell
        }
        
        return nil
    }
    
    private func getShareableGymBagCell(indexPath: IndexPath, tableView: UITableView) -> UIShareableCollectionObjectTableViewCell? {
        
        if let bag = collectionObjects[indexPath.row] as? GymBag,
            let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.collectionCell.stringValue, for: indexPath) as? UIShareableCollectionObjectTableViewCell {
            
            cell.titleLabel.text = bag.bagName
            cell.contentsLabel.text = getSingleItemNames(bagItems: bag.bagItems)
            cell.delegate = delegate
            cell.indexPath = indexPath
            
            if let iconID = bag.bagIconID, let image = cellImages[iconID] {
                
                cell.cellImageView.image = image
            }
            
            return cell
        }
        
        return nil
    }
    
    private func getGymBagCell(indexPath: IndexPath, tableView: UITableView) -> UICollectionObjectTableViewCell? {
        
        if let bag = collectionObjects[indexPath.row] as? GymBag,
            let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.collectionCell.stringValue, for: indexPath) as? UICollectionObjectTableViewCell {
            
            cell.collectionTitleLabel.text = bag.bagName
            cell.contentsLabel.text = getSingleItemNames(bagItems: bag.bagItems)
            
            if let iconID = bag.bagIconID, let image = cellImages[iconID] {
                
                cell.iconImageView.image = image
            }
            
            return cell
        }
        
        return nil
    }
}

extension CollectionObjectsTableViewHandler: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return collectionObjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell = UITableViewCell(style: .default, reuseIdentifier: "cellID")
        
        if isTrainer, let shareCell = getShareableRoutineCell(indexPath: indexPath, tableView: tableView) {
            
            cell = shareCell
        }
        else if !isTrainer, let collectionCell = getRoutineCell(indexPath: indexPath, tableView: tableView) {
            
            cell = collectionCell
        }
        else if isTrainer, let shareCell = getShareableGymBagCell(indexPath: indexPath, tableView: tableView) {
            
            cell = shareCell
        }
        else if !isTrainer, let collectionCell = getGymBagCell(indexPath: indexPath, tableView: tableView) {
            
            cell = collectionCell
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            // Remove the routine and routine IDs from the stored array.
            collectionObjects.remove(at: indexPath.row)
            
            delegate.cellDeleted(indexPath: indexPath)
            
            // Remove this row from the tableview.
            //tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Only breifly highlight the selection, letting the user know they indeed selected that item, but at the same time not making the selection appear "stuck" or "frozen."
        tableView.deselectRow(at: indexPath, animated: true)
        
        delegate.objectSelected(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            // Ipad
            return 150;
        }
        else
        {
            // Iphone
            return 85;
        }
    }
}
