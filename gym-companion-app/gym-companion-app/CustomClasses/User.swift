//
//  User.swift
//  gym-companion-app
//
//  Created by Marcos Silva on 6/27/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

class User {
    
    //MARK: - Properties
    var userImageID: String?
    var firstName: String
    var lastName: String
    var isTrainer: Bool
    var paid: Bool
    var email: String
    var trainerID: String?
    var routines: [String]
    var exercises: [String]
    var bags: [String]
    private var traineeIDs: [String]
    //TODO: Add gym time
    
    var trainees: [String] {
        get {
            if isTrainer {
                return traineeIDs
            } else {
                return []
            }
        }
        
        set {
            if isTrainer {
                traineeIDs = newValue
            }
        }
    }
    
    //MARK: - Computed properties
    var asDict: [String:Any] {
        return [
            "userImageID": userImageID ?? "none",
            "firstName": firstName,
            "lastName": lastName,
            "isTrainer": isTrainer,
            "paid": paid,
            "email": email,
            "trainerID": trainerID ?? "none",
            "routines": routines,
            "exercises": exercises,
            "gymBags": bags,
            "trainees": trainees
        ]
    }
    
    //MARK: - Initializers
    init (userImageID: String?, firstName: String, lastName: String, isTrainer: Bool, paid: Bool, email: String, trainerID: String?, routines: [String], exercises: [String], bags: [String], trainees: [String]) {
        
        self.userImageID = userImageID
        self.firstName = firstName
        self.lastName = lastName
        self.isTrainer = isTrainer
        self.paid = paid
        self.email = email
        self.trainerID = trainerID
        self.routines = routines
        self.exercises = exercises
        self.bags = bags
        self.traineeIDs = trainees
    }
    
    init?(fromNSDictionary userValue: NSDictionary) {
        guard let imageID = userValue["userImageID"] as? String?,
            let firstName = userValue["firstName"] as? String,
            let lastName = userValue["lastName"] as? String,
            let isTrainer = userValue["isTrainer"] as? Bool,
            let paid = userValue["paid"] as? Bool,
            let email = userValue["email"] as? String,
            let trainerID = userValue["trainerID"] as? String?
        else {
            
            print("\r\n\r\n------------------ User.swift | init?(userValue: NSDictionary) -> Error parsing NSDictionary object to User object. ------------------\r\n\r\n")
            return nil
        }
        
        self.userImageID = imageID
        self.firstName = firstName
        self.lastName = lastName
        self.isTrainer = isTrainer
        self.paid = paid
        self.email = email
        self.trainerID = trainerID
        
        if let routines = userValue["routines"] as? [String] {
            self.routines = routines
        } else {
            self.routines = []
        }
        
        if let exercises = userValue["exercises"] as? [String] {
            self.exercises = exercises
        } else {
            self.exercises = []
        }
        
        if let bags = userValue["gymBags"] as? [String] {
            self.bags = bags
        } else {
            self.bags = []
        }
        
        if let trainees = userValue["trainees"] as? [String] {
            self.traineeIDs = trainees
        } else {
            self.traineeIDs = []
        }
    }
}
