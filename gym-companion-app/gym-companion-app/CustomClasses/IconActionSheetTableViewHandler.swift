//
//  ActionSheetTableViewSource.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/15/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

class IconActionSheetTableViewHandler: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var iconLocations: [String]
    var iconImages: [String:UIImage]
    
    init(iconLocations: [String], iconImages: [String:UIImage]) {
        
        self.iconLocations = iconLocations
        self.iconImages = iconImages
        super.init()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: .default, reuseIdentifier: "alertCellID")
        
        if let image = iconImages[iconLocations[indexPath.row]] {
            cell.imageView?.image = image
        }
        else {
            cell.textLabel?.text = iconLocations[indexPath.row]
        }
        
        cell.backgroundColor = UIColor.clear
        
        let view: UIView = UIView(frame: cell.frame)
        view.alpha = 0.5
        view.backgroundColor = UIColor.appGreen
        
        cell.selectedBackgroundView = view
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return iconImages.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.cellForRow(at: indexPath)?.textLabel?.textColor = UIColor.appWhite
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        tableView.cellForRow(at: indexPath)?.textLabel?.textColor = UIColor.appDarkColor
    }
}
