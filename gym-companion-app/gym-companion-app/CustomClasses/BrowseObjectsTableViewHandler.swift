//
//  BrowseObjectsTableViewHandler.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/17/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

class BrowseObjectsTableViewHandler: NSObject {
    
    var delegate: TableViewCellObjectDelegate
    var expandableCells: [ExpandableCellObject]
    var singleObjectNames: [String:String]
    var cellImages: [String:UIImage]
    
    init(expandableCells: [ExpandableCellObject], singleObjectNames: [String:String], cellImages: [String:UIImage], delegate: TableViewCellObjectDelegate) {
        
        self.expandableCells = expandableCells
        self.singleObjectNames = singleObjectNames
        self.cellImages = cellImages
        self.delegate = delegate
    }
}

extension BrowseObjectsTableViewHandler {
    
    private func getRoutineCell(tableView: UITableView, indexPath: IndexPath, routine: Routine) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.collectionCell.stringValue, for: indexPath) as? UICollectionObjectTableViewCell {
            
            cell.collectionTitleLabel.text = routine.name
            cell.contentsLabel.text = getExerciseNames(exerciseIDs: routine.exercises)
            
            if let iconID = routine.routineIconID, let image = cellImages[iconID] {
                
                cell.iconImageView.image = image
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    private func getExerciseNames(exerciseIDs: [String]) -> String {
        
        var exerciseNames = ""
        
        for (index, id) in exerciseIDs.enumerated() {
            
            if let name = singleObjectNames[id] {
                
                if index == exerciseIDs.count - 1 {
                    
                    exerciseNames += name
                }
                else {
                    
                    exerciseNames += "\(name) | "
                }
            }
        }
        
        return exerciseNames
    }
    
    private func getGymBagCell(tableView: UITableView, indexPath: IndexPath, gymBag: GymBag) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.collectionCell.stringValue, for: indexPath) as? UICollectionObjectTableViewCell {
            
            cell.collectionTitleLabel.text = gymBag.bagName
            cell.contentsLabel.text = getBagItemNames(bagItems: gymBag.bagItems)
            
            if let iconID = gymBag.bagIconID, let image = cellImages[iconID] {
                
                cell.iconImageView.image = image
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    private func getBagItemNames(bagItems: [BagItem]) -> String {
        
        var bagItemNames = ""
        
        for (index, item) in bagItems.enumerated() {
            
            if index == bagItems.count - 1 {
                
                bagItemNames += item.itemName
            }
            else {
                
                bagItemNames += "\(item.itemName) | "
            }
        }
        
        return bagItemNames
    }
}

extension BrowseObjectsTableViewHandler: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return expandableCells.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return expandableCells[section].objects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let object: Any = expandableCells[indexPath.section].objects[indexPath.row]
        var cell = UITableViewCell(style: .default, reuseIdentifier: "defaultCellID")
        
        switch object {
            
        case is Routine:
            
            cell = getRoutineCell(tableView: tableView, indexPath: indexPath, routine: object as! Routine)
        case is Exercise:
            cell.textLabel?.text = (object as! Exercise).name
            cell.textLabel?.textColor = .appDarkColor
            
            if (UIDevice.current.userInterfaceIdiom == .pad)
            {
                // Ipad
                cell.textLabel?.font = UIFont.systemFont(ofSize: 26, weight: .regular)
            }
            else
            {
                // Iphone
                cell.textLabel?.font = UIFont.systemFont(ofSize: 20, weight: .regular)
            }
            
            cell.backgroundView?.backgroundColor = .appTransparentWhite
            cell.backgroundColor = .appTransparentWhite
        case is GymBag:
            
            cell = getGymBagCell(tableView: tableView, indexPath: indexPath, gymBag: object as! GymBag)
        case is BagItem:
            cell.textLabel?.text = (object as! BagItem).itemName
            cell.textLabel?.textColor = .appDarkColor
            
            if (UIDevice.current.userInterfaceIdiom == .pad)
            {
                // Ipad
                cell.textLabel?.font = UIFont.systemFont(ofSize: 26, weight: .regular)
            }
            else
            {
                // Iphone
                cell.textLabel?.font = UIFont.systemFont(ofSize: 20, weight: .regular)
            }
            
            cell.backgroundView?.backgroundColor = .appTransparentWhite
            cell.backgroundColor = .appTransparentWhite
        default:
            print("\rBrowsableObjectsTableViewHandler Error:\r\tUnexpected object type.")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return expandableCells[section].title
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Only breifly highlight the selection, letting the user know they indeed selected that item, but at the same time not making the selection appear "stuck" or "frozen."
        tableView.deselectRow(at: indexPath, animated: true)
        
        // Determine whether passing back a ready-to-go routine or a user created routine.
        let objectType: BrowseObjectType = indexPath.section == 0 && expandableCells.count > 1 ? .user : .browsable
        
        // Pass back the object selected at the cell.
        delegate.objectSelected(object: expandableCells[indexPath.section]
            .objects[indexPath.row], objectType: objectType)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            // Ipad
            return 150
        }
        else
        {
            // Iphone
            return 85
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            // Ipad
            return 80
        }
        else
        {
            // Iphone
            return 60
        }
    }
    
    //TODO: Use this once a custom header view is created.
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let label = UILabel()
        label.text = self.tableView(tableView, titleForHeaderInSection: section)
        label.textColor = .appWhite
        
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            // Ipad
            label.font = UIFont.systemFont(ofSize: 40, weight: .light)
            label.frame = CGRect(x: 20, y: 8, width: 320, height: 70)
        }
        else
        {
            // Iphone
            label.font = UIFont.systemFont(ofSize: 30, weight: .light)
            label.frame = CGRect(x: 20, y: 8, width: 320, height: 50)
        }
        
        let headerView = UIView()
        headerView.addSubview(label)
        
        return headerView
    }
}
