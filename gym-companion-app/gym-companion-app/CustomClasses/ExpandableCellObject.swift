//
//  ExpandableCellObject.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/17/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

struct ExpandableCellObject {
    
    var title: String
    var icon: UIImage?
    var objects: [Any]
    var isExpanded: Bool
    
    init(title: String, icon: UIImage?, objects: [Any], isExpanded: Bool) {
        
        self.title = title
        self.icon = icon
        self.objects = objects
        self.isExpanded = isExpanded
    }
}
