//
//  TraineeListActionSheetDelegate.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/26/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

class TraineeListActionSheetTableViewHandler: NSObject {
    
    var trainees: [User]
    
    init(trainees: [User]) {
        
        self.trainees = trainees
    }
}

extension TraineeListActionSheetTableViewHandler: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return trainees.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = UITableViewCell(style: .default, reuseIdentifier: "alertCellID")
        let user: User = trainees[indexPath.row]
        
        cell.textLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        cell.textLabel?.textColor = .appDarkColor
        cell.textLabel?.text = "\(user.firstName) \(user.lastName)"
        
        cell.backgroundView = UIView()
        cell.backgroundColor = .clear
        
        let view: UIView = UIView()
        view.backgroundColor = .appGreen
        view.alpha = 0.7
        cell.selectedBackgroundView = view
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) {

            cell.textLabel?.textColor = .white
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) {

            cell.textLabel?.textColor = .appDarkColor
        }
    }
}
