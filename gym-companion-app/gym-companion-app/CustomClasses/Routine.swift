//
//  Routine.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 6/27/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

class Routine {
    
    //MARK: - Properties
    var routineID: String
    var routineIconID: String?
    var creatorID: String
    var name: String
    var categories: [WorkoutCategory]
    var exercises: [String]
    
    //MARK: - Computed properties
    var asDict: [String:Any] {
        
        return [
            "creatorID": creatorID,
            "routineIconID": routineIconID ?? "none",
            "routineName": name,
            "categories": categoriesStringArray,
            "exercises": exercises
        ]
    }
    
    var categoriesStringArray: [String] {
        var categoriesArray: [String] = []
        
        for category in categories {
            
            categoriesArray += [category.categoryName]
        }
        
        return categoriesArray
    }
    
    //MARK: - Initializers
    init (routineID: String, routineIconID: String?, routineName: String, creatorID: String, categories: [WorkoutCategory], exercises: [String]) {
        //Regular init
        self.routineID = routineID
        self.routineIconID = routineIconID
        self.name = routineName
        self.creatorID = creatorID
        self.categories = categories
        self.exercises = exercises
    }
    
    init?(fromNSDictionary routineValue: NSDictionary, routineID: String) {
        //Firebase initializer
        guard let creatorID = routineValue["creatorID"] as? String,
            let name = routineValue["routineName"] as? String,
            let categoriesStringArry = routineValue["categories"] as? [String]
        else {
                
            print("\r\n\r\n------------------ Routine.swift | init?(routineValue: NSDictionary, routineID: String) -> Error parsing NSDictionary object to Routine object. ------------------\r\n\r\n")
                return nil
        }
        
        self.routineID = routineID
        self.creatorID = creatorID
        self.name = name
        
        self.categories = []
        for categoryString in categoriesStringArry {
            
            let category: WorkoutCategory? = WorkoutCategory.getCategoryFromString(categoryString: categoryString)
            
            if let validCategory = category {
                
                self.categories += [validCategory]
            }
        }
        
        self.exercises = []
        if let exercises = routineValue["exercises"] as? [String] {
            
            for exercise in exercises {
                
                self.exercises += [exercise]
            }
        }
        
        self.routineIconID = routineValue["routineIconID"] as? String
    }
    
    //MARK: - Custom Methods
    //TODO: Create a static method to collect the exercise IDs when first creating the routine.
    
}
