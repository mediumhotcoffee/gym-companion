//
//  GymBag.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/10/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

class GymBag {
    
    //MARK: - Properties
    var bagID: String
    var creatorID: String
    var bagIconID: String?
    var bagName: String
    var bagItems: [BagItem]
    
    //MARK: - Computed Properties
    var asDict: [String:Any] {

        return [
            
            "bagIconID": bagIconID ?? "none",
            "creatorID": creatorID,
            "bagName": bagName,
            "bagItems": bagItemsAsDictArray
        ]
    }
    
    var bagItemsAsDictArray: [[String:Any]] {
        
        var dictionaryArray: [[String:Any]] = []
        
        for item in bagItems {
            
            dictionaryArray += [item.asDict]
        }
        
        return dictionaryArray
    }
    
    init(bagID: String, creatorID: String, bagIconID: String?, bagName: String, bagItems: [BagItem]) {
        
        self.bagID = bagID
        self.creatorID = creatorID
        self.bagIconID = bagIconID
        self.bagName = bagName
        self.bagItems = bagItems
    }
    
    init?(fromNSDictionary bagValue: NSDictionary, bagID: String) {
        
        guard let bagIconID = bagValue["bagIconID"] as? String,
            let creatorID = bagValue["creatorID"] as? String,
            let bagName = bagValue["bagName"] as? String
        else {
            
            print("\r\n\r\n------------------ GymBag.swift | init?(bagValue: NSDictionary, bagID: String) -> Error parsing NSDictionary object \(bagID) to GymBag object. ------------------\r\n\r\n")
            return nil
        }
        
        self.bagID = bagID
        self.creatorID = creatorID
        self.bagIconID = bagIconID
        self.bagName = bagName
        
        self.bagItems = []
        
        if let bagItems = bagValue["bagItems"] as? [NSDictionary] {
            
            for itemDictionary in bagItems {
                
                if let itemObject = BagItem(fromNSDictionary: itemDictionary) {
                    
                    self.bagItems += [itemObject]
                }
            }
        }
    }
}
