//
//  SharedModalViewController.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 8/7/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

class SharedModalViewController: UIViewController {

    @IBOutlet weak var sentMessge: UILabel!
    @IBOutlet weak var sentImage: UIImageView!
    
    @IBOutlet weak var sentBackgroundView: UIView!
    
    var message: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = sentBackgroundView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        sentBackgroundView.addSubview(blurEffectView)
        // Do any additional setup after loading the view.
        
        sentBackgroundView.layer.cornerRadius = 30
        sentBackgroundView.layer.masksToBounds = true
        
        sentMessge.text = message
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func setSentMessage(traineeFirstName: String) {
        
        message = "Sent to \(traineeFirstName)"
    }
}
