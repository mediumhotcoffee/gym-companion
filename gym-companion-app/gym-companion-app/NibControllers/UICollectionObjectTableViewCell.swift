//
//  UICollectionObjectTableViewCell.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/18/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

class UICollectionObjectTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionTitleLabel: UILabel!
    @IBOutlet weak var contentsLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var cellBackgroundView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        cellBackgroundView.layer.cornerRadius = 15
        cellBackgroundView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            
            cellBackgroundView.backgroundColor = .appGreen
        }
        else {
            
            cellBackgroundView.backgroundColor = .appTransparentWhite
        }
    }
}
