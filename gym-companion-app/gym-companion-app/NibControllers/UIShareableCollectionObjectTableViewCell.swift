//
//  UIShareableCollectionObjectTableViewCell.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/25/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

class UIShareableCollectionObjectTableViewCell: UITableViewCell {

    @IBOutlet weak var cellBackgroundView: UIView!
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentsLabel: UILabel!
    
    var delegate: TableViewCellObjectDelegate?
    var indexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cellBackgroundView.layer.cornerRadius = 15
        cellBackgroundView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            
            cellBackgroundView.backgroundColor = .appGreen
        }
        else {
            
            cellBackgroundView.backgroundColor = .appTransparentWhite
        }
    }
    
    @IBAction func shareTapped(_ sender: UIButton) {
        
        if let delegate = delegate, let indexPath = indexPath {
            
            delegate.shareTapped(sender, indexPath: indexPath)
        }
    }
}
