//
//  RoutineTimerViewController.swift
//  gym-companion-app
//
//  Created by Marcos Felipe Costa da Silva on 7/11/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class SelectRoutineTimerViewController: UIViewController, EmptyListDelegate {

    //MARK: - Views
    //TableView
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Routine Variables
    var ref: DatabaseReference!
    var routineIDs: [String] = []
    var routines: [Routine] = []
    var selectedRoutine: Routine?
    
    //MARK: - User Variables
    var userID: String = "" //TODO: Fix this with a default value
    
    //MARK: - Lifecycle methods
    override func viewWillAppear(_ animated: Bool) {
        //Setup tableView
        (tableView.dataSource, tableView.delegate) = (self, self)
        setUpEmptyView()
        
        //Fill out table view
        loadRoutineIDs()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //Get a reference to the database
        ref = Database.database().reference()
        
        //Get user
        if let currentUser = Auth.auth().currentUser {
            userID = currentUser.uid
        }
    }
    
    //MARK: - Actions
    @IBAction func backTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - EmptyListView Methods
    private func emptyViewFromNib() -> UIEmptyListView {
        //Returns a UIEmptyListView from the custom nib file
        let nib = UINib(nibName: "UIEmptyListView", bundle: nil)
        
        return nib.instantiate(withOwner: self, options: nil).first as! UIEmptyListView
    }
    
    private func setUpEmptyView() {
        //Displays the empty list message with a button to add routines
        //This is done by using the UIEmptyListView nib file
        let emptyView: UIEmptyListView = emptyViewFromNib()
        emptyView.setupView(listType: .routine)
        emptyView.delegate = self
        
        //Hide the TableView
        tableView.backgroundView = emptyView
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
    }
    
    func showAddOptions(_ sender: UIButton) {
        //Takes the user to Workouts if they have no routines
        let alertController = UIAlertController(title: "No Routine", message: "You do not seem to have any routines. Do you want to create one?", preferredStyle: .actionSheet)
        
        //TODO: Segue to Workouts screen
        let createButton = UIAlertAction(title: "Create Routine", style: .default, handler: { (action) -> Void in
            self.tabBarController?.selectedIndex = 0
        })
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(createButton)
        alertController.addAction(cancelButton)
        
        present(alertController, animated: true, completion: nil)
    }
    
    private func setUpTableView() {
        //Start with empty arrays so duplicates are not created
        routines = []
        routineIDs = []
        
        // Make sure the background is clear, which will show the background color of the workouts screen.
        tableView.backgroundColor = .clear
        tableView.backgroundView = UIView(frame: tableView.frame)
        
        // Make sure the separators are visible (they could be hidden from showing the empty state.)
        tableView.separatorStyle = .singleLine
        
        // Make sure the changes are shown.
        tableView.reloadData()
    }
    
    //MARK: - Custom methods
    private func loadRoutineIDs() {
        // If we have a user, this can be done because we should have a user and a connection
        self.setUpTableView()
        
        // Drill into the "users" route and then into the current user in the database.
        ref.child("users").child(userID).observeSingleEvent(of: .value, with: { (userSnapshot) in
            // Make sure the user at this location is valid
            if let userValue = userSnapshot.value as? NSDictionary {
                //Try to instantiate a User object from the NSDictionary
                //object pulled down from Firebase
                if let currentUserObject = User(fromNSDictionary: userValue) {
                    //Update the routineIDs array with the user's routines
                    //so the tableview can be populated.
                    self.routineIDs = currentUserObject.routines
                    
                    if self.routineIDs.count > 0 {
                        // If there are routines, show them
                        self.showRoutines()
                    }
                }
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    private func showRoutines() {
        // Loop through all the routineIDs pulled from the Firebase user
        for id in routineIDs {
            // Drill into the "routines" root, then into this routine.
            ref.child("routines").child(id).observeSingleEvent(of: .value) { (routineSnapshot) in
                // Make sure the routine at this location is valid.
                if let routineDictionary = routineSnapshot.value as? NSDictionary {
                    // Attempt to instantiate a Routine object from the
                    //NSDictionary pulled from Firebase
                    if let routineObject = Routine(fromNSDictionary: routineDictionary, routineID: routineSnapshot.key) {
                        // If a Routine object was created successfully
                        //add it to the array of routines and show it in the tableview
                        self.routines += [routineObject]
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    //MARK: - Overrides
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case Segue.toRoutineTimer.stringValue:
            if let destination = segue.destination as? RoutineTimerViewController {
                destination.routineToTime = selectedRoutine
            }
        default:
            break
        }
    }
    
}

//MARK: - Extensions
extension SelectRoutineTimerViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return routines.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // TODO: Create a cell nib and return an appropriate cell.
        let cell = tableView.dequeueReusableCell(withIdentifier: "routineCell", for: indexPath)
        
        // Set the text and the text color for the cell. Text should match the name of the routine to be displayed in this cell.
        cell.textLabel?.text = routines[indexPath.row].name
        
        cell.textLabel?.textColor = .appWhite
        
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            // Ipad
            cell.textLabel?.font = UIFont.systemFont(ofSize: 26, weight: .medium)
        }
        else
        {
            // Iphone
            cell.textLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        }
        
        cell.backgroundView?.backgroundColor = .appTransparentWhite
        cell.backgroundColor = .appTransparentWhite
        
        return cell
    }
    
}

extension SelectRoutineTimerViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //Only breifly highlight the selection
        tableView.deselectRow(at: indexPath, animated: true)
        
        //Get the seleted routine
        selectedRoutine = routines[indexPath.row]
        
        //Segue
        performSegue(withIdentifier: Segue.toRoutineTimer.stringValue, sender: indexPath)
    }
    
}
