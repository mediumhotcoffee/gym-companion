//
//  HiitTimerViewController.swift
//  gym-companion-app
//
//  Created by Marcos Felipe Costa da Silva on 7/11/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

class SelectHiitTimerViewController: UIViewController {

    //MARK: - Variables
    //Times
    var warmUpTime: Int = 30
    var lowIntensityTime: Int = 30
    var highIntensityTime: Int = 30
    var coolDownTime: Int = 30
    
    //Number of sessions
    var numLowIntensitySessions: Int?
    var numHighIntensitySessions: Int?
    
    //MARK: - Views
    //Labels
    @IBOutlet weak var warmUpTimeLabel: UILabel!
    @IBOutlet weak var lowIntensityTimeLabel: UILabel!
    @IBOutlet weak var highIntensityTimeLabel: UILabel!
    @IBOutlet weak var coolDownTimeLabel: UILabel!
    //Buttons
    @IBOutlet weak var decreaseWarmUpTimeButton: UIButton!
    @IBOutlet weak var decreaseLowIntensityTimeButton: UIButton!
    @IBOutlet weak var decreaseHighIntensityTimeButton: UIButton!
    @IBOutlet weak var decreaseCoolDownTimeButton: UIButton!
    
    //MARK: - Lifecycle methods
    override func viewDidLoad() {
        //Do any additional setup after loading the view
        super.viewDidLoad()

        //Make time labels monospaced
        setupLabels()
    }

    //MARK: - Actions
    @IBAction func cancelTapped(_ sender: UIButton) {
        //Go back to the previous screen
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changeWarmUpTimeTapped(_ sender: UIButton) {
        //Raise or lower warm up time
        updateHiitTime(hiitSection: .warmUp, tag: sender.tag)
    }
    
    @IBAction func changeLowIntensityTimeTapped(_ sender: UIButton) {
        //Raise or lower low intensity time
        updateHiitTime(hiitSection: .lowIntensity, tag: sender.tag)
    }
    
    @IBAction func changeHighIntensityTimeTapped(_ sender: UIButton) {
        //Raise or lower high intensity time
        updateHiitTime(hiitSection: .highIntensity, tag: sender.tag)
    }
    
    @IBAction func changeCoolDownTimeTapped(_ sender: UIButton) {
        //Raise or lower cool down time
        updateHiitTime(hiitSection: .coolDown, tag: sender.tag)
    }

    @IBAction func startTapped(_ sender: UIButton) {
        //Start HIIT timer
        performSegue(withIdentifier: Segue.toHiitTimer.stringValue, sender: self)
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //Send times to timer as seconds
        if let destination = segue.destination as? HiitTimerViewController {
            //Send times
            destination.warmUpTime = warmUpTime
            destination.lowIntensityTime = lowIntensityTime
            destination.highIntensityTime = highIntensityTime
            destination.coolDownTime = coolDownTime
            
            //Send number of sessions
            if let numHighIntensitySessions = numHighIntensitySessions, let numLowIntensitySessions = numLowIntensitySessions {
                destination.numHighIntensitySessionsToGo = numHighIntensitySessions
                destination.numLowIntensitySessionsToGo = numLowIntensitySessions
            }
        }
    }
    
    //MARK: - Custom Methods
    func setupLabels() {
        //Set a monospace font for the timer labels so they don't wiggle out
        //of place
        warmUpTimeLabel.font = UIFont.monospacedDigitSystemFont(ofSize: 25, weight: UIFont.Weight.semibold)
        lowIntensityTimeLabel.font = UIFont.monospacedDigitSystemFont(ofSize: 25, weight: UIFont.Weight.semibold)
        highIntensityTimeLabel.font = UIFont.monospacedDigitSystemFont(ofSize: 25, weight: UIFont.Weight.semibold)
        coolDownTimeLabel.font = UIFont.monospacedDigitSystemFont(ofSize: 25, weight: UIFont.Weight.semibold)
    }
    
    private func updateHiitTime(hiitSection: HiitSection, tag: Int) {
        switch hiitSection {
        case .warmUp:
            //Update warm up time
            if tag == 0 && warmUpTime >= 60 {
                warmUpTime -= 30
                
                if warmUpTime == 30 {
                    decreaseWarmUpTimeButton.isEnabled = false
                }
            } else if tag == 1  && warmUpTime < 300 {
                warmUpTime += 30
                
                if warmUpTime > 30 {
                    decreaseWarmUpTimeButton.isEnabled = true
                }
            }
            
            warmUpTimeLabel.text = TimerFormatterUtil.formatToTime(this: warmUpTime)
        case .lowIntensity:
            if tag == 0  && lowIntensityTime >= 60 {
                lowIntensityTime -= 30
                
                if lowIntensityTime == 30 {
                    decreaseLowIntensityTimeButton.isEnabled = false
                }
            } else if tag == 1  && lowIntensityTime < 300 {
                lowIntensityTime += 30
                
                if lowIntensityTime > 30 {
                    decreaseLowIntensityTimeButton.isEnabled = true
                }
            }
            
            lowIntensityTimeLabel.text = TimerFormatterUtil.formatToTime(this: lowIntensityTime)
        case .highIntensity:
            if tag == 0  && highIntensityTime >= 60 {
                highIntensityTime -= 30
                
                if highIntensityTime == 30 {
                    decreaseHighIntensityTimeButton.isEnabled = false
                }
            } else if tag == 1  && highIntensityTime < 300 {
                highIntensityTime += 30
                
                if highIntensityTime > 30 {
                    decreaseHighIntensityTimeButton.isEnabled = true
                }
            }
            
            highIntensityTimeLabel.text = TimerFormatterUtil.formatToTime(this: highIntensityTime)
        case .coolDown:
            if tag == 0  && coolDownTime >= 60 {
                coolDownTime -= 30
                
                if coolDownTime == 30 {
                    decreaseCoolDownTimeButton.isEnabled = false
                }
            } else if tag == 1  && coolDownTime < 300 {
                coolDownTime += 30
                
                if coolDownTime > 30 {
                    decreaseCoolDownTimeButton.isEnabled = true
                }
            }
            
            coolDownTimeLabel.text = TimerFormatterUtil.formatToTime(this: coolDownTime)
        }
    }
    
}
