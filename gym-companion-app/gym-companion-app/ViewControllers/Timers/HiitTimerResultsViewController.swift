//
//  HiitTimerResultsViewController.swift
//  gym-companion-app
//
//  Created by Marcos Felipe Costa da Silva on 7/18/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

class HiitTimerResultsViewController: UIViewController {

    //MARK: - Variables
    //Number of sessions to go
    var numHighIntensitySessions: Int = 0
    var numLowIntensitySessions: Int = 0
    
    //MARK: - Views
    //Label
    @IBOutlet weak var resultLabel: UILabel!
    
    //MARK: - Lifecycle Methods
    override func viewDidLoad() {
        //Do any additional setup after loading the view
        super.viewDidLoad()
        
        //Show result
        updateResult()
    }
    
    //MARK: - Actions
    @IBAction func doneTapped(_ sender: UIButton) {
        //Go back to the timers screen
        performSegue(withIdentifier: Segue.toTimersUnwind.stringValue, sender: self)
    }

    //MARK: - Custom Methods
    func updateResult() {
        if numHighIntensitySessions != 0 && numLowIntensitySessions != 0 {
            resultLabel.text = "Congratulations! You completed \(numHighIntensitySessions) high intensity session(s) and \(numLowIntensitySessions) low intensity session(s)."
        } else {
            resultLabel.text = "No sessions completed."
        }
    }
    
}
