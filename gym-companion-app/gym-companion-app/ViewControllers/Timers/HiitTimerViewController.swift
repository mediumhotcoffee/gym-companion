//
//  HiitTimerViewController.swift
//  gym-companion-app
//
//  Created by Marcos Felipe Costa da Silva on 7/18/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

class HiitTimerViewController: UIViewController {

    //MARK: - Variables
    //Times
    var warmUpTime: Int?
    var highIntensityTime: Int?
    var lowIntensityTime: Int?
    var coolDownTime: Int?
    //Number of sessions to go
    var numHighIntensitySessionsCompleted: Int = 0
    var numLowIntensitySessionsCompleted: Int = 0
    var numHighIntensitySessionsToGo: Int = 0 //Value will be overriden
    var numLowIntensitySessionsToGo: Int = 0 //Value will be overriden
    //Timer Variables
    var timer: Timer?
    var secondsCount: Int = 0
    var shouldPause: Bool = false
    //HIIT Section
    var currentHiitSection: HiitSection = .warmUp
    
    //MARK: - Views
    //Labels
    @IBOutlet weak var sessionTypeLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var startPauseLabel: UILabel!
    
    //MARK: - Lifecycle Methods
    override func viewDidLoad() {
        //Do any additional setup after loading the view
        super.viewDidLoad()
        
        //Setup timer UI
        setupUI()
    }
    
    //MARK: - Actions
    @IBAction func startPauseTimerTapped(_ sender: Any) {
        //Start or pause the timer
        
        //Handle paused state
        if shouldPause {
            //Pause the timer
            startPauseLabel.text = "Start"
            timer?.invalidate()
        } else {
            //Start the timer
            startPauseLabel.text = "Pause"
            
            //Start the timer
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: true)
            
            //Put time on another run loop
            RunLoop.current.add(timer!, forMode: RunLoop.Mode.common)
        }
        
        shouldPause.toggle()
    }
    
    @IBAction func endTapped(_ sender: UIButton) {
        //End the timer
        let alert = UIAlertController(title: "End Timer", message: "Are you sure you want to end the HIIT timer?", preferredStyle: .alert)
        
        //OK action
        alert.addAction(UIAlertAction(title: "OK", style: .destructive) { (alert) in
            //Stop timer
            self.timer?.invalidate()
            
            self.performSegue(withIdentifier: Segue.toHiitTimerResult.stringValue, sender: self)
        })
        
        //Cancel action
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        //Present the alert
        present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Screen Management
    override var shouldAutorotate: Bool {
        return false
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segue.toHiitTimerResult.stringValue {
            if let destination = segue.destination as? HiitTimerResultsViewController {
                destination.numHighIntensitySessions = numHighIntensitySessionsCompleted
                destination.numLowIntensitySessions = numLowIntensitySessionsCompleted
            }
        }
    }
    
    //MARK: - Custom Methods
    func setupUI() {
        //Set a monospace font for the timer labels so they don't wiggle out
        //of place
        timerLabel.font = UIFont.monospacedDigitSystemFont(ofSize: 85, weight: UIFont.Weight.regular)
        
        //Set seconds as warm up time
        if let warmUpTime = warmUpTime {
            secondsCount = warmUpTime
            timerLabel.text = TimerFormatterUtil.formatToTime(this: secondsCount)
        }
    }
    
    func updateSectionLabel(_ section: HiitSection) {
        //Update the section on the label
        switch section {
        case .warmUp:
            sessionTypeLabel.text = "Warm Up"
        case .lowIntensity:
            sessionTypeLabel.text = "Low Intensity"
        case .highIntensity:
            sessionTypeLabel.text = "High Intensity"
        case .coolDown:
            sessionTypeLabel.text = "Cool Down"
        }
    }

    @objc func fireTimer(timer: Timer) {
        //Handle the timer
        
        //Decrement seconds
        secondsCount -= 1
        timerLabel.text = TimerFormatterUtil.formatToTime(this: secondsCount)
        
        //Stop timer if it reaches 0
        if secondsCount == 0 {
            //Stop timer
            timer.invalidate()
            
            //Go to next HIIT session
            switch currentHiitSection {
            case .warmUp:
                //On warm up section done
                
                //Get time for high intensity section
                guard let highIntensityTime = highIntensityTime else {
                    return
                }
                
                //Jump to high intensity section
                updateSectionLabel(.highIntensity)
                currentHiitSection = .highIntensity
                secondsCount = highIntensityTime
                timerLabel.text = TimerFormatterUtil.formatToTime(this: secondsCount)
            case .highIntensity:
                //On high intensity section done
                
                //Update number of remaining high intensity sections
                numHighIntensitySessionsToGo -= 1
                
                //Update number of sessions completed
                numHighIntensitySessionsCompleted += 1
                
                if numHighIntensitySessionsToGo <= 0 && numLowIntensitySessionsToGo <= 0 {
                    //Go to cool down
                    
                    //Get time for cool down section
                    guard let coolDownTime = coolDownTime else {
                        return
                    }
                    
                    //Jump to cool down section
                    updateSectionLabel(.coolDown)
                    currentHiitSection = .coolDown
                    secondsCount = coolDownTime
                    timerLabel.text = TimerFormatterUtil.formatToTime(this: secondsCount)
                } else {
                    //Go to low intensity section
                    
                    //Reset timer seconds
                    guard let lowIntensityTime = lowIntensityTime else {
                        return
                    }
                    
                    //Jump to low intensity section
                    updateSectionLabel(.lowIntensity)
                    currentHiitSection = .lowIntensity
                    secondsCount = lowIntensityTime
                    timerLabel.text = TimerFormatterUtil.formatToTime(this: secondsCount)
                }
            case .lowIntensity:
                //Go to high intensity section
                
                //Update number of remaining low intensity sections
                numLowIntensitySessionsToGo -= 1
                
                //Update number of sessions completed
                numLowIntensitySessionsCompleted += 1
                
                updateSectionLabel(.highIntensity)
                
                //Reset timer seconds
                guard let highIntensityTime = highIntensityTime else {
                    return
                }
                
                //Jump to high intensity section
                updateSectionLabel(.highIntensity)
                currentHiitSection = .highIntensity
                secondsCount = highIntensityTime
                timerLabel.text = TimerFormatterUtil.formatToTime(this: secondsCount)
            case .coolDown:
                //End timer and go to results
                timer.invalidate()
                performSegue(withIdentifier: Segue.toHiitTimerResult.stringValue, sender: self)
            }
            
            //Reset the state of the Start/Pause button
            shouldPause = false
            startPauseLabel.text = "Start"
            
            //Give haptic feedback to the user
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.success)
        }
    }
    
}
