//
//  TimersViewController.swift
//  gym-companion-app
//
//  Created by Marcos Silva on 6/28/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseStorage

class TimersViewController: UIViewController {
    
    //MARK: - Views
    @IBOutlet weak var accountButton: UIButton!
    
    //MARK: - Other variables
    var docsURL: URL?
    var downloadedImageURL: String?
    var userPicturesPath: String = ""
    
    //MARK: - Lifecycle methods
    override func viewWillAppear(_ animated: Bool) {
        //Hide NavigationController since the only purpose of it is to maintain
        //the tab bar showing through all segues
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        //Setup
        ImageUtil.loadUserPictureOn(button: accountButton, userId: Auth.auth().currentUser?.uid ?? "")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    //MARK: - Actions
    @IBAction func userAccountTapped(_ sender: UIButton) {
        //Open account screen
        accountTapped(sender)
    }
    
    @IBAction func routineTimerTapped(_ sender: UIButton) {
        //Go to routine timer if a user is logged in
        if FirebaseAuthValidationUtil.checkIfSignedIn() {
            //If a user is logged in, show routines so they can select one to
            //time
            performSegue(withIdentifier: Segue.toSelectRoutineTimer.stringValue, sender: self)
        } else {
            //If no user is logged in, tell the user to log in
            let alert = UIAlertController(title: "Not Logged In", message: "Please log in to see your routines", preferredStyle: .alert)
            
            //OK button
            let okButton = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okButton)
            
            present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: - Navigation
    @IBAction func unwindToTimers(segue: UIStoryboardSegue) {
        //Come to this screen through an unwind
    }
    
    //MARK: - Custom methods
    func displayUserAccountImage() {
        //Try to display the downloaded image
        do {
            if let url = docsURL?.appendingPathComponent(userPicturesPath) {
                let data = try Data(contentsOf: url)
                
                let userPicture = UIImage(data: data)
                
                //Display picture to the user
                self.accountButton.contentMode = .scaleToFill
                self.accountButton.setImage(userPicture, for: .normal)
                self.accountButton.layer.cornerRadius = self.accountButton.frame.size.width / 2
                self.accountButton.clipsToBounds = true
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
}

extension TimersViewController: HeaderBarDelegate {
    
    // This simply ensures that the screen is always able to open account
    func accountTapped(_ sender: UIButton) {
        // Open the account storyboard
        let storyboard = UIStoryboard.init(name: "Account", bundle: Bundle.main)
        let viewcontroller = storyboard.instantiateViewController(withIdentifier: Identifier.accountViewController.stringValue) as? AccountViewController
        
        present(viewcontroller!, animated: true, completion: nil)
    }
    
}
