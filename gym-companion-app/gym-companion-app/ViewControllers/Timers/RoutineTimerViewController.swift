//
//  RoutineTimerViewController.swift
//  gym-companion-app
//
//  Created by Marcos Felipe Costa da Silva on 7/11/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseDatabase

class RoutineTimerViewController: UIViewController {

    //MARK: - Variables
    //Timer
    var timer: Timer?
    var secondsCount: Int = 0
    var shouldPause: Bool = false
    //Workout
    var routineToTime: Routine?
    var exercises = [Exercise]()
    var currentExerciseIndex = 0
    var currentSet = 1
    //Firebase
    var databaseReference: DatabaseReference?
    
    //MARK: - Views
    //Buttons
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    //Views
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var innerTimerView: UIView!
    //Labels
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var routineNameLabel: UILabel!
    @IBOutlet weak var startPauseLabel: UILabel!
    @IBOutlet weak var exerciseNameLabel: UILabel!
    @IBOutlet weak var setNumberLabel: UILabel!
    
    //MARK: - Lifecycle methods
    override func viewWillAppear(_ animated: Bool) {
        //Setup database access
        databaseReference = Database.database().reference()
        
        //Load exercises for use
        if let routine = routineToTime {
            loadExercisesFromRoutine(routine)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Setup view's appearance
        setupViewsAppearance()
        
        if exercises.count == 0 {
            //Reenable Next and Previous buttons
            nextButton.isHidden = true
            previousButton.isHidden = true
        }
    }
    
    //MARK: - Actions
    @IBAction func changeExerciseButtonTapped(_ sender: UIButton) {
        //Update the UI for the new exercise
        
        //Determine the index of the exercise we want to display
        switch sender.tag {
        case 0:
            //Go to previous exercise
            if currentExerciseIndex == 0 {
                currentExerciseIndex = exercises.count - 1
            } else {
                currentExerciseIndex -= 1
            }
        case 1:
            //Go to next exercise
            if currentExerciseIndex == (exercises.count - 1) {
                currentExerciseIndex = 0
            } else {
                currentExerciseIndex += 1
            }
        default:
            break
        }
        
        let e = exercises[currentExerciseIndex]
        
        //Display the name of the exercise
        exerciseNameLabel.text = e.name
        
        //Display the number of sets
        setNumberLabel.text = "Set: \(currentSet) of \(e.sets)"
        
        //Update the amount of rest time
        self.secondsCount = e.restTimeSeconds
        self.timerLabel.text = TimerFormatterUtil.formatToTime(this: self.secondsCount)
    }
    
    @IBAction func startPauseButton(_ sender: UIButton) {
        //Start or pause the timer
        //Update the label on the Start/Pause button
        
        //Handle paused state
        if shouldPause {
            //Pause the timer
            startPauseLabel.text = "Start"
            
            //Reenable Next and Previous buttons
            nextButton.isEnabled = true
            previousButton.isEnabled = true
            
            timer?.invalidate()
        } else {
            //Start the timer
            startPauseLabel.text = "Pause"
            
            //Reenable Next and Previous buttons
            nextButton.isEnabled = false
            previousButton.isEnabled = false
            
            //Start the timer
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: true)
            RunLoop.current.add(timer!, forMode: RunLoop.Mode.common)
        }
        
        shouldPause.toggle()
    }
    
    @IBAction func resetButton(_ sender: UIButton) {
        //Stop the timer
        timer?.invalidate()
        
        //Reset seconds counter
        let e = exercises[currentExerciseIndex]
        secondsCount = e.restTimeSeconds
        timerLabel.text = TimerFormatterUtil.formatToTime(this: secondsCount)
        
        //Reenable Next and Previous buttons
        nextButton.isHidden = false
        previousButton.isHidden = false
        
        //Reset the state of the Start/Pause button
        shouldPause = false
        startPauseLabel.text = "Start"
    }
    
    @IBAction func finishTapped(_ sender: UIButton) {
        //Alert here to prevent the user from killing the timer
        //accidentally
        let alert = UIAlertController(title: "Stop Timer", message: "Are you sure you want to end the timer?", preferredStyle: .alert)
        
        //OK Action
        alert.addAction(UIAlertAction(title: "Stop", style: .destructive, handler: { (action) in
            //Stop the timer
            self.timer?.invalidate()
            
            self.navigationController?.popViewController(animated: true)
        }))
        
        //Cancel action
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        
        present(alert, animated: true)
    }
    
    //MARK: - Custom methods
    func setupViewsAppearance() {
        //Round the outer timer view
        outerView.layer.cornerRadius = 20.0
        
        //Round the inner timer view
        innerTimerView.layer.cornerRadius = 20.0
        
        //Create shadow for the reset button
        resetButton.layer.shadowColor = UIColor.black.cgColor
        resetButton.layer.shadowOffset = CGSize(width: 1, height: 0.5)
        resetButton.layer.shadowRadius = 1
        resetButton.layer.shadowOpacity = 0.25
        
        //Show the routine's name
        if let routine = routineToTime {
            routineNameLabel.text = routine.name
        }
        
        //Set a monospace font for the timer label so it doesn't wiggle out
        //of place
        timerLabel.font = UIFont.monospacedDigitSystemFont(ofSize: 85, weight: UIFont.Weight.regular)
    }
    
    @objc func fireTimer(timer: Timer) {
        //Handle the timer
        
        //Decrement seconds
        secondsCount -= 1

        //Display amount of time left
        timerLabel.text = TimerFormatterUtil.formatToTime(this: secondsCount)
        
        //Stop timer if it reaches 0
        if secondsCount <= 0 {
            //Stop timer
            timer.invalidate()
            
            //Increase set count
            let e = exercises[currentExerciseIndex]
            
            if currentSet < e.sets {
                currentSet += 1
            } else {
                currentSet = 1
            }
            
            setNumberLabel.text = "Set: \(currentSet) of \(e.sets)"
            
            //Reset the state of the Start/Pause button
            shouldPause = false
            startPauseLabel.text = "Start"
            
            //Reset seconds
            secondsCount = e.restTimeSeconds
            timerLabel.text = TimerFormatterUtil.formatToTime(this: secondsCount)
            
            //Reenable Next and Previous buttons
            nextButton.isHidden = false
            previousButton.isHidden = false
            
            //Give haptic feedback to the user
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.success)
        }
    }
    
    func loadExercisesFromRoutine(_ routineToTime: Routine) {
        //Loads exercises from database
        exercises = [Exercise]()
        
        for id in routineToTime.exercises {
            self.databaseReference?.child("exercises").child(id).observeSingleEvent(of: .value) { (exerciseSnapshot) in
                if let exerciseValue = exerciseSnapshot.value as? NSDictionary, let exerciseObject = Exercise(fromNSDictionary: exerciseValue, exerciseID: exerciseSnapshot.key) {
                    self.exercises += [exerciseObject]
                    
                    //Show the name of the first exercise
                    if self.exercises.count == 1 {
                        //Get first exercise that is loaded
                        let e = self.exercises[0]
                        
                        //Show exercise name
                        self.exerciseNameLabel.text = e.name
                        
                        //Show number of sets
                        self.setNumberLabel.text = "Set: 1 of \(e.sets)"
                        
                        //Show the amount of rest time
                        self.secondsCount = e.restTimeSeconds
                        self.timerLabel.text = TimerFormatterUtil.formatToTime(this: self.secondsCount)
                    }
                }
            }
        }
    }
    
}
