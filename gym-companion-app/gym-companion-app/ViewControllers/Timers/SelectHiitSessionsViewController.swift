//
//  SelectHiitSessionsViewController.swift
//  gym-companion-app
//
//  Created by Marcos Silva on 7/18/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

class SelectHiitSessionsViewController: UIViewController {

    //MARK: - Variables
    //Number of sessions
    var numHighIntensitySessions: Int = 2
    var numLowIntensitySessions: Int = 1
    
    //MARK: - Views
    //Labels
    @IBOutlet weak var highIntensitySessionsLabel: UILabel!
    @IBOutlet weak var lowIntensitySessionsLabel: UILabel!
    
    //MARK: - Lifecycle Methods
    override func viewDidLoad() {
        //Do any additional setup after loading the view
        super.viewDidLoad()
        
        //Make time labels monospaced
        setupLabels()
    }
    
    //MARK: - Actions
    @IBAction func cancelTapped(_ sender: UIButton) {
        //Go back to previous screen
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextTapped(_ sender: UIButton) {
        //Go to times selection screen
        performSegue(withIdentifier: Segue.toSelectHiitTimes.stringValue, sender: self)
    }
    
    @IBAction func changeHighIntensitySessionNumTapped(_ sender: UIButton) {
        //Increase/decrease the number of high intensity sessions
        updateSessionNumbers(tag: sender.tag)
    }
    
    @IBAction func changeLowIntensitySessionNumTapped(_ sender: UIButton) {
        //Increase/decrease the number of low intensity sessions
        updateSessionNumbers(tag: sender.tag)
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //Send number of sessions to next screen
        if let destination = segue.destination as? SelectHiitTimerViewController {
            destination.numLowIntensitySessions = numLowIntensitySessions
            destination.numHighIntensitySessions = numHighIntensitySessions
        }
    }
    
    //MARK: - Custom Methods
    func setupLabels() {
        //Set a monospace font for the labels so they don't wiggle out
        //of place
        lowIntensitySessionsLabel.font = UIFont.monospacedDigitSystemFont(ofSize: 85, weight: UIFont.Weight.semibold)
        highIntensitySessionsLabel.font = UIFont.monospacedDigitSystemFont(ofSize: 85, weight: UIFont.Weight.semibold)
    }
    
    func updateSessionNumbers(tag: Int) {
        switch tag {
        case 0:
            //Decrease
            if numLowIntensitySessions > 0 {
                numHighIntensitySessions -= 1
                numLowIntensitySessions -= 1
            }
        case 1:
            //Increase
            if numHighIntensitySessions < 20 {
                numHighIntensitySessions += 1
                numLowIntensitySessions += 1
            }
        default:
            break
        }
        
        //Update labels
        highIntensitySessionsLabel.text = numHighIntensitySessions.description
        lowIntensitySessionsLabel.text = numLowIntensitySessions.description
    }
    
}
