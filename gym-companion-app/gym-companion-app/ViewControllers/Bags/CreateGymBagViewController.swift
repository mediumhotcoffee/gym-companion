//
//  CreateGymBagViewController.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/11/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseStorage

class CreateGymBagViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var addIconButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var bagNameTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Variables
    var storageReference: StorageReference!
    var iconTableViewHandler: IconActionSheetTableViewHandler!
    
    var passedGymBag: GymBag?
    
    var bagItems: [BagItem] = []
    var itemToRemove: Int?
    var bagIconID: String?
    
    var iconLocations: [String] = []
    var iconImages: [String:UIImage] = [:]
    
    var savedToFirebase: Bool = false
    var savedLocally: Bool = true //TODO: Change to false when local saving is working.
    
    var paid: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        storageReference = Storage.storage().reference(withPath: "icons/").child("gymBagIcons/")
        populateBagItems()
        setTextFieldListener()
        setUpTableView()
        checkForPassedGymBag()
        toggleSaveButtonState()
        showBagIcon()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        FirebaseAccountsUtil.getCurrentUser(delegate: self)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>,
                               with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? CreateBagItemViewController {
            if let bagName = bagNameTextField.text, bagName != "" {
                destination.bagName = bagName
            }
            
            if let indexPath = sender as? IndexPath {
                itemToRemove = indexPath.row
                destination.passedBagItem = bagItems[indexPath.row]
            }
        }
    }
    
    private func checkForPassedGymBag() {
        if let bag = passedGymBag {
            bagNameTextField.text = bag.bagName
            bagIconID = bag.bagIconID
        }
    }
    
    private func checkIfShouldDismiss() {
        if savedToFirebase && savedLocally {
            navigationController?.popViewController(animated: true)
        }
    }
    
    private func populateBagItems() {
        if let bag = passedGymBag {
            bagItems = bag.bagItems
            tableView.reloadData()
        }
    }
    
    private func setTextFieldListener() {
        bagNameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        bagNameTextField.addTarget(self, action: #selector(textFieldEditingDidEnd(_:)), for: .editingDidEnd)
    }
    
    private func setUpTableView() {
        (tableView.delegate, tableView.dataSource) = (self, self)
    }
    
    private func showIconList() {
        iconTableViewHandler = IconActionSheetTableViewHandler(iconLocations: iconLocations, iconImages: iconImages)
        let alertController: UIAlertController = AlertHelper.getListActionSheet(screenHeight: self.view.frame.height, selectMultiple: false, tableViewHandler: iconTableViewHandler, actionSheetDelegate: self)
        
        if let popoverController = alertController.popoverPresentationController {
            let source: CGRect = CGRect(
                x: addIconButton.bounds.origin.x,
                y: addIconButton.bounds.origin.y,
                width: addIconButton.bounds.width,
                height: addIconButton.bounds.height)
            
            popoverController.sourceView = addIconButton
            popoverController.sourceRect = source
        }
        
        self.present(alertController, animated: true)
    }
    
    private func showBagIcon() {
        
        if let itemLocation = bagIconID, let image = iconImages[itemLocation] {
            addIconButton.setImage(image, for: .normal)
        } else if let bag = passedGymBag, let location = bag.bagIconID {
            PullIconsHelper.pullSingleIconImage(location: location, storageReference: storageReference, delegate: self)
        }
    }
    
    // Either shows or hids the save button depending on whether or not the routine has been given a name and at least one category.
    private func toggleSaveButtonState() {
        
        guard let textViewText = bagNameTextField.text, !textViewText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty
            else {
                
                if bagNameTextField.text == "" || bagNameTextField.text?.count ?? 0 < 2 {
                    bagNameTextField.text = bagNameTextField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                }
                
                saveButton.isEnabled = false
                saveButton.isHidden = true
                return
        }
        
        saveButton.isEnabled = true
        saveButton.isHidden = false
    }
    
    // When add is tapped, show a bottom action sheet that allows the user to either create or browse bag items.
    func showAddOptions(_ sender: UIButton) {
        
        if let paid = paid, !paid && bagItems.count > 2 {
            let alert: UIAlertController = AlertHelper.getLimitAlert(listType: .bagItem, delegate: self)
            present(alert, animated: true, completion: nil)
            return
        } else if !FirebaseAuthValidationUtil.checkIfSignedIn() {
            let alert: UIAlertController = AlertHelper.getSignInToAddListsAlert(sender: sender, delegate: self)
            present(alert, animated: true, completion: nil)
        }
        
        let alertController: UIAlertController = AlertHelper.getAddOptionsActionSheet(listType: .bagItem, sender: sender, delegate: self)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}

//MARK: - Extensions
extension CreateGymBagViewController {
    
    @IBAction func addBagItemTapped(_ sender: UIButton) {
        showAddOptions(sender)
    }
    
    @IBAction func addBagIconTapped(_ sender: UIButton) {
        PullIconsHelper.pullIconLocations(from: ListType.bagItem.stringValue, delegate: self, listType: ListType.bag)
    }
    
    @IBAction func saveTapped(_ sender: UIButton) {
        if ValidationHelper.textFieldIsNotEmpty(bagNameTextField, setIcon: true) {
            
            if let bagToUpdate = passedGymBag, let bagName = bagNameTextField.text {
                
                FirebaseGymBagsUtil.pushGymBagUpdateToFirebase(delegate: self, bagID: bagToUpdate.bagID, bagIconID: bagIconID, bagName: bagName, bagItems: bagItems)
            }
            else if let bagName = bagNameTextField.text {
                
                FirebaseGymBagsUtil.saveNewGymBagToGymBagsRoot(delegate: self, bagIconID: bagIconID, bagName: bagName, bagItems: bagItems)
            }
        }
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func passBackBagItem(_ segue: UIStoryboardSegue) {
        if let source = segue.source as? CreateBagItemViewController {
            
            var insertIndex: Int?
            
            if let removeIndex = itemToRemove {
                
                bagItems.remove(at: removeIndex)
                itemToRemove = nil
                insertIndex = removeIndex
            }
            
            if let passedItem = source.bagItemToPass {
                
                if let index = insertIndex, index < bagItems.count {
                    
                    bagItems.insert(passedItem, at: index)
                }
                else {
                    
                    bagItems += [passedItem]
                }
                
                tableView.reloadData()
            }
        }
        else if let source = segue.source as? BrowseGymBagItemsViewController, let passedItem = source.selectedItem {
            
            var insertIndex: Int?
            
            if let removeIndex = itemToRemove {
                
                bagItems.remove(at: removeIndex)
                itemToRemove = nil
                insertIndex = removeIndex
            }
            
            if let index = insertIndex, index < bagItems.count {
                
                bagItems.insert(passedItem, at: index)
            }
            else {
                
                bagItems += [passedItem]
            }
            
            tableView.reloadData()
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        toggleSaveButtonState()
    }
    
    @objc func textFieldEditingDidEnd(_ textField: UITextField) {
        textField.text = textField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
}

extension CreateGymBagViewController: SaveGymBagDelegate {
    
    func savedBagToUserRoot(_ saved: Bool, gymBag: GymBag) {
        savedToFirebase = saved
        checkIfShouldDismiss()
    }
    
    func savedBagToGymBagsRoot(_ saved: Bool, gymBag: GymBag) {
        FirebaseGymBagsUtil.saveGymBagNewToUserRoot(delegate: self, gymBag: gymBag)
    }
    
    func bagUpdated(_ updated: Bool, gymBag: GymBag) {
        savedToFirebase = updated
        checkIfShouldDismiss()
    }
    
    func savedBagLocally(_ saved: Bool, gymBag: GymBag) {
        savedLocally = saved
        checkIfShouldDismiss()
    }
    
}

extension CreateGymBagViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bagItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: Identifier.reusableCell.stringValue, for: indexPath)
        cell.textLabel?.text = bagItems[indexPath.row].itemName
        
        cell.textLabel?.textColor = .appDarkColor
        
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            // Ipad
            cell.textLabel?.font = UIFont.systemFont(ofSize: 26, weight: .medium)
        }
        else
        {
            // Iphone
            cell.textLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        }
        
        cell.backgroundView?.backgroundColor = .appTransparentWhite
        cell.backgroundColor = .appTransparentWhite
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        performSegue(withIdentifier: Segue.toCreateBagItem.stringValue, sender: indexPath)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            bagItems.remove(at: indexPath.row)
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
}

extension CreateGymBagViewController: PullIconsDelegate {
    
    func iconLocationsPulled(iconLocations: [String]) {
        self.iconLocations = iconLocations
        iconImages = [:]
        PullIconsHelper.pullIconImages(iconLocations: iconLocations, storageReference: storageReference, delegate: self)
    }
    
    func iconImagesPulled(_ iconImage: (key: String, image: UIImage)) {
        iconImages[iconImage.key] = iconImage.image
        
        if iconLocations.count == iconImages.count {
            showIconList()
        }
    }
    
    func singleIconImagePulled(iconImage: UIImage) {
        addIconButton.setImage(iconImage, for: .normal)
    }
    
}

extension CreateGymBagViewController: UITableViewActionSheetDelegate {
    
    func addToList(selectedIndexPaths: [IndexPath]) {
        if selectedIndexPaths.count > 0 {
            bagIconID = iconLocations[selectedIndexPaths[0].row]
            showBagIcon()
        }
    }
    
}

extension CreateGymBagViewController: AddOptionsActionSheetDelegate {
    
    func browseSelected(sender: UIButton) {
        if FirebaseAuthValidationUtil.checkIfSignedIn() {
            performSegue(withIdentifier: Segue.toBrowseBagItem.stringValue, sender: sender)
        } else {
            let alert: UIAlertController = AlertHelper.getSignInToAddListsAlert(sender: sender, delegate: self)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func createSelected(sender: UIButton) {
        if FirebaseAuthValidationUtil.checkIfSignedIn() {
            performSegue(withIdentifier: Segue.toCreateBagItem.stringValue, sender: sender)
        } else {
            let alert: UIAlertController = AlertHelper.getSignInToAddListsAlert(sender: sender, delegate: self)
            present(alert, animated: true, completion: nil)
        }
    }
    
}

extension CreateGymBagViewController: FirebaseAccountDelegate {
    
    func userPulled(user: User, current: Bool) {
        if current {
            paid = user.paid
        }
    }

    func signInSelected(_ sender: UIAlertAction) {
        accountTapped(UIButton(type: .system))
    }
    
    func upgradeTapped(sender: UIAlertAction) {
        accountTapped(UIButton(type: .system))
    }
    
}


extension CreateGymBagViewController: HeaderBarDelegate {
    
    func accountTapped(_ sender: UIButton) { }
    
}
