//
//  CreateGymBagItemViewController.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/12/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseStorage

class CreateBagItemViewController: UIViewController {
    
    @IBOutlet weak var bagNameLabel: UILabel!
    @IBOutlet weak var iconButton: UIButton!
    @IBOutlet weak var itemNameTextField: UITextField!
    
    var storageReference: StorageReference!
    var iconTableViewHandler: IconActionSheetTableViewHandler!
    
    var passedBagItem: BagItem?
    var bagItemToPass: BagItem?
    
    var itemIconID: String?
    var bagName: String?
    
    var iconLocations: [String] = []
    var iconImages: [String:UIImage] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        storageReference = Storage.storage().reference(withPath: "icons/").child("bagItemIcons/")
        setTextFieldText()
        showItemIcon()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>,
                               with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    private func setTextFieldText() {
        if let bagItem = passedBagItem {
            itemNameTextField.text = bagItem.itemName
        }
    }
    
    private func showIconList() {
        iconTableViewHandler = IconActionSheetTableViewHandler(iconLocations: iconLocations, iconImages: iconImages)
        let alertController: UIAlertController = AlertHelper.getListActionSheet(screenHeight: self.view.frame.height, selectMultiple: false, tableViewHandler: iconTableViewHandler, actionSheetDelegate: self)
        
        if let popoverController = alertController.popoverPresentationController {
            let source: CGRect = CGRect(
                x: iconButton.bounds.origin.x,
                y: iconButton.bounds.origin.y,
                width: iconButton.bounds.width,
                height: iconButton.bounds.height)
            
            popoverController.sourceView = iconButton
            popoverController.sourceRect = source
        }
        
        self.present(alertController, animated: true, completion:nil)
    }
    
    private func showItemIcon() {
        if let itemLocation = itemIconID, let image = iconImages[itemLocation] {
            
            iconButton.setImage(image, for: .normal)
        } else if let item = passedBagItem, let location = item.itemIconID {
            
            PullIconsHelper.pullSingleIconImage(location: location, storageReference: storageReference, delegate: self)
        }
    }
    
}

//MARK: - Extensions
extension CreateBagItemViewController {
    
    @IBAction func addIconTapped(_ sender: UIButton) {
        PullIconsHelper.pullIconLocations(from: ListType.bagItem.stringValue, delegate: self, listType: ListType.bagItem)
    }
    
    @IBAction func saveTapped(_ sender: UIButton) {
        if ValidationHelper.textFieldIsNotEmpty(itemNameTextField, setIcon: true) {
            if let text = itemNameTextField.text {
                bagItemToPass = BagItem(itemIconID: itemIconID, itemName: text)
                performSegue(withIdentifier: Segue.toPassBackBagItem.stringValue, sender: nil)
            }
        }
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
    }
    
}

extension CreateBagItemViewController: UITableViewActionSheetDelegate {
    
    func addToList(selectedIndexPaths: [IndexPath]) {
        if selectedIndexPaths.count > 0 {
            itemIconID = iconLocations[selectedIndexPaths[0].row]
            showItemIcon()
        }
    }
    
}

extension CreateBagItemViewController: PullIconsDelegate {
    
    func iconLocationsPulled(iconLocations: [String]) {
        self.iconLocations = iconLocations
        iconImages = [:]
        PullIconsHelper.pullIconImages(iconLocations: iconLocations, storageReference: storageReference, delegate: self)
    }
    
    func iconImagesPulled(_ iconImage: (key: String, image: UIImage)) {
        iconImages[iconImage.key] = iconImage.image
        
        if iconLocations.count == iconImages.count {
            showIconList()
        }
    }
    
    func singleIconImagePulled(iconImage: UIImage) {
        iconButton.setImage(iconImage, for: .normal)
    }
    
}
