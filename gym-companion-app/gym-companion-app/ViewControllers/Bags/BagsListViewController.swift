//
//  BagsListViewController.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 6/24/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage

class BagsListViewController: UIViewController {

    //MARK: - Views
    //TableView
    @IBOutlet weak var tableView: UITableView!
    //Button
    @IBOutlet weak var accountButton: UIButton!
    @IBOutlet weak var addBagButton: UIButton!
    
    var traineesTableView: UITableView!
    
    var gymBagsTableViewHandler: CollectionObjectsTableViewHandler!
    var traineesTableViewHandler: TraineeListActionSheetTableViewHandler!
    
    //MARK: - Firebase variables
    var handle: AuthStateDidChangeListenerHandle?
    var storageReference: StorageReference!
    var userID: String = ""
    var paid: Bool?
    
    //MARK: - Other variables
    var bagIDs: [String] = []
    var gymBags: [GymBag] = []
    
    var iconImages: [String:UIImage] = [:]
    
    var traineeIDs: [String]?
    var trainees: [User]?
    var bagToShare: GymBag?
    
    //MARK: - Lifecycle methods
    override func viewWillAppear(_ animated: Bool) {
        //Setup
        ImageUtil.loadUserPictureOn(button: accountButton, userId: Auth.auth().currentUser?.uid ?? "")
        loadBagIDs()
        trainees = []
        traineesTableViewHandler = TraineeListActionSheetTableViewHandler(trainees: [])
        FirebaseAccountsUtil.getCurrentUser(delegate: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Hide the navigation controller. The only reason the navigation controller exists is so that that tab bar will persist through segues.
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        storageReference = Storage.storage().reference(withPath: "icons/").child("gymBagIcons/")
        
        setUpTableView(isTrainer: false)
        setUpTraineesTableView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? CreateGymBagViewController {
            if let indexPath = sender as? IndexPath {
                destination.passedGymBag = gymBags[indexPath.row]
            }
        }
    }
    
    // Pulls the user's bagIDs from Firebase.
    private func loadBagIDs() {
        
        // Make sure to start with no IDs so as to not create duplicates when loading.
        bagIDs = []
        gymBags = []
        gymBagsTableViewHandler.collectionObjects = []
        // Start with an empty view. This will be fixed later if gym bags are found successfully. Otherwise, the empty state should be shown by default.
        setTableViewBackground()
        
        FirebaseGymBagsUtil.pullBagIDs(delegate: self)
    }
    
    private func pullTrainees() {
        if let ids = traineeIDs {
            for id in ids {
                FirebaseAccountsUtil.getUser(userID: id, delegate: self)
            }
        }
    }
    
    private func setTableViewBackground() {
        if gymBags.count > 0 {
            // If there are routines, the add button on the empty state will be hidden, so the addBagButton needs to be visible.
            addBagButton.isHidden = false
            // Make sure the background is clear, which will show the background color of the workouts screen.
            tableView.backgroundColor = .clear
            tableView.backgroundView = UIView(frame: tableView.frame)
            
            // Make sure the separators are visible (they could be hidden from showing the empty state.)
            tableView.separatorStyle = .singleLine
            
            // Make sure the changes are shown.
            gymBagsTableViewHandler.collectionObjects = gymBags
            tableView.reloadData()
        } else {
            addBagButton.isHidden = true
            showEmptyView()
        }
    }
    
    // Displays the empty list message with a button to add more bags.
    // This is done by using the UIEmptyListView nib file.
    private func showEmptyView() {
        // Get a UIEmptyListView from the nib so the outlets can be manipulated.
        let emptyView: UIEmptyListView = UIEmptyListView.emptyViewFromNib()
        emptyView.setupView(listType: .bag)
        emptyView.delegate = self
        
        tableView.backgroundView = emptyView
        tableView.separatorStyle = .none
        tableView.reloadData()
    }
    
    // Sets the delegate and data source for the table view, allowing this ViewController to control the tableview.
    private func setUpTableView(isTrainer: Bool) {
        
        var nib: UINib = UINib()
        
        if isTrainer {
            nib = UINib(nibName: NibName.shareableCollectionCell.stringValue, bundle: nil)
        } else {
            
            nib = UINib(nibName: NibName.collectionCell.stringValue, bundle: nil)
        }
        
        tableView.register(nib, forCellReuseIdentifier: Identifier.collectionCell.stringValue)
        
        gymBagsTableViewHandler = CollectionObjectsTableViewHandler(delegate: self, collectionObjects: gymBags, singleItemNames: [:], cellImages: [:], isTrainer: isTrainer)
        
        // Allows this ViewController to manipulate of the tableview using the appropriate data.
        (tableView.dataSource, tableView.delegate) = (gymBagsTableViewHandler, gymBagsTableViewHandler)
    }
    
    private func setUpTraineesTableView() {
        let alertController = UIAlertController(title: "Share", message: "Select a trainee to share this with.", preferredStyle: UIAlertController.Style.actionSheet)
        let margin:CGFloat = view.frame.height * 0.1
        let rect = CGRect(x: 0, y: margin, width: alertController.view.bounds.size.width * 0.95, height: view.frame.height * 0.2)
        
        traineesTableView = UITableView(frame: rect, style: .plain)
        traineesTableView.backgroundView = UIView()
        traineesTableView.backgroundColor = .clear
    }
    
}

//MARK: - Extensions
extension BagsListViewController {
    
    //MARK: - Actions
    @IBAction func userAccountTapped(_ sender: UIButton) {
        accountTapped(sender)
    }
    
    @IBAction func addBagTapped(_ sender: UIButton) {
        showAddOptions(sender)
    }
    
}

extension BagsListViewController: PullGymBagsDelegate {
    
    func gymBagIDsPulled(gymBagIDs: [String]) {
        // If successful, we have a valid User object.
        // Update the bagIDs array with the user's bags so the tableview can be populated.
        bagIDs = gymBagIDs
        
        if self.bagIDs.count > 0 {
            // If there are gym bags, show them.
            self.setTableViewBackground()
            FirebaseGymBagsUtil.pullGymBagObjects(delegate: self, bagIDs: bagIDs)
        }
    }
    
    func gymBagObjectsPulled(gymBags: [GymBag], browseObjectType: BrowseObjectType, totalToPull: Int) {
        // If a Routine object was created successfully, add it to the array of routines and show it in the tableview.
        self.gymBags += gymBags
        setTableViewBackground()
        
        var iconLocations: [String] = []
        
        for bag in gymBags {
            if let iconID = bag.bagIconID {
                iconLocations += [iconID]
            }
        }
        
        PullIconsHelper.pullIconImages(iconLocations: iconLocations, storageReference: storageReference, delegate: self)
    }
    
}

extension BagsListViewController: EmptyListDelegate {
    
    // MARK: - EmptyListView Methods
    func showAddOptions(_ sender: UIButton) {
        if let paid = paid, !paid && gymBags.count > 0 {
            let alert: UIAlertController = AlertHelper.getLimitAlert(listType: .bag, delegate: self)
            present(alert, animated: true, completion: nil)
            return
        }
        else if !FirebaseAuthValidationUtil.checkIfSignedIn() {
            
            let alert: UIAlertController = AlertHelper.getSignInToAddListsAlert(sender: sender, delegate: self)
            present(alert, animated: true, completion: nil)
        }
        
        let alertController: UIAlertController = AlertHelper.getAddOptionsActionSheet(listType: .bag, sender: sender, delegate: self)
        
        present(alertController, animated: true)
    }
    
}

extension BagsListViewController: AddOptionsActionSheetDelegate {
    
    func browseSelected(sender: UIButton) {
        performSegue(withIdentifier: Segue.toBrowseGymBags.stringValue, sender: sender)
    }
    
    func createSelected(sender: UIButton) {
        if FirebaseAuthValidationUtil.checkIfSignedIn() {
            performSegue(withIdentifier: Segue.toCreateGymBag.stringValue, sender: sender)
        } else {
            
            let alert: UIAlertController = AlertHelper.getSignInToAddListsAlert(sender: sender, delegate: self)
            present(alert, animated: true)
        }
    }
    
}

extension BagsListViewController: FirebaseAccountDelegate {
    
    func userPulled(user: User, current: Bool) {
        if current {
            paid = user.paid
            traineeIDs = user.trainees
            setUpTableView(isTrainer: user.isTrainer)
            setTableViewBackground()
            pullTrainees()
        } else {
            
            trainees = [user]
            
            traineesTableViewHandler.trainees = trainees!
            traineesTableView.reloadData()
        }
    }
    
    func signInSelected(_ sender: UIAlertAction) {
        accountTapped(UIButton(type: .system))
    }
    
    func upgradeTapped(sender: UIAlertAction) {
        accountTapped(UIButton(type: .system))
    }
    
}

extension BagsListViewController: HeaderBarDelegate {
    
    // This simply ensures that the screen is always able to open account
    func accountTapped(_ sender: UIButton) {
        // Open the account storyboard
        let storyboard = UIStoryboard.init(name: "Account", bundle: Bundle.main)
        let viewcontroller = storyboard.instantiateViewController(withIdentifier: Identifier.accountViewController.stringValue) as? AccountViewController
        
        present(viewcontroller!, animated: true, completion: nil)
    }
}

extension BagsListViewController: TableViewCellObjectDelegate {
    
    func objectSelected(object: Any, objectType: BrowseObjectType) { }
    
    func objectSelected(indexPath: IndexPath) {
        // Segue to the CreateGymBagViewController using the indexPath, which will trigger the new view controller to become populated with the information about the selected bag.
        performSegue(withIdentifier: Segue.toCreateGymBag.stringValue, sender: indexPath)
    }
    
    func shareTapped(_ sender: UIButton, indexPath: IndexPath) {
        bagToShare = gymBags[indexPath.row]
        
        if let traineeIDs = traineeIDs, let handler = traineesTableViewHandler {
            let alert: UIAlertController = AlertHelper.getTraineeListActionSheet(screenHeight: self.view.frame.height, selectMultiple: false, traineeIDs: traineeIDs, alertTableView: traineesTableView, tableViewHandler: handler, actionSheetDelegate: self, sender: sender)
            
            present(alert, animated: true)
        }
    }
    
    func cellDeleted(indexPath: IndexPath) {
        
        let id: String = bagIDs[indexPath.row]
        
        // Delete the gym bag from the "gymBags" root in Firebase.
        FirebaseGymBagsUtil.deleteFromFirebaseGymBagsRoot(bagID: id)
        
        // Remove the bag and bag IDs from the stored arrays.
        gymBags.remove(at: indexPath.row)
        bagIDs.remove(at: indexPath.row)
        
        // Delete the gym bag from the Firebase user in the "users" root.
        FirebaseGymBagsUtil.deleteFromFirebaseUserRoot(bagID: id)
        
        setTableViewBackground()
    }
    
}

extension BagsListViewController: ShareObjectDelegate {
    
    func sent(_ sent: Bool, object: Any, to user: User?) {
        
        if let bag = object as? GymBag, let user = user, sent {
            
            print("\(sent ? "Sent" : "Did not send") \(bag.bagName) to \(user.firstName) \(user.lastName)")
        }
    }
    
}

extension BagsListViewController: UITableViewActionSheetDelegate {
    
    func addToList(selectedIndexPaths: [IndexPath]) {
        if let bag = bagToShare, let traineeIDs = traineeIDs {
            FirebaseGymBagsUtil.pushNewGymBagToUser(userID: traineeIDs[selectedIndexPaths[0].row], gymBag: bag, delegate: self)
        }
    }
}

extension BagsListViewController: PullIconsDelegate {
    
    func iconLocationsPulled(iconLocations: [String]) {
        // Not selecting icons, so nothing else needs to happen here.
    }
    
    func iconImagesPulled(_ iconImage: (key: String, image: UIImage)) {
        
        if !iconImages.keys.contains(iconImage.key) {
            iconImages[iconImage.key] = iconImage.image
        }
        
        gymBagsTableViewHandler.cellImages = iconImages
        
        setTableViewBackground()
    }
    
    func singleIconImagePulled(iconImage: UIImage) {
        // Not displaying a single image on its own for this screen, so nothing else needs to happen here.
    }
}
