//
//  BrowseGymBagItemsViewController.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/16/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseStorage

class BrowseGymBagItemsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Variables
    var storageReference: StorageReference!
    var tableViewHandler: BrowseObjectsTableViewHandler!
    
    var userBagItems: [BagItem] = []
    var browsableBagItems: [BagItem] = []
    
    var iconImages: [String:UIImage] = [:]
    
    var userBagItemsPulled: Bool = false
    var browsableBagItemsPulled: Bool = false
    
    var bagsPulled: Int = 0
    
    var savedToFirebase: Bool = false
    var savedLocally: Bool = true // TODO: Change this to false when local saving is workting
    
    var selectedItem: BagItem?
    
    //MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        storageReference = Storage.storage().reference(withPath: "icons/").child("bagItemIcons//")
        setUpTableView()
        FirebaseGymBagsUtil.pullBagIDs(delegate: self)
        FirebaseGymBagsUtil.pullBrowsableGymBags(delegate: self)
    }
    
    //MARK: - Custom Methods
    private func checkIfShouldDismiss() {
        print("Saved to Firebase? -> \(savedToFirebase.description)\rSaved Locally? -> \(savedLocally.description)")
        if savedToFirebase && savedLocally {
            
            dismiss(animated: true, completion: nil)
        }
    }
    
    private func checkIfShouldLoadTableView() {
        
        if userBagItemsPulled && browsableBagItemsPulled {
            
            tableViewHandler.cellImages = iconImages
            
            if tableViewHandler.expandableCells.count < 2 {
                
                if userBagItems.count > 0 {
                    
                    tableViewHandler.expandableCells = [
                        ExpandableCellObject(title: "My Bag Items", icon: nil, objects: browsableBagItems, isExpanded: true),
                        ExpandableCellObject(title: "Fit+ Bag Items", icon: nil, objects: userBagItems, isExpanded: true)
                    ]
                }
                else {
                    
                    tableViewHandler.expandableCells = [
                        ExpandableCellObject(title: "Fit+ Bag Items", icon: nil, objects: browsableBagItems, isExpanded: true)
                    ]
                }
            }
            
            tableView.reloadData()
        }
    }
    
    private func setUpTableView() {
        let nib: UINib = UINib(nibName: NibName.collectionCell.stringValue, bundle: nil)
        
        tableView.backgroundColor = .clear
        tableView.backgroundView = UIView()
        tableView.separatorStyle = .none
        
        tableView.register(nib, forCellReuseIdentifier: Identifier.collectionCell.stringValue)
        tableViewHandler = BrowseObjectsTableViewHandler(expandableCells: [], singleObjectNames: [:], cellImages: iconImages, delegate: self)
        (tableView.delegate, tableView.dataSource) = (tableViewHandler, tableViewHandler)
    }
    
}

//MARK: - Extensions
extension BrowseGymBagItemsViewController {
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        //Go back to the previous screen
        
        dismiss(animated: true, completion: nil)
    }
    
}

extension BrowseGymBagItemsViewController: PullGymBagsDelegate {
    
    func gymBagIDsPulled(gymBagIDs: [String]) {
        if gymBagIDs.count > 0 {
            FirebaseGymBagsUtil.pullGymBagObjects(delegate: self, bagIDs: gymBagIDs)
        } else {
            userBagItemsPulled = true
            checkIfShouldLoadTableView()
        }
    }
    
    func gymBagObjectsPulled(gymBags: [GymBag], browseObjectType: BrowseObjectType, totalToPull: Int) {
        //TODO: Break up this method.
        var iconLocations: [String] = []
        
        for bag in gymBags {
            
            for item in bag.bagItems {
                
                if let iconID = item.itemIconID, !iconLocations.contains(iconID) {
                    
                    iconLocations += [iconID]
                }
            }
        }
        
        switch browseObjectType {
        case .browsable:
            
            for bag in gymBags {
                
                browsableBagItems += bag.bagItems
            }
            
            browsableBagItemsPulled = true
        case .user:
            
            for bag in gymBags {
                
                userBagItems += bag.bagItems
                bagsPulled += 1
            }
            
            if bagsPulled == totalToPull {
                userBagItemsPulled = true
            }
        }
        
        checkIfShouldLoadTableView()
        
        PullIconsHelper.pullIconImages(iconLocations: iconLocations, storageReference: storageReference, delegate: self)
    }
    
}

extension BrowseGymBagItemsViewController: TableViewCellObjectDelegate {
    
    func objectSelected(indexPath: IndexPath) { }
    
    func shareTapped(_ sender: UIButton, indexPath: IndexPath) { }
    
    func cellDeleted(indexPath: IndexPath) { }
    
    func objectSelected(object: Any, objectType: BrowseObjectType) {
        // Save the selected gym bag.
        if let selectedBagItem = object as? BagItem {
            
            selectedItem = selectedBagItem
            performSegue(withIdentifier: Segue.toPassBackBrowsableBagItem.stringValue, sender: nil)
        }
    }
    
}

extension BrowseGymBagItemsViewController: PullIconsDelegate {
    
    func iconLocationsPulled(iconLocations: [String]) {
        // Not selecting icons, so nothing else needs to happen here.
    }
    
    func iconImagesPulled(_ iconImage: (key: String, image: UIImage)) {
        if !iconImages.keys.contains(iconImage.key) {
            iconImages[iconImage.key] = iconImage.image
        }
        
        checkIfShouldLoadTableView()
    }
    
    func singleIconImagePulled(iconImage: UIImage) {
        // Not displaying a single image on its own for this screen, so nothing else needs to happen here.
    }
    
}
