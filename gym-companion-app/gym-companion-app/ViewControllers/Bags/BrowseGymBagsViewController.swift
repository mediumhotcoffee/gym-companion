//
//  BrowseGymBagsViewController.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/16/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseStorage

class BrowseGymBagsViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Variables
    var storageReference: StorageReference!
    var tableViewHandler: BrowseObjectsTableViewHandler!
    
    var userBags: [GymBag] = []
    var browsableBags: [GymBag] = []
    
    var iconImages: [String:UIImage] = [:]
    
    var userBagsPulled: Bool = false
    var browsableBagsPulled: Bool = false
    
    var savedToFirebase: Bool = false
    var savedLocally: Bool = true // TODO: Change this to false when local saving is workting
    
    //MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        storageReference = Storage.storage().reference(withPath: "icons/").child("gymBagIcons/")
        setUpTableView()
        FirebaseGymBagsUtil.pullBagIDs(delegate: self)
        FirebaseGymBagsUtil.pullBrowsableGymBags(delegate: self)
    }
    
    //MARK: - Custom Methods
    private func checkIfShouldDismiss() {
        print("Saved to Firebase? -> \(savedToFirebase.description)\rSaved Locally? -> \(savedLocally.description)")
        if savedToFirebase && savedLocally {
            
            dismiss(animated: true, completion: nil)
        }
    }
    
    private func checkIfShouldLoadTableView() {
        print("User Bags pulled? -> \(userBagsPulled.description)\rBrowsable Bags pulled? -> \(browsableBagsPulled.description)")
        if userBagsPulled && browsableBagsPulled {
            
            tableViewHandler.cellImages = iconImages
            
            if tableViewHandler.expandableCells.count < 2 {
                
                if userBags.count > 0 {
                    
                    tableViewHandler.expandableCells = [
                        ExpandableCellObject(title: "My Gym Bags", icon: nil, objects: userBags, isExpanded: true),
                        ExpandableCellObject(title: "Fit+ Gym Bags", icon: nil, objects: browsableBags, isExpanded: true)
                    ]
                }
                else {
                    
                    tableViewHandler.expandableCells = [
                        ExpandableCellObject(title: "Fit+ Gym Bags", icon: nil, objects: browsableBags, isExpanded: true)
                    ]
                }
            }
            
            tableView.reloadData()
        }
    }
    
    private func setUpTableView() {
        let nib: UINib = UINib(nibName: NibName.collectionCell.stringValue, bundle: nil)
        
        tableView.backgroundColor = .clear
        tableView.backgroundView = UIView()
        tableView.separatorStyle = .none
        
        tableView.register(nib, forCellReuseIdentifier: Identifier.collectionCell.stringValue)
        tableViewHandler = BrowseObjectsTableViewHandler(expandableCells: [], singleObjectNames: [:], cellImages: iconImages, delegate: self)
        (tableView.delegate, tableView.dataSource) = (tableViewHandler, tableViewHandler)
    }
    
}

extension BrowseGymBagsViewController {
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
    }
    
}

extension BrowseGymBagsViewController: PullGymBagsDelegate {
    
    func gymBagIDsPulled(gymBagIDs: [String]) {
        if gymBagIDs.count > 0 {
            FirebaseGymBagsUtil.pullGymBagObjects(delegate: self, bagIDs: gymBagIDs)
        } else {
            userBagsPulled = true
            checkIfShouldLoadTableView()
        }
    }
    
    func gymBagObjectsPulled(gymBags: [GymBag], browseObjectType: BrowseObjectType, totalToPull: Int) {
        //TODO: Break up this method.
        var iconLocations: [String] = []
        
        for bag in gymBags {
            if let iconID = bag.bagIconID {
                iconLocations += [iconID]
            }
        }
        
        switch browseObjectType {
        case .browsable:
            browsableBags = gymBags
            browsableBagsPulled = true
        case .user:
            userBags += gymBags
            
            if userBags.count == totalToPull {
                userBagsPulled = true
            }
        }
        
        checkIfShouldLoadTableView()
        
        PullIconsHelper.pullIconImages(iconLocations: iconLocations, storageReference: storageReference, delegate: self)
    }
    
}

extension BrowseGymBagsViewController: TableViewCellObjectDelegate {
    
    func objectSelected(indexPath: IndexPath) { }
    
    func shareTapped(_ sender: UIButton, indexPath: IndexPath) { }
    
    func cellDeleted(indexPath: IndexPath) { }
    
    func objectSelected(object: Any, objectType: BrowseObjectType) {
        // Save the selected gym bag.
        if let selectedGymBag = object as? GymBag {

            FirebaseGymBagsUtil.saveNewGymBagToGymBagsRoot(delegate: self, bagIconID: selectedGymBag.bagIconID, bagName: selectedGymBag.bagName, bagItems: selectedGymBag.bagItems)
        }
    }
    
}

extension BrowseGymBagsViewController: PullIconsDelegate {
    
    func iconLocationsPulled(iconLocations: [String]) {
        // Not selecting icons, so nothing else needs to happen here.
    }
    
    func iconImagesPulled(_ iconImage: (key: String, image: UIImage)) {
        if !iconImages.keys.contains(iconImage.key) {
            iconImages[iconImage.key] = iconImage.image
        }
        
        checkIfShouldLoadTableView()
    }
    
    func singleIconImagePulled(iconImage: UIImage) {
        // Not displaying a single image on its own for this screen, so nothing else needs to happen here.
    }
}

extension BrowseGymBagsViewController: SaveGymBagDelegate {
    
    func savedBagToUserRoot(_ saved: Bool, gymBag: GymBag) {
        savedToFirebase = true
        checkIfShouldDismiss()
    }
    
    func savedBagToGymBagsRoot(_ saved: Bool, gymBag: GymBag) {
        FirebaseGymBagsUtil.saveGymBagNewToUserRoot(delegate: self, gymBag: gymBag)
    }
    
    func bagUpdated(_ updated: Bool, gymBag: GymBag) {
        // Not updating bags here, only browsing to save new bags, so nothing else needs to happen here.
    }
    
    
    func savedBagLocally(_ saved: Bool, gymBag: GymBag) {
        //TODO: Save locally.
    }
    
}
