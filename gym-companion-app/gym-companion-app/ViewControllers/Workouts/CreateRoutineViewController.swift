//
//  CreateRoutineViewController.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 6/26/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class CreateRoutineViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var routineNameTextField: UITextField!
    @IBOutlet weak var categoriesButton: UIButton!
    @IBOutlet weak var iconButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var alertTableView: UITableView = UITableView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), style: .plain)
    
    //MARK: - Class Properties
    var ref: DatabaseReference!
    var storageReference: StorageReference!
    var iconTableViewHandler: IconActionSheetTableViewHandler!
    
    var passedRoutine: Routine?
    
    var exercises: [Exercise] = []
    var categoryOptions: [WorkoutCategory] = []
    var selectedCategories: [WorkoutCategory] = []
    
    var unsavedExerciseIDs: [String] = []
    var unsavedExercises: [Exercise] = []
    
    var routineIconID: String?
    var iconLocations: [String] = []
    var iconImages: [String:UIImage] = [:]
    
    var savedToRoutines: Bool = false
    var savedToUser: Bool = false
    
    var paid: Bool?
    
    //MARK: - Computed Properties
    var exerciseIDs: [String] {
        get {
            var ids: [String] = []
            
            for exercise in exercises {
                
                ids += [exercise.exerciseID]
            }
            
            return ids
        }
    }
    
    //MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        ref = Database.database().reference()
        storageReference = Storage.storage().reference(withPath: "icons/").child("routineIcons/")
        setButtonTextListener()
        setTextFieldListener()
        setMuscleGroupOptions()
        setUpTableView()
        toggleSaveButtonState()
        loadExercises()
        setInputs()
        showRoutineIcon()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        FirebaseAccountsUtil.getCurrentUser(delegate: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? CreateExerciseViewController {
            
            destination.routineName = routineNameTextField.text
            
            if let indexPath = sender as? IndexPath {
                destination.exercise = exercises[indexPath.row]
            }
        }
    }
    
    // Allows view to set a listener for when the text of the categoriesButton changes.
    // See setButtonTextListener.
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        toggleSaveButtonState()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK: - Custom Methods
    private func changeCategoryButtonText() {
        if selectedCategories.count < 1 {
            categoriesButton.setTitle("Select Categories", for: .normal)
        } else {
            var title: String = ""
            
            for (index, category) in selectedCategories.sorted(by: { $0.categoryName < $1.categoryName }).enumerated() {
                title += ""
                
                if index == selectedCategories.count - 1 {
                    title += category.categoryName
                } else {
                    title += "\(category.categoryName) | "
                }
            }
            
            categoriesButton.setTitle(title, for: .normal)
        }
    }
    
    private func checkIfShouldDismiss() {
        if savedToUser && savedToRoutines {
            navigationController?.popViewController(animated: true)
        }
    }
    
    private func deleteFromFirebase() {
        if let currentUser = Auth.auth().currentUser {
            let currentUserRef: DatabaseReference = ref.child("users").child(currentUser.uid)
            
            currentUserRef.observeSingleEvent(of: .value, with: {
                (userSnapshot) in
                if let userValue = userSnapshot.value as? NSDictionary {
                    if let currentUserObject = User(fromNSDictionary: userValue) {
                        currentUserObject.exercises = self.exerciseIDs
                        currentUserRef.setValue(currentUserObject.asDict)
                        
                        // This could be used later if I want to set up the empty view on this screen.
//                        if self.exerciseIDs.count < 1 {
//                            self.setUpEmptyView()
//                        }
                    }
                }
            }) { (error) in
                print(error.localizedDescription)
            }
        }
    }
    
    private func deleteFromFirebase(id: String) {
        ref.child("exercises").child(id).setValue(nil)
    }
    
    private func loadExercises() {
        exercises = unsavedExercises
        
        if let routineToEdit = passedRoutine {
            
            for id in routineToEdit.exercises {
                
                ref.child("exercises").child(id).observeSingleEvent(of: .value) { (exerciseSnapshot) in
                    
                    if let exerciseValue = exerciseSnapshot.value as? NSDictionary, let exerciseObject = Exercise(fromNSDictionary: exerciseValue, exerciseID: exerciseSnapshot.key) {
                        
                        self.exercises += [exerciseObject]
                        self.tableView.reloadData()
                    }
                }
            }
        }
        
        tableView.reloadData()
    }
    
    private func saveToFirebase() {
        if let routineToEdit = passedRoutine, let name = routineNameTextField.text {
            routineToEdit.name = name
            routineToEdit.categories = selectedCategories
            routineToEdit.exercises = exerciseIDs
        }
        
        if let validRoutine = saveToFirebaseRoutines() {
            saveToFirebaseUser(routine: validRoutine)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    private func saveToFirebaseRoutines() -> Routine? {
        let routinesRef: DatabaseReference = ref.child("routines")
        
        if let routineToEdit = passedRoutine {
            routineToEdit.routineIconID = routineIconID
            routinesRef.child(routineToEdit.routineID).setValue(routineToEdit.asDict)
            return nil
        }
        
        let newRoutineRef: DatabaseReference = routinesRef.childByAutoId()
        
        if let key = newRoutineRef.key, let name = routineNameTextField.text {
            if let currentUser = Auth.auth().currentUser {
                let routine: Routine = Routine(routineID: key, routineIconID: routineIconID, routineName: name, creatorID: currentUser.uid, categories: selectedCategories, exercises: exerciseIDs)
                
                newRoutineRef.setValue(routine.asDict) {
                    (error:Error?, ref:DatabaseReference) in
                    
                    if let error = error {
                        print("Data could not be saved: \(error).")
                    } else {
                        
                        print("Data saved successfully!")
                        self.savedToRoutines = true
                        self.checkIfShouldDismiss()
                    }
                }
                
                return routine
            } else {
                
                // User is not signed in.
                // TODO: Alert user that for the alpha, saving locally is not yet up and running. To create routines, they must create an account and be signed in.
            }
        }
        
        return nil
    }
    
    private func saveToFirebaseUser(routine: Routine) {
        
        if self.passedRoutine != nil {
            return
        }
        
        let usersRef: DatabaseReference = ref.child("users")
        
        if let currentUser = Auth.auth().currentUser {
            
            let currentUserRef: DatabaseReference = usersRef.child(currentUser.uid)
            
            currentUserRef.observeSingleEvent(of: .value, with: { (userSnapshot) in
                
                if let userValue = userSnapshot.value as? NSDictionary {
                    if let currentUserObject = User(fromNSDictionary: userValue) {
                        currentUserObject.routines += [routine.routineID]
                        currentUserRef.setValue(currentUserObject.asDict) {
                            (error:Error?, ref:DatabaseReference) in
                            
                            if let error = error {
                                print("Data could not be saved: \(error).")
                            } else {
                                
                                print("Data saved successfully!")
                                self.savedToUser = true
                                self.checkIfShouldDismiss()
                            }
                        }
                    }
                }
            }) { (error) in
                print("\r\n\r\n------------------ CreateWorkoutsViewController.swift | getUserObject -> Description: \(error.localizedDescription) ------------------\r\n\r\n")
            }
        }
    }
    
    private func setInputs() {
        if let routineToEdit = passedRoutine {
            
            routineNameTextField.text = routineToEdit.name
            selectedCategories = routineToEdit.categories
            changeCategoryButtonText()
        }
    }
    
    private func showMuscleGroupSelectionActionSheet() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let margin:CGFloat = 8.0
        let rect = CGRect(x: margin, y: margin, width: alertController.view.bounds.size.width - margin * 4.0, height: self.view.frame.height * 0.4)
        
        alertTableView = UITableView(frame: rect)
        alertTableView.delegate = self
        alertTableView.dataSource = self
        alertTableView.allowsMultipleSelection = true
        alertTableView.allowsMultipleSelectionDuringEditing = true
        alertTableView.backgroundColor = UIColor.clear
        
        alertController.view.addSubview(alertTableView)
        
        let addAction = UIAlertAction(title: "Add", style: .default) { (action) in
            if let selectedIndexPaths = self.alertTableView.indexPathsForSelectedRows {
                
                for path in selectedIndexPaths {
                    
                    if !self.selectedCategories.contains(self.categoryOptions[path.row]) {
                        
                        self.selectedCategories += [self.categoryOptions[path.row]]
                    }
                    
                }
            }
            
            self.changeCategoryButtonText()
        }
        
        alertController.addAction(addAction)
        
        //Cancel action
        alertController.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""),
                                                style: UIAlertAction.Style.cancel))
        
        let height:NSLayoutConstraint = NSLayoutConstraint(item: alertController.view!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: self.view.frame.height * 0.6)
        
        alertController.view.addConstraint(height)
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = categoriesButton
            popoverController.sourceRect = categoriesButton.bounds
        }
        
        self.present(alertController, animated: true, completion:nil)
    }
    
    private func setButtonTextListener() {
        let options = NSKeyValueObservingOptions([.new])
        
        self.addObserver(self, forKeyPath: "categoriesButton.titleLabel.text", options: options, context: nil)
    }
    
    private func setMuscleGroupOptions() {
        WorkoutCategory.allCases.forEach { (option) in
            categoryOptions += [option]
        }
    }
    
    private func setTextFieldListener() {
        routineNameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        routineNameTextField.addTarget(self, action: #selector(textFieldEditingDidEnd(_:)), for: .editingDidEnd)
    }
    
    // Either shows or hids the save button depending on whether or not the routine has been given a name and at least one category.
    private func toggleSaveButtonState() {
        guard let textViewText = routineNameTextField.text, !textViewText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty, let buttonText = categoriesButton.titleLabel?.text, buttonText != "Select Categories"
            else {
            
            if routineNameTextField.text == "" || routineNameTextField.text?.count ?? 0 < 2 {
                routineNameTextField.text = routineNameTextField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            }
            
            saveButton.isEnabled = false
            saveButton.isHidden = true
            return
        }
        
        saveButton.isEnabled = true
        saveButton.isHidden = false
    }
    
    // Sets the delegate and data source for the table view, allowing this ViewController to control the tableview.
    private func setUpTableView() {
        (tableView.dataSource, tableView.delegate) = (self, self)
    }
    
    // When add is tapped, show a bottom action sheet that allows the user to either create or browse exercises.
    func showAddOptions(_ sender: UIButton) {
        if let paid = paid, !paid && exercises.count > 4 {
            
            let alert: UIAlertController = AlertHelper.getLimitAlert(listType: .exercise, delegate: self)
            present(alert, animated: true)
            return
        } else if !FirebaseAuthValidationUtil.checkIfSignedIn() {
            let alert: UIAlertController = AlertHelper.getSignInToAddListsAlert(sender: sender, delegate: self)
            present(alert, animated: true)
        }
        
        let alertController: UIAlertController = AlertHelper.getAddOptionsActionSheet(listType: .exercise, sender: sender, delegate: self)
        
        self.present(alertController, animated: true)
    }
    
    private func showRoutineIcon() {
        if let itemLocation = routineIconID, let image = iconImages[itemLocation] {
            iconButton.setImage(image, for: .normal)
        } else if let routine = passedRoutine, let location = routine.routineIconID {
            
            PullIconsHelper.pullSingleIconImage(location: location, storageReference: storageReference, delegate: self)
        }
    }
    
    private func showIconList() {
        iconTableViewHandler = IconActionSheetTableViewHandler(iconLocations: iconLocations, iconImages: iconImages)
        
        let alertController: UIAlertController = AlertHelper.getListActionSheet(screenHeight: self.view.frame.height, selectMultiple: false, tableViewHandler: iconTableViewHandler, actionSheetDelegate: self)
        
        if let popoverController = alertController.popoverPresentationController {
            let source: CGRect = CGRect(
                x: iconButton.bounds.origin.x,
                y: iconButton.bounds.origin.y,
                width: iconButton.bounds.width,
                height: iconButton.bounds.height)
            
            popoverController.sourceView = iconButton
            popoverController.sourceRect = source
        }
        
        self.present(alertController, animated: true)
    }
}

extension CreateRoutineViewController {
    
    @IBAction func addExerciseTapped(_ sender: UIButton) {
        showAddOptions(sender)
    }
    
    @IBAction func chooseIconTapped(_ sender: UIButton) {
        PullIconsHelper.pullIconLocations(from: ListType.routine.stringValue, delegate: self, listType: ListType.routine)
    }
    
    @IBAction func muscleGroupsTapped(_ sender: UIButton) {
        showMuscleGroupSelectionActionSheet()
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveTapped(_ sender: UIButton) {
        //TODO: Save locally
        saveToFirebase()
    }
    
    @IBAction func passExerciseBackToRoutine(_ segue: UIStoryboardSegue) {
        if let createExerciseViewController = segue.source as? CreateExerciseViewController,
            let exercise = createExerciseViewController.exercise {
            
            if !exerciseIDs.contains(exercise.exerciseID) {
                
                if !unsavedExerciseIDs.contains(exercise.exerciseID) {
                    
                    unsavedExerciseIDs += [exercise.exerciseID]
                    unsavedExercises += [exercise]
                }
            }
            
            loadExercises()
        }
        if let browseExerciseViewController = segue.source as? BrowseExercisesViewController,
            let exercise = browseExerciseViewController.selectedExercise {
            
            if !exerciseIDs.contains(exercise.exerciseID) {
                
                if !unsavedExerciseIDs.contains(exercise.exerciseID) {
                    
                    unsavedExerciseIDs += [exercise.exerciseID]
                    unsavedExercises += [exercise]
                }
            }
            
            loadExercises()
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        toggleSaveButtonState()
    }
    
    @objc func textFieldEditingDidEnd(_ textField: UITextField) {
        textField.text = textField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
}

//MARK: - Extensions
extension CreateRoutineViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.isEqual(self.tableView) {
            return exercises.count
        } else if tableView.isEqual(alertTableView) {
            return categoryOptions.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.isEqual(self.tableView) {

            let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath)
            cell.textLabel?.text = exercises[indexPath.row].name
            cell.textLabel?.textColor = .appDarkColor
            
            if (UIDevice.current.userInterfaceIdiom == .pad)
            {
                // Ipad
                cell.textLabel?.font = UIFont.systemFont(ofSize: 26, weight: .medium)
            }
            else
            {
                // Iphone
                cell.textLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            }
            
            cell.backgroundView?.backgroundColor = .appTransparentWhite
            cell.backgroundColor = .appTransparentWhite
            
            return cell
        } else if tableView.isEqual(alertTableView) {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "alertCellID")
            cell.textLabel?.text = categoryOptions[indexPath.row].categoryName
            
            if selectedCategories.contains(categoryOptions[indexPath.row]) {
                alertTableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            }
            
            if let selected = alertTableView.indexPathsForSelectedRows?.contains(indexPath) {
                cell.textLabel?.textColor = selected ? UIColor.appWhite : UIColor.appDarkColor
            }
            
            cell.backgroundColor = UIColor.clear

            let view: UIView = UIView(frame: cell.frame)
            view.alpha = 0.5
            view.backgroundColor = UIColor.appGreen

            cell.selectedBackgroundView = view
            
            return cell
        }
        
        return UITableViewCell(style: .default, reuseIdentifier: "errorCellID")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.isEqual(self.tableView) {
            tableView.deselectRow(at: indexPath, animated: true)
            
            performSegue(withIdentifier: Segue.toCreateExercise.stringValue, sender: indexPath)
        } else if tableView.isEqual(alertTableView) {
            tableView.cellForRow(at: indexPath)?.textLabel?.textColor = UIColor.appWhite
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView.isEqual(alertTableView) {
            tableView.cellForRow(at: indexPath)?.textLabel?.textColor = UIColor.appDarkColor
            
            if let index = selectedCategories.firstIndex(of: categoryOptions[indexPath.row]) {
                
                selectedCategories.remove(at: index)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if tableView.isEqual(self.tableView) && editingStyle == .delete {
            deleteFromFirebase(id: exerciseIDs[indexPath.row])
            
            exercises.remove(at: indexPath.row)
            
            deleteFromFirebase()
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
}

extension CreateRoutineViewController: UITableViewActionSheetDelegate {
    
    func addToList(selectedIndexPaths: [IndexPath]) {
        if selectedIndexPaths.count > 0 {
            
            routineIconID = iconLocations[selectedIndexPaths[0].row]
            showRoutineIcon()
        }
    }
    
}

extension CreateRoutineViewController: PullIconsDelegate {
    
    func iconLocationsPulled(iconLocations: [String]) {
        
        self.iconLocations = iconLocations
        iconImages = [:]
        PullIconsHelper.pullIconImages(iconLocations: iconLocations, storageReference: storageReference, delegate: self)
    }
    
    func iconImagesPulled(_ iconImage: (key: String, image: UIImage)) {
        iconImages[iconImage.key] = iconImage.image
        
        if iconLocations.count == iconImages.count {
            showIconList()
        }
    }
    
    func singleIconImagePulled(iconImage: UIImage) {
        iconButton.setImage(iconImage, for: .normal)
    }
    
}

extension CreateRoutineViewController: AddOptionsActionSheetDelegate {
    
    func browseSelected(sender: UIButton) {
        if FirebaseAuthValidationUtil.checkIfSignedIn() {
            performSegue(withIdentifier: Segue.toBrowseExercises.stringValue, sender: sender)
        } else {
            let alert: UIAlertController = AlertHelper.getSignInToAddListsAlert(sender: sender, delegate: self)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func createSelected(sender: UIButton) {
        if FirebaseAuthValidationUtil.checkIfSignedIn() {
            
            performSegue(withIdentifier: Segue.toCreateExercise.stringValue, sender: sender)
        } else {
            let alert: UIAlertController = AlertHelper.getSignInToAddListsAlert(sender: sender, delegate: self)
            present(alert, animated: true)
        }
    }
    
}

extension CreateRoutineViewController: FirebaseAccountDelegate {
    
    func userPulled(user: User, current: Bool) {
        if current {
            paid = user.paid
        }
    }
    
    func signInSelected(_ sender: UIAlertAction) {
        accountTapped(UIButton(type: .system))
    }
    
    func upgradeTapped(sender: UIAlertAction) {
        accountTapped(UIButton(type: .system))
    }
    
    func accountTapped(_ sender: UIButton) {
        
        // Open the account storyboard
        print("Account clicked")
        
        let storyboard = UIStoryboard.init(name: "Account", bundle: Bundle.main)
        let viewcontroller = storyboard.instantiateViewController(withIdentifier: Identifier.accountViewController.stringValue) as? AccountViewController
        
        present(viewcontroller!, animated: true, completion: nil)
    }
    
}
