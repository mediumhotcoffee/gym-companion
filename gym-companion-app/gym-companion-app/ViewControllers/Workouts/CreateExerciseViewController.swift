//
//  CreateExerciseViewController.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 6/30/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import Firebase

class CreateExerciseViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var routineNameLabel: UILabel!
    @IBOutlet weak var categoriesButton: UIButton!
    @IBOutlet weak var weightedSwitch: UISwitch!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet var textFields: [UITextField]!
    
    //MARK: - Variables
    var alertTableView: UITableView!
    
    var routineName: String!
    
    var exercise: Exercise?
    var ref: DatabaseReference!
    var categoryOptions: [WorkoutCategory] = []
    var selectedCategories: [WorkoutCategory] = []
    
    var savedToExercises: Bool = false
    var savedToUser: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ref = Database.database().reference()
        alertTableView = UITableView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), style: .plain)
        weightedSwitch.addTarget(self, action: #selector(weightedSwitched(_:)), for: .valueChanged)
        
        setNumberTextFieldDelegates()
        setTextFieldListeners()
        setCategoryOptions()
        setUpInputs()
    }
    
    // Dismisses keyboard on touch oustide of keyboard.
    override func touchesBegan(_ touches: Set<UITouch>,
                               with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK: - Custom Methods
    private func changeCategoryButtonText() {
        if selectedCategories.count < 1 {
            categoriesButton.setTitle("Select Categories", for: .normal)
        } else {
            var title: String = ""
            
            for (index, category) in selectedCategories.sorted(by: { $0.categoryName < $1.categoryName }).enumerated() {
                title += ""
                
                if index == selectedCategories.count - 1 {
                    title += category.categoryName
                } else {
                    title += "\(category.categoryName) | "
                }
            }
            
            categoriesButton.setTitle(title, for: .normal)
        }
    }
    
    private func checkIfShouldDismiss() {
        
        if savedToUser && savedToExercises {
            
            performSegue(withIdentifier: Segue.toPassBackExercise.stringValue, sender: nil)
        }
    }
    
    private func saveToFirebase() {
        var nameTextField: UITextField?
        var restTimeTextField: UITextField?
        var repsTextField: UITextField?
        var setsTextField: UITextField?
        var weightTextField: UITextField?
        
        for textField in textFields {
            switch textField.tag {
            case 0:
                nameTextField = textField
            case 1:
                restTimeTextField = textField
            case 2:
                repsTextField = textField
            case 3:
                setsTextField = textField
            case 4:
                weightTextField = textField
            default:
                print("\r\n\r\n--------- CreateExerciseViewController | saveToFirebaseExercises -> Error, invalid case.\r\nTag: \(textField.tag) ---------\r\n\r\n")
            }
        }
        
        if let passedExercise = self.exercise {
            
            FirebaseWorkoutsUitl.updateExercise(delegate: self, exercise: passedExercise, nameTextField: nameTextField, restTimeTextField: restTimeTextField, repsTextField: repsTextField, setsTextField: setsTextField, weightTextField: weightTextField, selectedCategories: selectedCategories)
            return
        }
        
        FirebaseWorkoutsUitl.saveNewExerciseToExerciseRoot(ref: ref, delegate: self, nameTextField: nameTextField, restTimeTextField: restTimeTextField, repsTextField: repsTextField, setsTextField: setsTextField, weightTextField: weightTextField, selectedCategories: selectedCategories)
    }

    private func setCategoryOptions() {
        WorkoutCategory.allCases.forEach { (option) in
            categoryOptions += [option]
        }
    }
    
    private func setNumberTextFieldDelegates() {
        
        for textField in textFields {
            
            if textField.tag > 0 && textField.tag < 5 {
                
                textField.delegate = self
            }
        }
    }

    private func setTextFieldListeners() {
        for textField in textFields {
            textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        }
    }
    
    private func setUpInputs() {
        if routineName != "" {
            routineNameLabel.text = routineName
        } else {
            routineNameLabel.isHidden = true
        }
        
        if let passedExercise = self.exercise {
            self.selectedCategories = passedExercise.categories
            changeCategoryButtonText()
            
            for textField in textFields {
                switch textField.tag {
                case 0: // Exercise Name
                    textField.text = passedExercise.name
                case 1: // Rest Time
                    textField.text = passedExercise.restTimeSeconds.description
                case 2: // Reps
                    textField.text = passedExercise.reps.description
                case 3: // Sets
                    textField.text = passedExercise.sets.description
                case 4: // Weight
                    if let weight = passedExercise.weight {
                        weightedSwitch.isOn = true
                        weightTextField.text = weight.description
                        weightTextField.isHidden = false
                    } else {
                        weightedSwitch.isOn = false
                        weightTextField.text = ""
                        weightTextField.isHidden = true
                    }
                default:
                    print("\r\n\r\n--------- CreateExerciseViewController | saveToFirebaseExercises -> Error, invalid case.\r\nTag: \(textField.tag) ---------\r\n\r\n")
                }
            }
        }
    }
    
    private func showCategoriesSelectionActionSheet() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let margin:CGFloat = 8.0
        let rect = CGRect(x: margin, y: margin, width: alertController.view.bounds.size.width - margin * 4.0, height: self.view.frame.height * 0.4)
        
        alertTableView = UITableView(frame: rect)
        alertTableView.delegate = self
        alertTableView.dataSource = self
        alertTableView.allowsMultipleSelection = true
        alertTableView.allowsMultipleSelectionDuringEditing = true
        alertTableView.backgroundColor = UIColor.clear
        
        alertController.view.addSubview(alertTableView)
        
        let addAction = UIAlertAction(title: "Add", style: .default) { (action) in
            
            if let selectedIndexPaths = self.alertTableView.indexPathsForSelectedRows {
                
                for path in selectedIndexPaths {
                    if !self.selectedCategories.contains(self.categoryOptions[path.row]) {
                        
                        self.selectedCategories += [self.categoryOptions[path.row]]
                    }
                }
            }
            
            self.changeCategoryButtonText()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        
        alertController.addAction(addAction)
        alertController.addAction(cancelAction)
        
        let height:NSLayoutConstraint = NSLayoutConstraint(item: alertController.view!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: self.view.frame.height * 0.6)
        
        alertController.view.addConstraint(height)
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = categoriesButton
            popoverController.sourceRect = categoriesButton.bounds
        }
        
        self.present(alertController, animated: true)
    }
}

extension CreateExerciseViewController {
    
    @IBAction func categoriesTapped(_ sender: UIButton) {
        showCategoriesSelectionActionSheet()
    }
    
    @IBAction func saveTapped(_ sender: UIButton) {
        if validateAllTextFields() && validateCategoryButtonText() {
            saveToFirebase()
        }
    }
    
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @objc func weightedSwitched(_ sender: UISwitch) {
        if sender.isOn {
            UIView.animate(withDuration: 0.5, animations: {
                self.weightTextField.isHidden = false
                self.weightLabel.isHidden = false
                self.weightTextField.alpha = 1
                self.weightLabel.alpha = 1
            })
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.weightTextField.alpha = 0
                self.weightLabel.alpha = 0
            }, completion: { (bool) in
                self.weightTextField.isHidden = true
                self.weightLabel.isHidden = true
            })
        }
    }
    
    @objc func textFieldDidChange(_ sender: UITextField) {
        switch sender.tag {
        case 0: // Exercise Name
            print(validateNameTextField(sender))
        case 1: // Rest Time
            print(validateRestTimeTextField(sender))
        case 2: // Reps
            print(validateRepsTextField(sender))
        case 3: // Sets
            print(validateSetsTextField(sender))
        case 4: // Weight (Can Be Hidden)
            print(validateWeightTextField(sender))
        default: // >>> Error <<<
            print("\r\n\r\n--------- CreateExerciseViewController | textViewDidChange -> Error, invalid case.\r\nTag: \(sender.tag) ---------\r\n\r\n")
        }
    }
}

extension CreateExerciseViewController {
    
    private func validateNameTextField(_ sender: UITextField) -> Bool {
        return ValidationHelper.textFieldIsNotEmpty(sender, setIcon: false)
    }
    
    private func validateRestTimeTextField(_ sender: UITextField) -> Bool {
        return ValidationHelper.textFieldInputIsInt(sender, setIcon: false, maxDigits: 3)
    }
    
    private func validateRepsTextField(_ sender: UITextField) -> Bool {
        return ValidationHelper.textFieldInputIsInt(sender, setIcon: false, maxDigits: 3)
    }
    
    private func validateSetsTextField(_ sender: UITextField) -> Bool {
        return ValidationHelper.textFieldInputIsInt(sender, setIcon: false, maxDigits: 3)
    }
    
    private func validateWeightTextField(_ sender: UITextField) -> Bool {
        return ValidationHelper.textFieldInputIsInt(sender, setIcon: false, maxDigits: 3)
    }
    
    private func validateCategoryButtonText() -> Bool {
        if ValidationHelper.buttonText(isNot: "Select Categories", for: categoriesButton) {
            return true
        } else {
            ValidationHelper.emphasizeInvalidButton(button: categoriesButton)
            return false
        }
    }
    
    private func validateAllTextFields() -> Bool {
        var allValid: Bool = true
        
        for textField in textFields {
            var textFieldValid: Bool = false
            
            switch textField.tag {
            case 0:
                textFieldValid = ValidationHelper.textFieldIsNotEmpty(textField, setIcon: true)
            case 1...3:
                textFieldValid = ValidationHelper.textFieldInputIsInt(textField, setIcon: true, maxDigits: 3)
            case 4:
                if !textField.isHidden {
                    textFieldValid = ValidationHelper.textFieldInputIsInt(textField, setIcon: true, maxDigits: 3)
                } else {
                    textFieldValid = true
                }
            default:
                print("\r\n\r\n--------- CreateExerciseViewController | validateAllTextFields -> Error, invalid case.\r\nTag: \(textField.tag) ---------\r\n\r\n")
                
            }
            
            print("All Valid: \(allValid)")
            allValid = allValid && textFieldValid
            print("All Valid 2: \(allValid)")
        }
        
        return allValid
    }
}

extension CreateExerciseViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.isEqual(alertTableView) {
            return categoryOptions.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.isEqual(alertTableView) {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "alertCellID")
            cell.textLabel?.text = categoryOptions[indexPath.row].categoryName
            
            if selectedCategories.contains(categoryOptions[indexPath.row]) {
                alertTableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            }
            
            if let selected = alertTableView.indexPathsForSelectedRows?.contains(indexPath) {
                
                cell.textLabel?.textColor = selected ? UIColor.appWhite : UIColor.appDarkColor
            }
            
            if cell.isSelected { }
            
            cell.backgroundColor = UIColor.clear
            
            let view: UIView = UIView(frame: cell.frame)
            view.alpha = 0.5
            view.backgroundColor = UIColor.appGreen
            
            cell.selectedBackgroundView = view
            
            return cell
        }
        
        return UITableViewCell(style: .default, reuseIdentifier: "errorCellID")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.isEqual(alertTableView) {
            tableView.cellForRow(at: indexPath)?.textLabel?.textColor = UIColor.appWhite
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView.isEqual(alertTableView) {
            tableView.cellForRow(at: indexPath)?.textLabel?.textColor = UIColor.appDarkColor
            
            if let index = selectedCategories.firstIndex(of: categoryOptions[indexPath.row]) {
                selectedCategories.remove(at: index)
            }
        }
    }
    
}

extension CreateExerciseViewController: SaveExerciseDelegate {
    
    func savedExerciseToExerciseRoot(exercise: Exercise, _ saved: Bool) {
        savedToExercises = saved
        self.exercise = exercise
        FirebaseWorkoutsUitl.saveNewExerciseToUserRoot(delegate: self, exercise: exercise)
    }
    
    func savedExerciseToUserRoot(_ saved: Bool) {
        savedToUser = saved
        checkIfShouldDismiss()
    }
    
    func updatedExercise(_ updated: Bool) {
        
        savedToUser = updated
        savedToExercises = updated
        checkIfShouldDismiss()
    }
}

extension CreateExerciseViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //Prevent non-digit characters from being typed in a text
        //field
        
        //Limit the number of characters in the text field to 3
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        //Creates a set of invalid characters by excluding digits from the set of all characters
        let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
        
        return string.rangeOfCharacter(from: invalidCharacters) == nil
            && updatedText.count <= 3
    }
}
