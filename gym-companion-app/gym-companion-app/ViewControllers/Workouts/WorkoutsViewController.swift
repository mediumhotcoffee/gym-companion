//
//  ViewController.swift
//  gym-companion-app
//
//  Created by Marcos Felipe Costa da Silva on 6/5/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase
import HealthKit

class WorkoutsViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var accountButton: UIButton!
    @IBOutlet weak var addRoutineButton: UIButton!
    
    //MARK: - HealthKit variables
    let healthKitSetupAssistant: HealthKitSetupAssistant = HealthKitSetupAssistant.sharedInstance
    var healthStore: HKHealthStore!
    
    //MARK: - Other variables
    var ref: DatabaseReference!
    var storageReference: StorageReference!
    var routineIDs: [String] = []
    var routines: [Routine] = []
    
    var traineeIDs: [String]?
    var trainees: [User] = []
    var routineToShare: Routine?
    
    var exerciseNames: [String:String] = [:]
    var iconImages: [String:UIImage] = [:]
    
    var traineesTableView: UITableView!
    var routinesTableViewHandler: CollectionObjectsTableViewHandler!
    var traineesTableViewHandler: TraineeListActionSheetTableViewHandler!

    var paid: Bool?
    
    //MARK: - Lifecycle Methods
    override func viewWillAppear(_ animated: Bool) {
        //Hide Navigation Controller. The only purpose of the Navigation Controller is to maintin the tab bar through all segues.
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        //Setup
        ImageUtil.loadUserPictureOn(button: accountButton, userId: Auth.auth().currentUser?.uid ?? "")
        loadRoutineIDs()
        FirebaseAccountsUtil.getCurrentUser(delegate: self)
        trainees = []
        traineesTableViewHandler = TraineeListActionSheetTableViewHandler(trainees: [])
        saveRoutineJSON()
        //print(JSONHelper().readCurrentUserRoutinesJSON().debugDescription)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Store a reference to the Firebase database so it can be used for queries later.
        ref = Database.database().reference()
        setUpTableView(isTrainer: false)
        setUpTraineesTableView()
        
        storageReference = Storage.storage().reference(withPath: "icons/").child("routineIcons/")
        
        //Request HealthKit authorization on the iPhone when the app starts
        authorizeHealthKit()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        JSONHelper().saveCurrentUserRoutines()
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // If the sender is an IndexPath, that means the segue was called from a tap on an already existing routine on the tableview. The segue should populate the CreateRoutineViewController with info from the selected routine.
        // The only other way to the CreateRoutineViewController from here is by tapping on create routine, in which case nothing needs passed because a new routine will be created from scratch. So, nothing needs to happen here for that situation.
        if let indexPath = sender as? IndexPath, let destination = segue.destination as? CreateRoutineViewController {
            destination.passedRoutine = routines[indexPath.row]
        }
    }
    
    //MARK: - Custom Methods
    private func deleteFromFirebase() {
        // Get the current user
        if let currentUser = Auth.auth().currentUser {
            // Finds the current user in the database.
            let currentUserRef: DatabaseReference = ref.child("users").child(currentUser.uid)
            
            // Drill into the current user in the database.
            currentUserRef.observeSingleEvent(of: .value, with: {
                (userSnapshot) in
                
                // Makes sure the current user exists in the database to ensure there is data to work with.
                if let userValue = userSnapshot.value as? NSDictionary {
                    
                    // Attempts to instantiate a User object from the NSDictionary object pulled for the user from Firebase.
                    if let currentUserObject = User(fromNSDictionary: userValue) {
                        
                        // If User object was successfully created, we have the correct info in the database.
                        // At this point, the routineID to be deleted has already been removed from the routineIDs array.
                        // So, overriding the user's routines in Firebase with the routineIDs array for the tableview effectively removes the routineID to be deleted from Firebase.
                        currentUserObject.routines = self.routineIDs
                        currentUserRef.setValue(currentUserObject.asDict)
                        
                        // If deleting the routine cause there to be no routines left, show the empty state view.
                        if self.routineIDs.count < 1 {
                            self.showEmptyView()
                        }
                    }
                }
            }) {
                (error) in
                print(error.localizedDescription)
            }
        }
    }
    
    // Deletes a routine from the "routines" Firebase root using a routineID.
    private func deleteFromFirebase(id: String) {
        
        // Drill into routines, then to the routineID, and delete that routine from the database.
        ref.child("routines").child(id).setValue(nil)
    }
    
    private func loadRoutineIDs() {
        
        // Make sure to start with no IDs so as to not create duplicates when loading.
        routines = []
        routineIDs = []
        routinesTableViewHandler.collectionObjects = []
        // Start with an empty view. This will be fixed later if routines are found successfully. Otherwise, the empty state should be shown by default.
        setTableViewBackground()
        
        // Get the current user.
        if let currentUser = Auth.auth().currentUser {
            
            // Drill into the "users" route and then into the current user in the database.
            ref.child("users").child(currentUser.uid).observeSingleEvent(of: .value, with: {
                (userSnapshot) in
                
                // Make sure the user at this location is valid.
                if let userValue = userSnapshot.value as? NSDictionary {
                    
                    // Try to instantiate a User object from the NSDictionary object pulled down from Firebase.
                    if let currentUserObject = User(fromNSDictionary: userValue) {
                        
                        // If successful, we have a valid User object.
                        // Update the routineIDs array with the user's routines so the tableview can be populated.
                        self.routineIDs = currentUserObject.routines
                        
                        if self.routineIDs.count > 0 {
                            
                            // If there are routines, show them.
                            self.setTableViewBackground()
                            self.showRoutines()
                        }
                    }
                }
            }) {
                (error) in
                print(error.localizedDescription)
            }
        }
    }
    
    private func pullExerciseNames(ids: [String]) {
        
        for id in ids {
            
            let exercisesRef: DatabaseReference = ref.child(FirebaseDatabaseRoot.exercises.stringValue)
            let exerciseRef: DatabaseReference = exercisesRef.child(id)
            
            exerciseRef.observeSingleEvent(of: .value
                , with: {
                    (exerciseSnapshot) in
                    
                    if let exerciseValue = exerciseSnapshot.value as? NSDictionary, let exerciseObject = Exercise(fromNSDictionary: exerciseValue, exerciseID: id) {
                        
                        self.exerciseNames[id] = exerciseObject.name
                        self.routinesTableViewHandler.singleItemNames = self.exerciseNames
                        self.tableView.reloadData()
                    }
            }) {
                (error) in
                print(error.localizedDescription)
            }
        }
    }
    
    private func pullTrainees() {
        if let ids = traineeIDs {
            for id in ids {
                FirebaseAccountsUtil.getUser(userID: id, delegate: self)
            }
        }
    }
    
    private func saveRoutineJSON() {
        JSONHelper().saveCurrentUserRoutines()
    }
    
    private func showRoutines() {
        // If there are routines, the add button on the empty state will be hidden, so the addRoutineButton needs to be visible.
        addRoutineButton.isHidden = false
        
        // Loop through all the routineIDs pulled from the Firebase user.
        for id in routineIDs {
            
            // Drill into the "routines" root, then into this routine.
            ref.child("routines").child(id).observeSingleEvent(of: .value) {
                (routineSnapshot) in
                
                // Make sure the routine at this location is valid.
                if let routineDictionary = routineSnapshot.value as? NSDictionary {
                    
                    // Attempt to instantiate a Routine object from the NSDictionary pulled from Firebase.
                    if let routineObject = Routine(fromNSDictionary: routineDictionary, routineID: routineSnapshot.key) {
                        
                        // If a Routine object was created successfully, add it to the array of routines and show it in the tableview.
                        self.routines += [routineObject]
                        self.pullExerciseNames(ids: routineObject.exercises)
                        
                        var iconLocations: [String] = []
                        
                        for routine in self.routines {
                            if let iconID = routine.routineIconID {
                                iconLocations += [iconID]
                            }
                        }
                        
                        PullIconsHelper.pullIconImages(iconLocations: iconLocations, storageReference: self.storageReference, delegate: self)
                        self.setTableViewBackground()
                    }
                }
            }
        }
    }
    
    private func showLimitAlert() {
        let alert: UIAlertController = AlertHelper.getLimitAlert(listType: .routine, delegate: self)
        present(alert, animated: true)
    }
    
    private func showEmptyView() {
        // Displays the empty list message with a button to add more bags.
        // This is done by using the UIEmptyListView nib file.
        
        // Get a UIEmptyListView from the nib so the outlets can be manipulated.
        let emptyView: UIEmptyListView = UIEmptyListView.emptyViewFromNib()
        // Make sure the empty state refers to routines and set the delegate to self to allow for detection of when the add routine button is tapped.
        emptyView.setupView(listType: .routine)
        emptyView.delegate = self
        
        // Hide the addRoutineButton because the empty state has its own add routine button that will be visible.
        tableView.backgroundView = emptyView
        addRoutineButton.isHidden = true
        
        // Show the empty view.
        tableView.reloadData()
    }
    
    private func setTableViewBackground() {
        if routines.count > 0 {
            
            // If there are routines, the add button on the empty state will be hidden, so the addBagButton needs to be visible.
            addRoutineButton.isHidden = false
            tableView.backgroundView = UIView()
            
            // Make sure the changes are shown.
            routinesTableViewHandler.collectionObjects = routines
            tableView.reloadData()
        } else {
            
            addRoutineButton.isHidden = true
            showEmptyView()
        }
    }
    
    private func setUpTableView(isTrainer: Bool) {
        // Sets the delegate and data source for the table view, allowing this ViewController to control the tableview.
        var nib: UINib = UINib()
        
        if isTrainer {
            
            nib = UINib(nibName: NibName.shareableCollectionCell.stringValue, bundle: nil)
        } else {
            
            nib = UINib(nibName: NibName.collectionCell.stringValue, bundle: nil)
        }
        
        tableView.register(nib, forCellReuseIdentifier: Identifier.collectionCell.stringValue)
        
        routinesTableViewHandler = CollectionObjectsTableViewHandler(delegate: self, collectionObjects: routines, singleItemNames: exerciseNames, cellImages: [:], isTrainer: isTrainer)
        
        // Essentially make it look like there is no longer a tableview visible so as to not distract from the empty view.
        tableView.backgroundView = UIView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        
        // Allows this ViewController to manipulate of the tableview using the appropriate data.
        (tableView.dataSource, tableView.delegate) = (routinesTableViewHandler, routinesTableViewHandler)
    }
    
    private func setUpTraineesTableView() {
        let alertController = UIAlertController(title: "Share", message: "Select a trainee to share this with.", preferredStyle: UIAlertController.Style.actionSheet)
        let margin:CGFloat = view.frame.height * 0.1
        let rect = CGRect(x: 0, y: margin, width: alertController.view.bounds.size.width * 0.95, height: view.frame.height * 0.2)
        
        traineesTableView = UITableView(frame: rect, style: .plain)
        traineesTableView.backgroundView = UIView()
        traineesTableView.backgroundColor = .clear
    }
    
    //MARK: - HealthKit methods
    private func authorizeHealthKit() {
        //Authorize HK
        healthKitSetupAssistant.authorizeHealthKit { (success, error) in
            guard success else {
                //If the authorization fails, print the error
                let defaultMessage = "HealthKit Authorization Failed"
                
                if let error = error {
                    print("\(defaultMessage).\n"
                        + "ERROR:\n"
                        + "\(error.localizedDescription)")
                } else {
                    print(defaultMessage)
                }
                
                return
            }
        }
    }
    
}

//MARK: - Extensions
extension WorkoutsViewController {
    
    //MARK: - Actions
    @IBAction func addRoutineTapped(_ sender: UIButton) {
        showAddOptions(sender)
    }
    
    @IBAction func userAccountTapped(_ sender: UIButton) {
        accountTapped(sender)
    }
    
}

extension WorkoutsViewController: EmptyListDelegate {
    
    // When add is tapped, show a bottom action sheet that allows the user to either create or browse routines.
    func showAddOptions(_ sender: UIButton) {
        if let paid = paid, !paid && routines.count > 2 {
            showLimitAlert()
            return
        } else if !FirebaseAuthValidationUtil.checkIfSignedIn() {
            
            let alert: UIAlertController = AlertHelper.getSignInToAddListsAlert(sender: sender, delegate: self)
            present(alert, animated: true)
        }
        
        let alertController: UIAlertController = AlertHelper.getAddOptionsActionSheet(listType: .routine, sender: sender, delegate: self)
        
        self.present(alertController, animated: true)
    }
    
}

extension WorkoutsViewController: AddOptionsActionSheetDelegate {
    
    func browseSelected(sender: UIButton) {
        performSegue(withIdentifier: Segue.toBrowseRoutines.stringValue, sender: sender)
    }
    
    func createSelected(sender: UIButton) {
        if FirebaseAuthValidationUtil.checkIfSignedIn() {
            performSegue(withIdentifier: Segue.toCreateRoutine.stringValue, sender: sender)
        } else {
            let alert: UIAlertController = AlertHelper.getSignInToAddListsAlert(sender: sender, delegate: self)
            present(alert, animated: true)
        }
    }
}

extension WorkoutsViewController: HeaderBarDelegate {
    
    // Make sure the view conforms to the delegate.
    // This simply ensures that the screen is always able to open account.
    func accountTapped(_ sender: UIButton) {
        // Open the account storyboard
        let storyboard = UIStoryboard.init(name: "Account", bundle: Bundle.main)
        let viewcontroller = storyboard.instantiateViewController(withIdentifier: Identifier.accountViewController.stringValue) as? AccountViewController
        
        present(viewcontroller!, animated: true)
    }
}

extension WorkoutsViewController: FirebaseAccountDelegate {
    func userPulled(user: User, current: Bool) {
        if current {
            paid = user.paid
            traineeIDs = user.trainees
            setUpTableView(isTrainer: user.isTrainer)
            setTableViewBackground()
            pullTrainees()
        } else {
            
            trainees += [user]
            
            traineesTableViewHandler.trainees = trainees
            traineesTableView.reloadData()
        }
    }
    
    func signInSelected(_ sender: UIAlertAction) {
        accountTapped(UIButton(type: .system))
    }
    
    func upgradeTapped(sender: UIAlertAction) {
        accountTapped(UIButton(type: .system))
    }
    
}

extension WorkoutsViewController: TableViewCellObjectDelegate {
    
    func objectSelected(object: Any, objectType: BrowseObjectType) { }
    
    func objectSelected(indexPath: IndexPath) {
        // Segue to the CreateRoutineViewController using the indexPath, which will trigger the new view controller to become populated with the information about the selected routine.
        performSegue(withIdentifier: Segue.toCreateRoutine.stringValue, sender: indexPath)
    }
    
    func shareTapped(_ sender: UIButton, indexPath: IndexPath) {
        routineToShare = routines[indexPath.row]
        
        if let traineeIDs = traineeIDs, let handler = traineesTableViewHandler {
            let alert: UIAlertController = AlertHelper.getTraineeListActionSheet(screenHeight: self.view.frame.height, selectMultiple: false, traineeIDs: traineeIDs, alertTableView: traineesTableView, tableViewHandler: handler, actionSheetDelegate: self, sender: sender)
            
            present(alert, animated: true)
        }
    }
    
    func cellDeleted(indexPath: IndexPath) {
        // Delete the routine from the "routines" root in Firebase.
        deleteFromFirebase(id: routineIDs[indexPath.row])
        
        // Remove the routine and routine IDs from the stored array.
        routines.remove(at: indexPath.row)
        routineIDs.remove(at: indexPath.row)
        
        // Delete the routine from the Firebase user in the "users" root.
        deleteFromFirebase()
        
        setTableViewBackground()
    }
    
}

extension WorkoutsViewController: UITableViewActionSheetDelegate {
    
    func addToList(selectedIndexPaths: [IndexPath]) {
        if let routine = routineToShare, let traineeIDs = traineeIDs {
            
            FirebaseWorkoutsUitl.pushNewRoutineToUser(userID: traineeIDs[selectedIndexPaths[0].row], routine: routine, delegate: self)
        }
    }
    
}

extension WorkoutsViewController: ShareObjectDelegate {
    
    func sent(_ sent: Bool, object: Any, to user: User?) {
        if let routine = object as? Routine, let user = user, sent {
            
            print("\(sent ? "Sent" : "Did not send") \(routine.name) to \(user.firstName) \(user.lastName)")
            
            let alert: UIViewController = AlertHelper.getSentAlert(sent: true, traineeFirstName: user.firstName)
            
            present(alert, animated: true, completion: nil)
        }
    }
}

extension WorkoutsViewController: PullIconsDelegate {
    
    func iconLocationsPulled(iconLocations: [String]) {
        // Not selecting icons, so nothing else needs to happen here.
    }
    
    func iconImagesPulled(_ iconImage: (key: String, image: UIImage)) {
        
        if !iconImages.keys.contains(iconImage.key) {
            
            iconImages[iconImage.key] = iconImage.image
        }
        
        routinesTableViewHandler.cellImages = iconImages
        
        setTableViewBackground()
    }
    
    func singleIconImagePulled(iconImage: UIImage) {
        // Not displaying a single image on its own for this screen, so nothing else needs to happen here.
    }
}
