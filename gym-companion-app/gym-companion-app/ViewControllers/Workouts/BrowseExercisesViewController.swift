//
//  BrowseExercisesViewController.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/16/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

class BrowseExercisesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Variables
    var tableViewHandler: BrowseObjectsTableViewHandler!
    
    var selectedExercise: Exercise?
    
    var userExercises: [Exercise] = []
    var browsableExercises: [Exercise] = []
    
    var userExercisesPulled: Bool = false
    var browsableExercisesPulled: Bool = false
    
    var savedToFirebase: Bool = false
    var savedLocally: Bool = true // TODO: Change this to false when local saving is workting
    
    //MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpTableView()
        FirebaseWorkoutsUitl.pullUserExerciseIDs(delegate: self)
        FirebaseWorkoutsUitl.pullBrowsableExercises(delegate: self)
    }
    
    //MARK: - Custom Methods
    private func checkIfShouldDismiss() {
        
        if savedToFirebase && savedLocally {
            
            performSegue(withIdentifier: Segue.toPassBackBrowsableExercise.stringValue, sender: nil)
        }
    }
    
    private func checkIfShouldLoadTableView() {
        
        if userExercisesPulled && browsableExercisesPulled {
            
            if tableViewHandler.expandableCells.count < 2 {
                
                if userExercises.count > 0 {
                    tableViewHandler.expandableCells = [
                        ExpandableCellObject(title: "My Exercises", icon: nil, objects: userExercises, isExpanded: true),
                        ExpandableCellObject(title: "Fit+ Exercises", icon: nil, objects: browsableExercises, isExpanded: true)
                    ]
                } else {
                    
                    tableViewHandler.expandableCells = [
                        ExpandableCellObject(title: "Fit+ Exercises", icon: nil, objects: browsableExercises, isExpanded: true)
                    ]
                }
            }
            
            tableView.reloadData()
        }
    }
    
    private func setUpTableView() {
        let nib: UINib = UINib(nibName: NibName.collectionCell.stringValue, bundle: nil)
        
        tableView.backgroundColor = .clear
        tableView.backgroundView = UIView()
        tableView.separatorStyle = .none
        
        tableView.register(nib, forCellReuseIdentifier: Identifier.collectionCell.stringValue)
        tableViewHandler = BrowseObjectsTableViewHandler(expandableCells: [], singleObjectNames: [:], cellImages: [:], delegate: self)
        (tableView.delegate, tableView.dataSource) = (tableViewHandler, tableViewHandler)
    }
    
}

//MARK: - Extensions
extension BrowseExercisesViewController {
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
    }
    
}

extension BrowseExercisesViewController: PullExercisesDelegate {
    
    func exerciseIDsPulled(exerciseIDs: [String]) {
        
        FirebaseWorkoutsUitl.pullUserExercises(exerciseIDs: exerciseIDs, delegate: self)
    }
    
    func exerciseObjectsPulled(exercises: [Exercise], browseObjectType: BrowseObjectType, totalToPull: Int) {
        
        //TODO: Break up this method.
        
        switch browseObjectType {
        case .browsable:
            
            browsableExercises += exercises
            
            if browsableExercises.count == totalToPull {
                
                browsableExercisesPulled = true
            }
        case .user:
            
            userExercises += exercises
            
            if userExercises.count == totalToPull {
                
                userExercisesPulled = true
            }
        }
        
        checkIfShouldLoadTableView()
    }
    
}

extension BrowseExercisesViewController: SaveExerciseDelegate {
    
    func savedExerciseToExerciseRoot(exercise: Exercise, _ saved: Bool) {
        
        if let selectedExercise = selectedExercise {
            
            selectedExercise.exerciseID = exercise.exerciseID
            FirebaseWorkoutsUitl.saveNewExerciseToUserRoot(delegate: self, exercise: selectedExercise)
        }
    }
    
    func savedExerciseToUserRoot(_ saved: Bool) {
        
        savedToFirebase = saved
        checkIfShouldDismiss()
    }
    
    func updatedExercise(_ updated: Bool) {
        
    }
}

extension BrowseExercisesViewController: TableViewCellObjectDelegate {
    
    func objectSelected(indexPath: IndexPath) { }
    
    func shareTapped(_ sender: UIButton, indexPath: IndexPath) { }
    
    func cellDeleted(indexPath: IndexPath) { }
    
    func objectSelected(object: Any, objectType: BrowseObjectType) {
        if let selectedExercise = object as? Exercise {
            
            self.selectedExercise = selectedExercise
            
            switch objectType {
            case .browsable:
                
                FirebaseWorkoutsUitl
                    .saveNewBrowsableExerciseToExerciseRoot(delegate: self, exerciseID: selectedExercise.exerciseID)
            case .user:
                
                FirebaseWorkoutsUitl.saveNewUserExerciseToExerciseRoot(delegate: self, exerciseID: selectedExercise.exerciseID)
            }
        }
    }
    
}


