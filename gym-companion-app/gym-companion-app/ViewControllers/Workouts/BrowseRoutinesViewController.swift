//
//  BrowseRoutinesViewController.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/16/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseStorage

class BrowseRoutinesViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Variables
    var storageReference: StorageReference!
    var tableViewHandler: BrowseObjectsTableViewHandler!
    
    var selectedRoutine: Routine?
    var newExerciseIDs: [String] = []
    
    var userRoutines: [Routine] = []
    var userExercises: [Exercise] = []
    var browsableRoutines: [Routine] = []
    
    var iconImages: [String:UIImage] = [:]
    
    var userRoutinesPulled: Bool = false
    var browsableRoutinesPulled: Bool = false
    
    var savedToFirebase: Bool = false
    var savedLocally: Bool = true // TODO: Change this to false when local saving is workting
    
    var exerciseNames: [String:String] {
        var names: [String:String] = [:]
        
        for exercise in userExercises {
            names[exercise.exerciseID] = exercise.name
        }
        
        return names
    }

    //MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        storageReference = Storage.storage().reference(withPath: "icons/").child("routineIcons/")
        setUpTableView()
        FirebaseWorkoutsUitl.pullBrowsableRoutines(delegate: self)
        FirebaseWorkoutsUitl.pullUserRoutinesIDs(delegate: self)
    }
    
    //MARK: - Custom Methods
    private func checkIfShouldDismiss() {
        if savedToFirebase && savedLocally {
            
            dismiss(animated: true, completion: nil)
        }
    }
    
    private func checkIfShouldLoadTableView() {
        if userRoutinesPulled && browsableRoutinesPulled {
            tableViewHandler.cellImages = iconImages
            
            if tableViewHandler.expandableCells.count < 2 {
                if userRoutines.count > 0 {
                    tableViewHandler.expandableCells = [
                        ExpandableCellObject(title: "My Routines", icon: nil, objects: userRoutines, isExpanded: true),
                        ExpandableCellObject(title: "Fit+ Routines", icon: nil, objects: browsableRoutines, isExpanded: true)
                    ]
                } else {
                    
                    tableViewHandler.expandableCells = [
                        ExpandableCellObject(title: "Fit+ Routines", icon: nil, objects: browsableRoutines, isExpanded: true)
                    ]
                }
            }
            
            tableView.reloadData()
        }
    }
    
    private func setUpTableView() {
        let nib: UINib = UINib(nibName: NibName.collectionCell.stringValue, bundle: nil)
        
        tableView.backgroundColor = .clear
        tableView.backgroundView = UIView()
        tableView.separatorStyle = .none
        
        tableView.register(nib, forCellReuseIdentifier: Identifier.collectionCell.stringValue)
        tableViewHandler = BrowseObjectsTableViewHandler(expandableCells: [], singleObjectNames: exerciseNames, cellImages: iconImages, delegate: self)
        (tableView.delegate, tableView.dataSource) = (tableViewHandler, tableViewHandler)
    }
    
}

//MARK: - Extensions
extension BrowseRoutinesViewController {
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
    }
    
}

extension BrowseRoutinesViewController: PullRoutinesDelegate {
    
    // Triggered when the routineIDs for the current user's routines have been pulled.
    func routineIDsPulled(routineIDs: [String]) {
        
        if routineIDs.count > 0 {
            FirebaseWorkoutsUitl.pullUserRoutines(routineIDs: routineIDs, delegate: self)
        } else {
            userRoutinesPulled = true
            checkIfShouldLoadTableView()
        }
    }
    
    // Triggered by one of the following:
    //  -- The current user's routines have been pulled by using the routineIDs saved within the current user object.
    //  -- The ready-to-go browsable routines have been pulled.
    func routineObjectsPulled(routines: [Routine], browseObjectType: BrowseObjectType, totalToPull: Int) {
        
        //TODO: Break up this method.
        
        var iconLocations: [String] = []
        
        for routine in routines {
            if let iconID = routine.routineIconID {
                iconLocations += [iconID]
            }
        }
        
        switch browseObjectType {
        case .browsable:
            browsableRoutines = routines
            
            for routine in routines {
                FirebaseWorkoutsUitl.pullBrowsableExercises(exerciseIDs: routine.exercises, delegate: self)
            }
            
            browsableRoutinesPulled = true
        case .user:
            userRoutines += routines
            
            if userRoutines.count == totalToPull {
                userRoutinesPulled = true
            }
            
            for routine in routines {
                FirebaseWorkoutsUitl.pullUserExercises(exerciseIDs: routine.exercises, delegate: self)
            }
        }
        
        checkIfShouldLoadTableView()
        
        PullIconsHelper.pullIconImages(iconLocations: iconLocations, storageReference: storageReference, delegate: self)
    }
    
}

extension BrowseRoutinesViewController: PullExercisesDelegate {
    
    func exerciseIDsPulled(exerciseIDs: [String]) {
        // Getting exercises IDs directly from routines, so nothing else needs to happen here.
    }
    
    func exerciseObjectsPulled(exercises: [Exercise], browseObjectType: BrowseObjectType, totalToPull: Int) {
        
        userExercises += exercises
        tableViewHandler.singleObjectNames = exerciseNames
        tableView.reloadData()
    }
    
}

extension BrowseRoutinesViewController: PullIconsDelegate {
    
    func iconLocationsPulled(iconLocations: [String]) {
        // Not selecting icons, so nothing else needs to happen here.
    }
    
    // Triggered by one of the following:
    //  -- The icons for the current user's routines have been pulled.
    //  -- The icons for the ready-to-go browsable routines have been pulled.
    func iconImagesPulled(_ iconImage: (key: String, image: UIImage)) {
        if !iconImages.keys.contains(iconImage.key) {
            
            iconImages[iconImage.key] = iconImage.image
        }
        
        checkIfShouldLoadTableView()
    }
    
    func singleIconImagePulled(iconImage: UIImage) {
        // Not displaying a single image on its own for this screen, so nothing else needs to happen here.
    }
    
}

extension BrowseRoutinesViewController: SaveExerciseDelegate {
    
    func savedExerciseToExerciseRoot(exercise: Exercise, _ saved: Bool) {
        newExerciseIDs += [exercise.exerciseID]
        
        if let selectedRoutine = selectedRoutine, newExerciseIDs.count == selectedRoutine.exercises.count {
            selectedRoutine.exercises = newExerciseIDs
            FirebaseWorkoutsUitl.saveNewRoutineToRoutineRoot(routine: selectedRoutine, delegate: self)
        }
    }
    
    func savedExerciseToUserRoot(_ saved: Bool) {
        //Will be saving all exercises to user when the routine is saved, so nothing else needs to happen here.
    }
    
    func updatedExercise(_ updated: Bool) { }
}

extension BrowseRoutinesViewController: SaveRoutineDelegate {
    
    func savedRoutineToUserRoot(_ saved: Bool) {
        savedToFirebase = true
        checkIfShouldDismiss()
    }
    
    func savedRoutineToRoutinesRoot(_ saved: Bool) {
        if let routine = selectedRoutine, saved {
            FirebaseWorkoutsUitl.saveNewRoutineToUserRoot(routine: routine, delegate: self)
        }
    }
    
    func routineUpdated(_ saved: Bool) {
        // Not updating routines here, only browsing to save new routines, so nothing else needs to happen here.
    }
    
    func savedRoutineLocally(_ saved: Bool) {
        //TODO: Save locally.
    }
    
}

extension BrowseRoutinesViewController: TableViewCellObjectDelegate {
    
    func objectSelected(indexPath: IndexPath) { }
    
    func shareTapped(_ sender: UIButton, indexPath: IndexPath) { }
    
    func cellDeleted(indexPath: IndexPath) { }
    
    func objectSelected(object: Any, objectType: BrowseObjectType) {
        if let selectedRoutine = object as? Routine {
            self.selectedRoutine = selectedRoutine
            
            switch objectType {
            case .browsable:
                for id in selectedRoutine.exercises {
                    
                    FirebaseWorkoutsUitl
                        .saveNewBrowsableExerciseToExerciseRoot(delegate: self, exerciseID: id)
                }
            case .user:
                for id in selectedRoutine.exercises {
                    
                    FirebaseWorkoutsUitl.saveNewUserExerciseToExerciseRoot(delegate: self, exerciseID: id)
                }
            }
        }
    }
    
}
