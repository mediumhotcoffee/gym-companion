//
//  WeightCalculatorViewController.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/22/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

class WeightCalculatorViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var modalBackground: UIView!
    @IBOutlet weak var backgroundBlurView: UIView!
    
    //MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        modalBackground.layer.cornerRadius = 15
        modalBackground.layer.masksToBounds = true
        modalBackground.backgroundColor = .appGreen
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.alpha = 0.25 
        backgroundBlurView.addSubview(blurEffectView)
    }
    
    //MARK: - Actions
    @IBAction func buttonTap(_ sender: Any) {
        dismiss(animated: true)
    }
    
}
