//
//  StatsViewController.swift
//  gym-companion-app
//
//  Created by Marcos Felipe Costa da Silva on 6/28/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseStorage
import HealthKit

class StatsViewController: UIViewController {

    //MARK: - Views
    //Buttons
    @IBOutlet weak var accountButton: UIButton!
    @IBOutlet weak var cardioButton: UIButton!
    @IBOutlet weak var cardioLabel: UILabel!
    
    //MARK: - HealthKit variables
    let healthKitSetupAssistant: HealthKitSetupAssistant = HealthKitSetupAssistant.sharedInstance
    var healthStore: HKHealthStore!
    
    //MARK: - Other variables
    var docsURL: URL?
    var downloadedImageURL: String?
    var userPicturesPath: String = ""
    
    //MARK: - Lifecycle methods
    override func viewWillAppear(_ animated: Bool) {
        //Hide Navigation Controller
        //The only purpose of the Navigation Controller is to maintain the
        //tab bar through all segues
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        //Setup
        ImageUtil.loadUserPictureOn(button: accountButton, userId: Auth.auth().currentUser?.uid ?? "")
        
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            // Ipad
            cardioLabel.isHidden = true
            cardioButton.isHidden = true
        }
    }
    
    override func viewDidLoad() {
        //Do any additional setup after loading the view
        super.viewDidLoad()
        
        //Check if HealthKit was allowed
        authorizeHealthKit()
    }
    
    //MARK: - Actions
    @IBAction func userAccountTapped(_ sender: UIButton) {
        accountTapped(sender)
    }
    
    //MARK: - HealthKit methods
    private func authorizeHealthKit() {
        //Authorize HK
        healthKitSetupAssistant.authorizeHealthKit { (success, error) in
            guard success else {
                //If the authorization fails, print the error
                let defaultMessage = NSLocalizedString("healthkit-authorization-failed", comment: "")
                
                if let error = error {
                    print(
                        "\(defaultMessage).\n"
                            + "ERROR:\n"
                            + "\(error.localizedDescription)")
                } else {
                    print(defaultMessage)
                }
                
                return
            }
        }
    }
    
}

//MARK: - Extensions
extension StatsViewController: HeaderBarDelegate {
    
    // This simply ensures that the screen is always able to open account
    func accountTapped(_ sender: UIButton) {
        // Open the account storyboard
        let storyboard = UIStoryboard.init(name: "Account", bundle: Bundle.main)
        let viewcontroller = storyboard.instantiateViewController(withIdentifier: Identifier.accountViewController.stringValue) as? AccountViewController
        
        present(viewcontroller!, animated: true)
    }
    
}
