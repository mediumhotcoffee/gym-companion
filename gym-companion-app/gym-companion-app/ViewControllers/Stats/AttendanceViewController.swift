//
//  AttendanceViewController.swift
//  gym-companion-app
//
//  Created by Marcos Felipe Costa da Silva on 7/25/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class AttendanceViewController: UIViewController {

    //MARK: - Variables
    private var currentMonthName: Month = .january
    private var currentMonth: Int = 0
    private var attendance: Int = 0
    private let userDefaults = UserDefaults.standard
    //Firebase variables
    var handle: AuthStateDidChangeListenerHandle?
    var userId: String = ""
    
    //MARK: - Views
    //Labels
    @IBOutlet weak var attendanceCountLabel: UILabel!
    @IBOutlet weak var attendaceMonthLabel: UILabel!
    
    //MARK: - Lifecycle Methods
    override func viewWillAppear(_ animated: Bool) {
        //Hide Navigation Controller
        
        //Add log in status listener
        handle = Auth.auth().addStateDidChangeListener({ (auth, user) in
            guard let user = user else {
                return
            }
            
            //Get Firebase Storage reference
            self.userId = user.uid
            
            //Load attendance from memory
            let currentAttendance = self.userDefaults.integer(forKey: self.currentMonthName.rawValue + self.userId)
            self.attendanceCountLabel.text = String(currentAttendance)
            self.attendance = currentAttendance
        })
    }
    
    override func viewDidLoad() {
        //Do any additional setup after loading the view
        super.viewDidLoad()
        
        //Define text
        attendaceMonthLabel.text = Month.january.rawValue
    }
    
    //MARK: - Action
    @IBAction func doneTapped(_ sender: UIButton) {
        //Go back to previous screen
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resetTapped(_ sender: UIButton) {
        //Reset attendance
        let alert = UIAlertController(title: NSLocalizedString("reset-attendance", comment: ""),
                                      message: NSLocalizedString("do-you-want-to-set-your-attendance", comment: ""),
                                      preferredStyle: .alert)
        
        //Reset action
        alert.addAction(UIAlertAction(title: NSLocalizedString("reset", comment: ""),
                                      style: .destructive,
                                      handler: { (action) in
            //Save to user defaults
            self.userDefaults.set(0, forKey: self.currentMonthName.rawValue + self.userId)
            
            //Load attendance from memory
            let currentAttendance = self.userDefaults.integer(forKey: self.currentMonthName.rawValue + self.userId)
            self.attendanceCountLabel.text = String(currentAttendance)
            self.attendance = currentAttendance
            
            //Give haptic feedback to the user
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.success)
        }))
        
        //Cancel action
        alert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""),
                                      style: .cancel))
        
        //Show alert
        present(alert, animated: true)
    }
    
    @IBAction func updateAttendaceTapped(_ sender: UIButton) {
        //Raise or lower attendance count
        switch sender.tag {
        case 0:
            //Reduce attendance
            if attendance > 0 {
                attendance -= 1
            }
        case 1:
            //Limit attendance days to days available on that month
            switch currentMonthName {
            case .january, .march, .may, .july, .august, .october, .december:
                if attendance < 31 {
                    attendance += 1
                }
            case .april, .june, .september, .november:
                if attendance < 30 {
                    attendance += 1
                }
            case .february:
                if attendance < 29 {
                    attendance += 1
                }
            }
        default:
            //Do nothing
            break
        }
        
        //Update Attendance label
        attendanceCountLabel.text = String(attendance)
        
        //Save to user defaults
        userDefaults.set(attendance, forKey: currentMonthName.rawValue + userId)
        
        //Give haptic feedback to the user
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
    }
    
    @IBAction func updateMonthTapped(_ sender: UIButton) {
        //Select month
        switch sender.tag {
        case 0:
            if currentMonth == 0 {
                currentMonth = 11
            } else if currentMonth > 0 {
                currentMonth -= 1
            }
        case 1:
            if currentMonth == 11 {
                currentMonth = 0
            } else if currentMonth < 11 {
                currentMonth += 1
            }
        default:
            break
        }
        
        //Update Month label
        updateToNextMonthLabel(currentMonth)
    }
    
    //MARK: - Custom Methods
    func updateToNextMonthLabel(_ monthNum: Int) {
        switch monthNum {
        case 0:
            currentMonthName = .january
            
            //Update label with month name
            attendaceMonthLabel.text = NSLocalizedString("january", comment: "")
        case 1:
            currentMonthName = .february
            
            //Update label with month name
            attendaceMonthLabel.text = NSLocalizedString("february", comment: "")
        case 2:
            currentMonthName = .march
            
            //Update label with month name
            attendaceMonthLabel.text = NSLocalizedString("march", comment: "")
        case 3:
            currentMonthName = .april
            
            //Update label with month name
            attendaceMonthLabel.text = NSLocalizedString("april", comment: "")
        case 4:
            currentMonthName = .may
            
            //Update label with month name
            attendaceMonthLabel.text = NSLocalizedString("may", comment: "")
        case 5:
            currentMonthName = .june
            
            //Update label with month name
            attendaceMonthLabel.text = NSLocalizedString("june", comment: "")
        case 6:
            currentMonthName = .july
            
            //Update label with month name
            attendaceMonthLabel.text = NSLocalizedString("july", comment: "")
        case 7:
            currentMonthName = .august
            
            //Update label with month name
            attendaceMonthLabel.text = NSLocalizedString("august", comment: "")
        case 8:
            currentMonthName = .september
            
            //Update label with month name
            attendaceMonthLabel.text = NSLocalizedString("september", comment: "")
        case 9:
            currentMonthName = .october
            
            //Update label with month name
            attendaceMonthLabel.text = NSLocalizedString("october", comment: "")
        case 10:
            currentMonthName = .november
            
            //Update label with month name
            attendaceMonthLabel.text = NSLocalizedString("november", comment: "")
        case 11:
            currentMonthName = .december
            
            //Update label with month name
            attendaceMonthLabel.text = NSLocalizedString("december", comment: "")
        default:
            break
        }
        
        //Load attendance from memory
        let currentAttendance = userDefaults.integer(forKey: currentMonthName.rawValue + userId)
        attendanceCountLabel.text = String(currentAttendance)
        attendance = currentAttendance
    }
}

//MARK: - Enums
enum Month: String {
    case january = "January"
    case february = "February"
    case march = "March"
    case april = "April"
    case may = "May"
    case june = "June"
    case july = "July"
    case august = "August"
    case september = "September"
    case october = "October"
    case november = "November"
    case december = "December"
}
