//
//  WorkoutsDataViewController.swift
//  gym-companion-app
//
//  Created by Marcos Silva on 7/21/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import HealthKit

class WorkoutsDataViewController: UIViewController {

    //MARK: - HealthKit Variables
    var previousWorkouts = [HKWorkout]()
    var workoutSelected: HKWorkout?
    var healthStore: HKHealthStore!
    let healthKitSetupAssistant: HealthKitSetupAssistant = HealthKitSetupAssistant.sharedInstance
    
    //MARK: - Views
    @IBOutlet weak var workoutsTableView: UITableView!
    
    //MARK: - Lifecycle Methods
    override func viewDidLoad() {
        //Do any additional setup after loading the view
        super.viewDidLoad()
        
        //Setup the TableView
        setupTableView()
        
        //Request autohrization on the iPhone
        authorizeHealthKit()
        
        //Try to get the user's age
        getAge()
        
        //Get previous workouts stored on the device
        getWorkoutsForTableView()
    }
    
    //MARK: - Actions
    @IBAction func backTapped(_ sender: UIButton) {
        //Go back to the previous screen
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        if let destination = segue.destination as? WorkoutDetailsViewController, let workoutToSend = workoutSelected {
            //Pass the selected object to the new view controller
            destination.workoutToDisplay = workoutToSend
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        //Prevent segue if HKWorkout object to pass is nil
        if identifier == Segue.toWorkoutDetailsSegue.stringValue {
            if workoutSelected == nil {
                return false
            }
        }
        
        return true
    }
    
    //MARK: - HealthKit methods
    private func authorizeHealthKit() {
        //Authorize HK
        healthKitSetupAssistant.authorizeHealthKit { (success, error) in
            guard success else {
                //If authorization fails, print error
                if let error = error {
                    print("HealthKit Authorization Failed\n"
                        + "ERROR:\n"
                        + "\(error.localizedDescription)")
                }
                
                return
            }
        }
    }
    
    func getAge() {
        //Retrieve user's age from HealthKit
        do {
            let userAge = try ProfileDataStore.getAge()
            
            if userAge == nil {
                let alertController = UIAlertController(title: NSLocalizedString("age-missing", comment: ""),
                                                        message: NSLocalizedString("please-define-your-age", comment: ""),
                                                        preferredStyle: .actionSheet)
                
                //OK action
                alertController.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""),
                                                        style: .default))
                
                //Show alert
                self.present(alertController, animated: true, completion: nil)
            }
        } catch {
            print("WorkoutsDataViewController.getAge()\n"
                + "ERROR:\n"
                + error.localizedDescription)
        }
    }
    
    func getWorkoutsForTableView() {
        //Retrieve workouts
        ProfileDataStore.loadWorkouts { (workouts, error) in
            if let workouts = workouts {
                self.previousWorkouts = workouts
                self.workoutsTableView.reloadData()
            }
            
            if let error = error {
                print("WorkoutsDataViewController.getWorkoutsForTableView()\n"
                    + "ERROR:\n"
                    + error.localizedDescription)
            }
        }
    }
    
    //MARK: - Custom Methods
    func setupTableView() {
        //Setup the table view
        
        //Setup table view data source and delegate
        (workoutsTableView.delegate, workoutsTableView.dataSource) = (self, self)

        //Make table view transparent
        workoutsTableView.backgroundColor = .clear
        
        //Setup separators
        workoutsTableView.separatorColor = .white
    }
    
}

//MARK: - Extensions
extension WorkoutsDataViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return previousWorkouts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "workoutCell")
        
        //Retrieve date and time from the workout
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        let date = dateFormatter.string(from: previousWorkouts[indexPath.row].startDate)
        cell.textLabel?.text = date
        
        //Setup detail text
        let minutes = Int(previousWorkouts[indexPath.row].duration / 60)
        let seconds = Int(previousWorkouts[indexPath.row].duration) % 60
        if seconds < 10 {
            cell.detailTextLabel?.text = "Time: \(minutes):0\(seconds)"
        } else {
            cell.detailTextLabel?.text = "Time: \(minutes):\(seconds)"
        }
        
        //Setup cell appearance
        //Color
        cell.backgroundColor = .clear
        //Text
        cell.textLabel?.textColor = UIColor.appDarkColor
        cell.textLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        //Detail text
        cell.detailTextLabel?.textColor = .white
        
        return cell
    }
    
}

extension WorkoutsDataViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Get the workout that was selected
        workoutSelected = previousWorkouts[indexPath.row]
     
        //Unselect cell
        tableView.deselectRow(at: indexPath, animated: true)
        
        //Go to workout details screen
        performSegue(withIdentifier: Segue.toWorkoutDetailsSegue.stringValue, sender: nil)
    }
    
}
