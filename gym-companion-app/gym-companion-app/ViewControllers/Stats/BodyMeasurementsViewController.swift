//
//  BodyMeasurementsViewController.swift
//  gym-companion-app
//
//  Created by Marcos Silva on 7/21/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import HealthKit

class BodyMeasurementsViewController: UIViewController {

    //MARK: - HealthKit variables
    let healthKitSetupAssistant: HealthKitSetupAssistant = HealthKitSetupAssistant.sharedInstance
    var measurementType: HKQuantityTypeIdentifier = HKQuantityTypeIdentifier.bodyMass
    
    //MARK: - Views
    //Segmented Controls
    @IBOutlet weak var measurementTypeSegmentedControl: UISegmentedControl!
    //Labels
    @IBOutlet weak var measurementLabel: UILabel!
    @IBOutlet weak var measurementTypeLabel: UILabel!
    @IBOutlet weak var instructionsLabel: UILabel!
    //TextField
    @IBOutlet weak var newMeasurementTextField: UITextField!
    
    //MARK: - Lifecycle Methods
    override func viewDidLoad() {
        //Do any additional setup after loading the view
        super.viewDidLoad()

        //Setup delegate for the TextField
        newMeasurementTextField.delegate = self
        
        //Load weight
        getMostRecentWeight()
        
        //Prepare for possible update
        measurementType = HKQuantityTypeIdentifier.bodyMass
    }
    
    //MARK: - Actions
    @IBAction func backTapped(_ sender: UIButton) {
        //Go back to the previous screen
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        //Handle changes to the segmented control
        switch sender.selectedSegmentIndex {
        case 0:
            //Update labels
            measurementTypeLabel.text = NSLocalizedString("weight", comment: "")
            instructionsLabel.text = NSLocalizedString("enter-your-new-weight", comment: "")
            
            //Prepare for possible update
            measurementType = HKQuantityTypeIdentifier.bodyMass
            
            //Load most recent weight
            getMostRecentWeight()
        case 1:
            //Update labels
            measurementTypeLabel.text = NSLocalizedString("bmi", comment: "")
            instructionsLabel.text = NSLocalizedString("enter-your-new-BMI", comment: "")
            
            //Prepare for possible update
            measurementType = HKQuantityTypeIdentifier.bodyMassIndex
            
            //Load BMI
            getMostRecentBmi()
        case 2:
            //Update labels
            measurementTypeLabel.text = NSLocalizedString("body-fat", comment: "")
            instructionsLabel.text = NSLocalizedString("enter-your-new-body", comment: "")
            
            //Prepare for possible update
            measurementType = HKQuantityTypeIdentifier.bodyFatPercentage
            
            //Load Body Fat
            getMostRecentBodyFat()
        default:
            break
        }
    }
    
    @IBAction func updateTapped(_ sender: UIButton) {
        //Save new measurement when Update is tapped
        guard let valueToSaveAsText = newMeasurementTextField.text else { return }
        
        guard let valueToSave = Double(valueToSaveAsText) else {
            //Try to convert text to Double
            showErrorWhileSavingToHealthKitMessage()
            return
        }
        
        switch measurementType {
        case HKQuantityTypeIdentifier.bodyMass:
            saveNewWeightMeasurement(weightToSave: valueToSave, forDate: Date())
        case HKQuantityTypeIdentifier.bodyMassIndex:
            saveNewBmiMeasurement(bmiToSave: valueToSave, forDate: Date())
        case HKQuantityTypeIdentifier.bodyFatPercentage:
            saveNewBodyFatMeasurement(bodyFatToSave: valueToSave, forDate: Date())
        default:
            break
        }
        
        //Give haptic feedback to the user
        //NOT THREAD SAFE
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
    }
    
    //MARK: - Screen Management
    override var shouldAutorotate: Bool {
        return false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //Dismiss keyboard when touching the rest of the screen
        newMeasurementTextField.resignFirstResponder()
    }
    
    //MARK: - Custom Methods
    func getMostRecentWeight() {
        //Build the query
        let query = HKStatisticsQuery.init(quantityType: HKObjectType.quantityType(forIdentifier: .bodyMass)!,
                                           quantitySamplePredicate: nil,
                                           options: .discreteMostRecent) { (query, results, error) in
            //Define lb as unit
            let weightUnit = HKUnit(from: "lb")
            
            DispatchQueue.main.async {
                //Update UI with result
                if let mostRecentWeight = results?.mostRecentQuantity() {
                    guard mostRecentWeight.doubleValue(for: weightUnit) > 0 else {
                        //Handle weight not available
                        self.measurementLabel.font = UIFont.systemFont(ofSize: 25, weight: .semibold)
                        self.measurementLabel.text = NSLocalizedString("not-available", comment: "")
                        return
                    }
                    
                    self.measurementLabel.font = UIFont.systemFont(ofSize: 85, weight: .semibold)
                    self.measurementLabel.text = String(format: "%0.1flbs", mostRecentWeight.doubleValue(for: weightUnit))
                }
            }
        }
        
        //Perform query to get the weight data
        healthKitSetupAssistant.healthKitStore.execute(query)
    }
    
    func getMostRecentBmi() {
        //Build the query
        let query = HKStatisticsQuery.init(quantityType: HKObjectType.quantityType(forIdentifier: .bodyMassIndex)!,
                                           quantitySamplePredicate: nil,
                                           options: .discreteMostRecent) { (query, results, error) in
            
            let countUnit = HKUnit(from: "count")
            
            DispatchQueue.main.async {
                //Update UI with result
                if let mostRecentBmi = results?.mostRecentQuantity() {
                    guard mostRecentBmi.doubleValue(for: countUnit) > 0 else {
                        //Handle BMI not available
                        self.measurementLabel.font = UIFont.systemFont(ofSize: 25, weight: .semibold)
                        self.measurementLabel.text = NSLocalizedString("not-available", comment: "")
                        return
                    }
                    
                    self.measurementLabel.font = UIFont.systemFont(ofSize: 85, weight: .semibold)
                    self.measurementLabel.text = String(format: "%0.1f", mostRecentBmi.doubleValue(for: countUnit))
                }
            }
        }
        
        //Perform query to get the weight data
        healthKitSetupAssistant.healthKitStore.execute(query)
    }
    
    func getMostRecentBodyFat() {
        //Build the query
        let query = HKStatisticsQuery.init(quantityType: HKObjectType.quantityType(forIdentifier: .bodyFatPercentage)!,
                                           quantitySamplePredicate: nil,
                                           options: .discreteMostRecent) { (query, results, error) in

            let percentUnit = HKUnit(from: "%")
            
            DispatchQueue.main.async {
                //Update UI with result
                if let mostRecentBodyFat = results?.mostRecentQuantity() {
                    guard Double(mostRecentBodyFat.doubleValue(for: percentUnit) * 100.0) > 0 else {
                        //Handle Body Fat not available
                        self.measurementLabel.font = UIFont.systemFont(ofSize: 25, weight: .semibold)
                        self.measurementLabel.text = NSLocalizedString("not-available", comment: "")
                        return
                    }
                    
                    self.measurementLabel.font = UIFont.systemFont(ofSize: 85, weight: .semibold)
                    self.measurementLabel.text = String(format: "%0.1f", mostRecentBodyFat.doubleValue(for: percentUnit))
                }
            }
        }
        
        //Perform query to get the weight data
        healthKitSetupAssistant.healthKitStore.execute(query)
    }
    
    func saveNewWeightMeasurement(weightToSave: Double, forDate: Date) {
        //Save a new weight to HealthKit
        let quantityType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)!
        
        let bodyMass = HKQuantitySample (type: quantityType,
                                         quantity: HKQuantity.init(unit: HKUnit.pound(), doubleValue: weightToSave),
                                         start: forDate,
                                         end: forDate)
        
        //Try to save to HealthKit
        healthKitSetupAssistant.healthKitStore.save(bodyMass) { (success, error) in
            if let error = error {
                self.showErrorWhileSavingToHealthKitMessage()
                print("Error: \(String(describing: error))")
                return
            }
            
            if success {
                //Update label
                self.getMostRecentWeight()
            }
        }
    }
    
    func saveNewBmiMeasurement(bmiToSave: Double, forDate: Date) {
        //Save a new BMI to HealthKit
        let quantityType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMassIndex)!
        
        let bmi = HKQuantitySample (type: quantityType,
                                    quantity: HKQuantity.init(unit: HKUnit.count(), doubleValue: bmiToSave),
                                    start: forDate,
                                    end: forDate)
        
        //Try to save to HealthKit
        healthKitSetupAssistant.healthKitStore.save(bmi) { (success, error) in
            if let error = error {
                self.showErrorWhileSavingToHealthKitMessage()
                print("Error: \(String(describing: error))")
                return
            }
            
            if success {
                //Update label
                self.getMostRecentBmi()
            }
        }
    }
    
    func saveNewBodyFatMeasurement(bodyFatToSave: Double, forDate: Date) {
        //Save a new body fat percentage to HealthKit
        let quantityType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyFatPercentage)!
        
        let bodyFat = HKQuantitySample (type: quantityType,
                                        quantity: HKQuantity.init(unit: HKUnit.percent(), doubleValue: bodyFatToSave),
                                        start: forDate,
                                        end: forDate)
        
        //Try to save to HealthKit
        healthKitSetupAssistant.healthKitStore.save(bodyFat) { (success, error) in
            if let error = error {
                self.showErrorWhileSavingToHealthKitMessage()
                print("Error: \(String(describing: error))")
                return
            }
            
            if success {
                //Update label
                self.getMostRecentBodyFat()
            }
        }
    }
    
    func showErrorWhileSavingToHealthKitMessage() {
        //Message to show if saving to HealthKit fails
        let alert = UIAlertController(title: NSLocalizedString("error-while-saving", comment: ""),
                                      message: NSLocalizedString("we-could-not-save-this-to-healthkit", comment: ""),
                                      preferredStyle: .alert)
        
        //OK action
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""),
                                      style: .default))
        
        //Show alert
        present(alert, animated: true)
    }
    
}

//MARK: - Extensions
extension BodyMeasurementsViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //Prevent non-digit and non-period characters from being typed in a text
        //field
        
        //Limit the number of characters in the text field to 5
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        //Creates a set of invalid characters by excluding digits from the set of all characters
        let invalidCharacters = CharacterSet(charactersIn: ".0123456789").inverted
        
        return string.rangeOfCharacter(from: invalidCharacters) == nil
            && updatedText.count <= 5
    }
    
}
