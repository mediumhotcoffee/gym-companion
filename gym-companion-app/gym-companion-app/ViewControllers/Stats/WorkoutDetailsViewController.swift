//
//  WorkoutDetailsViewController.swift
//  gym-companion-app
//
//  Created by Marcos Silva on 7/21/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import HealthKit

class WorkoutDetailsViewController: UIViewController {

    //MARK: - HealthKit variables
    let healthKitSetupAssistant: HealthKitSetupAssistant = HealthKitSetupAssistant.sharedInstance
    var workoutToDisplay: HKWorkout?
    
    //MARK: - Views
    //Labels
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var maximumLabel: UILabel!
    @IBOutlet weak var averageLabel: UILabel!
    @IBOutlet weak var minimumLabel: UILabel!
    @IBOutlet weak var caloriesBurnedLabel: UILabel!
    
    //MARK: - Lifecycle Methods
    override func viewDidLoad() {
        //Do any additional setup after loading the view
        super.viewDidLoad()
        
        //Show data
        //Get dates for the workout
        let startDate = workoutToDisplay?.startDate
        let endDate = workoutToDisplay?.endDate
        
        //Get and display date
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        dateLabel.text = "Date: \(formatter.string(from: startDate!))"
        
        //Get and display duration
        let minutes = Int(workoutToDisplay!.duration / 60)
        let seconds = Int(workoutToDisplay!.duration) % 60
        if seconds < 10 {
            durationLabel.text = "Time: \(minutes):0\(seconds)"
        } else {
            durationLabel.text = "Time: \(minutes):\(seconds)"
        }
        
        //Get and display calories
        if let caloriesBurned =
            workoutToDisplay?.totalEnergyBurned?.doubleValue(for: .kilocalorie()) {
            let formattedCalories = String(format: "Calories burned (kCal): %.2f", caloriesBurned)
            caloriesBurnedLabel.text = "\(formattedCalories)"
        }
        
        //Establish the predicate/filter
        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: .strictStartDate)
        
        //Establish options
        let options: HKStatisticsOptions = [HKStatisticsOptions.discreteAverage, HKStatisticsOptions.discreteMax, HKStatisticsOptions.discreteMin]
        
        //Set the type of data we want
        if let heartRateType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate) {
            //Build the query
            let query = HKStatisticsQuery.init(quantityType: heartRateType,
                quantitySamplePredicate: predicate,
                options: options) { (query, results, error) in
                                                
                let heartRateUnit = HKUnit(from: "count/min")
                    
                DispatchQueue.main.async {
                    if let max = results?.maximumQuantity()?.doubleValue(for: heartRateUnit),
                        let avg = results?.averageQuantity()?.doubleValue(for: heartRateUnit),
                        let min = results?.minimumQuantity()?.doubleValue(for: heartRateUnit) {
                            self.updateHeartRateLabels(max: max, avg: avg, min: min)
                        }
                }
            }
            
            //Perform query to get the data (avg, max, and min)
            healthKitSetupAssistant.healthKitStore.execute(query)
        }
    }
    
    //MARK: - Actions
    @IBAction func doneTapped(_ sender: UIButton) {
        //Go back to the previous screen
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Screen Management
    override var shouldAutorotate: Bool {
        return false
    }
    
    //MARK: - Custom Methods
    func updateHeartRateLabels(max: Double, avg: Double , min: Double) {
        var maxText = NSLocalizedString("maximum-heart-rate-not-available", comment: "")
        var avgText = NSLocalizedString("average-heart-rate-not-available", comment: "")
        var minText = NSLocalizedString("minimum-heart-rate-not-available", comment: "")
        
        if max > 0 {
            maxText = "Maximum Heart Rate (bpm): \(Int(max))❤️"
        }
        
        if avg > 0 {
            avgText = "Average Heart Rate (bpm): \(Int(avg))❤️"
        }
        
        if min > 0 {
            minText = "Minimum Heart Rate (bpm): \(Int(min))❤️"
        }
        
        maximumLabel.text = maxText
        averageLabel.text = avgText
        minimumLabel.text = minText
    }

}
