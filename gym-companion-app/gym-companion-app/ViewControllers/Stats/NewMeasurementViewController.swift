//
//  NewMeasurementViewController.swift
//  gym-companion-app
//
//  Created by Marcos Silva on 7/21/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import HealthKit

class NewMeasurementViewController: UIViewController {

    //MARK: - HealthKit variables
    let healthKitSetupAssistant: HealthKitSetupAssistant = HealthKitSetupAssistant.sharedInstance
    var measurementType: HKQuantityTypeIdentifier?
    
    //MARK: - Views
    //TextField
    @IBOutlet weak var newMeasurementTextField: UITextField!
    //Labels
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var instructionsLabel: UILabel!
    
    //MARK: - Lifecycle Methods
    override func viewDidLoad() {
        //Do any additional setup after loading the view
        super.viewDidLoad()
        
        //Open keyboard immediately
        newMeasurementTextField.becomeFirstResponder()
        
        //Update labels
        updateLabelsText()
    }
    
    //MARK: - Actions
    @IBAction func cancelTapped(_ sender: UIButton) {
        //Go back to the previous screen
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveTapped(_ sender: UIButton) {
        //Save new measurement when save is tapped
        guard let measurementType = measurementType else { return }
        
        guard let valueToSaveAsText = newMeasurementTextField.text else { return }
        
        guard let valueToSave = Double(valueToSaveAsText) else {
            //Try to convert text to Double
            showErrorWhileSavingToHealthKitMessage()
            return
        }
        
        switch measurementType {
        case HKQuantityTypeIdentifier.bodyMass:
            saveNewWeightMeasurement(weightToSave: valueToSave, forDate: Date())
        case HKQuantityTypeIdentifier.bodyMassIndex:
            saveNewBmiMeasurement(bmiToSave: valueToSave, forDate: Date())
        case HKQuantityTypeIdentifier.bodyFatPercentage:
            saveNewBodyFatMeasurement(bodyFatToSave: valueToSave, forDate: Date())
        default:
            break
        }
    }
    
    //MARK: - Screen Management
    override var shouldAutorotate: Bool {
        //Prevent screen from autorotating
        return false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //Dismiss keyboard when touching the rest of the screen
        newMeasurementTextField.resignFirstResponder()
    }
    
    //MARK: - Custom Methods
    //FIXME: These methods do not dismiss the view controller
    func saveNewWeightMeasurement(weightToSave: Double, forDate: Date) {
        //Save a new weight to HealthKit
        let quantityType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)!
        
        let bodyMass = HKQuantitySample (type: quantityType,
                                         quantity: HKQuantity.init(unit: HKUnit.pound(), doubleValue: weightToSave),
                                         start: forDate,
                                         end: forDate)
        
        //Try to save to HealthKit
        healthKitSetupAssistant.healthKitStore.save(bodyMass) { (success, error) in
            if error != nil {
                self.showErrorWhileSavingToHealthKitMessage()
                print("Error: \(String(describing: error))")
            }
            
            if success {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func saveNewBmiMeasurement(bmiToSave: Double, forDate: Date) {
        //Save a new BMI to HealthKit
        let quantityType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMassIndex)!
        
        let bmi = HKQuantitySample (type: quantityType,
                                         quantity: HKQuantity.init(unit: HKUnit.count(), doubleValue: bmiToSave),
                                         start: forDate,
                                         end: forDate)
        
        //Try to save to HealthKit
        healthKitSetupAssistant.healthKitStore.save(bmi) { (success, error) in
            if error != nil {
                self.showErrorWhileSavingToHealthKitMessage()
                print("Error: \(String(describing: error))")
            }
            
            if success {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func saveNewBodyFatMeasurement(bodyFatToSave: Double, forDate: Date) {
        //Save a new body fat percentage to HealthKit
        let quantityType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyFatPercentage)!
        
        let bodyFat = HKQuantitySample (type: quantityType,
                                         quantity: HKQuantity.init(unit: HKUnit.percent(), doubleValue: bodyFatToSave),
                                         start: forDate,
                                         end: forDate)
        
        //Try to save to HealthKit
        healthKitSetupAssistant.healthKitStore.save(bodyFat) { (success, error) in
            if error != nil {
                self.showErrorWhileSavingToHealthKitMessage()
                print("Error: \(String(describing: error))")
            }
            
            if success {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func showErrorWhileSavingToHealthKitMessage() {
        let alert = UIAlertController(title: "Error While Saving", message: "We could not save this to HealthKit. Please try again", preferredStyle: .alert)
        
        //OK action
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    func updateLabelsText() {
        //Update header and instructions to make it clear to the user what they
        //are saving
        switch measurementType {
        case HKQuantityTypeIdentifier.bodyMass:
            headerLabel.text = "New Weight"
            instructionsLabel.text = "Enter your new weight below:"
        case HKQuantityTypeIdentifier.bodyMassIndex:
            headerLabel.text = "New BMI"
            instructionsLabel.text = "Enter your new BMI below:"
        case HKQuantityTypeIdentifier.bodyFatPercentage:
            headerLabel.text = "New Body Fat"
            instructionsLabel.text = "Enter your new body fat percentage below:"
        default:
            break
        }
    }
    
}

//MARK: - Extensions
extension NewMeasurementViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //Prevent non-digit and non-period characters from being typed in a text
        //field
        
        //Creates a set of invalid characters by excluding digits from the set of all characters
        let invalidCharacters = CharacterSet(charactersIn: ".0123456789").inverted
        
        return string.rangeOfCharacter(from: invalidCharacters) == nil
    }
}
