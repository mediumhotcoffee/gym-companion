//
//  SignUpViewController.swift
//  gym-companion-app
//
//  Created by Marcos Silva on 6/26/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class CreateAccountViewController: UIViewController {
    
    //MARK: - Views
    //TextFields
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    //Buttons
    @IBOutlet weak var addAccountPictureButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    //Switch
    @IBOutlet weak var isTrainerSwitch: UISwitch!
    //ActivityIndicator
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: - Firebase variables
    var databaseReference: DatabaseReference!
    var userID: String?
    
    //MARK: - Form variables
    var firstName: String? = nil
    var lastName: String? = nil
    var email: String? = nil
    var password: String? = nil
    var isTrainer: Bool = false
    var userPicture: UIImage? = nil
    
    //MARK: - Other variables
    var userWasCreated: Bool = false
    
    //MARK: - Lifecycle management
    override func viewWillAppear(_ animated: Bool) {
        //Get Firebase database reference
        self.databaseReference = Database.database().reference()
    }
    
    override func viewDidLoad() {
        //Do any additional setup after loading the view
        super.viewDidLoad()
        
        //Setup TextField delegates
        (firstNameTextField.delegate, lastNameTextField.delegate, emailTextField.delegate, passwordTextField.delegate, confirmPasswordTextField.delegate) = (self, self, self, self, self)
    }
    
    //MARK: - Actions
    @IBAction func cancelTapped(_ sender: UIButton) {
        //Go to previous screen
        dismiss(animated: true)
    }
    
    @IBAction func createTapped(_ sender: UIButton) {
        //Try to create an user account
        
        //Block UI while this runs
        blockUI(true)
        
        //Get data from form
        let formValidationUtil: FormValidationUtil = FormValidationUtil(formViewController: self,
                                                                        firstNameTextField: firstNameTextField,
                                                                        lastNameTextField: lastNameTextField,
                                                                        emailTextField: emailTextField,
                                                                        passwordTextField: passwordTextField,
                                                                        confirmPasswordTextField: confirmPasswordTextField)
        
        guard let firstNameExtracted = formValidationUtil.firstNameExtractor(),
            let lastNameExtracted = formValidationUtil.lastNameExtractor(),
            let emailExtracted = formValidationUtil.emailExtractor(),
            let passwordExtracted = formValidationUtil.passwordExtractor() else {
            //Unblock the UI if extraction fails
            blockUI(false)
            return
        }
        
        firstName = firstNameExtracted
        lastName = lastNameExtracted
        email = emailExtracted
        password = passwordExtracted
        
        //Get type of user (trainer or trainee)
        if isTrainerSwitch.isOn { isTrainer = true }
        
        //Create the user on Firebase Auth
        Auth.auth().createUser(withEmail: email!, password: password!) { (authResult, error) in
            if let error = error {
                //Notify user of error
                let fA = FirebaseAuthValidationUtil(formViewController: self)
                fA.notifyOfFirebaseAuthError(error)

                //Reenable UI if FirebaseAuth validation fails
                self.blockUI(false)
                
                return
            }
            
            //Create user in Firebaser Realtime Database if Firebase Auth
            //suceeds in creating a new user
            let user = User(
                userImageID: nil,
                firstName: self.firstName!,
                lastName: self.lastName!,
                isTrainer: self.isTrainer,
                paid: false,
                email: self.email!,
                trainerID: nil,
                routines: [],
                exercises: [],
                bags: [],
                trainees: []
            )

            self.databaseReference.child("users/\(authResult!.user.uid)")
                .setValue(user.asDict)

            //Create the user's picture on Firebase Storage
            let storageReference = Storage.storage().reference()
            
            if let pictureToSave = self.userPicture {
                ImageUtil.saveImageToFirebaseStorage(storageReference: storageReference, picture: pictureToSave, userID: (authResult?.user.uid)!)
            }
            
            self.userWasCreated = true
            
            //Dismiss form through segue
            self.performSegue(withIdentifier: "userCreatedSegue", sender: self)
        }
    }
    
    //MARK: - Navigation
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        //Block segue if user was not created
        if userWasCreated {
            return true
        }
        
        return false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //Send data to AccountViewController after user is done
        //creating an account
        firstName = firstNameTextField.text!
        lastName = firstNameTextField.text!
        email = firstNameTextField.text!
        isTrainer = isTrainerSwitch.isOn
    }
    
    //MARK: - Screen Management
    override var shouldAutorotate: Bool {
        //Prevent the screen from auto rorataion
        return false
    }
    
    func blockUI(_ shouldBlockUI: Bool) {
        if shouldBlockUI {
            //Block buttons
            cancelButton.isEnabled = false
            saveButton.isEnabled = false
            addAccountPictureButton.isEnabled = false
            //Block switch
            isTrainerSwitch.isEnabled = false
            //Show activity indicator
            activityIndicator.startAnimating()
            activityIndicator.isHidden = false
            //Dismiss keyboard
            firstNameTextField.resignFirstResponder()
            lastNameTextField.resignFirstResponder()
            emailTextField.resignFirstResponder()
            passwordTextField.resignFirstResponder()
            confirmPasswordTextField.resignFirstResponder()
        } else {
            //Unblock buttons
            cancelButton.isEnabled = true
            saveButton.isEnabled = true
            addAccountPictureButton.isEnabled = true
            //Block switch
            isTrainerSwitch.isEnabled = true
            //Show activity indicator
            activityIndicator.stopAnimating()
            activityIndicator.isHidden = true
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //Makes TextFields dismiss the keyboard when something else is
        //tapped
        firstNameTextField.resignFirstResponder()
        lastNameTextField.resignFirstResponder()
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        confirmPasswordTextField.resignFirstResponder()
    }
    
    //MARK: - Custom methods
    func showRecentLoginRequiredAlert() {
        //Notify the user that they need to log in recently
        let alert = UIAlertController(title: NSLocalizedString("recent-log-in-required", comment: ""),
                                      message: NSLocalizedString("this-operation-requires-recent-authentication", comment: ""),
                                      preferredStyle: .actionSheet)
        
        //Login action
        alert.addAction(UIAlertAction(title: NSLocalizedString("login", comment: ""),
                                      style: .default,
                                      handler: { (action) -> Void in
            let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
            self.present(viewcontroller!, animated: true)
        }))
    
        //Cancel action
        alert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""),
                                      style: .cancel,
                                      handler: { (action) -> Void in
            self.dismiss(animated: true)
        }))
        
        //Show alert
        self.present(alert, animated: true)
    }
    
}

//MARK: - Extensions
extension CreateAccountViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //Move to the next TextField when filling out the form
        switch textField {
        case firstNameTextField:
            lastNameTextField.becomeFirstResponder()
        case lastNameTextField:
            emailTextField.becomeFirstResponder()
        case emailTextField:
            passwordTextField.becomeFirstResponder()
        case passwordTextField:
            confirmPasswordTextField.becomeFirstResponder()
        case confirmPasswordTextField:
            confirmPasswordTextField.resignFirstResponder()
        default:
            break
        }
        
        return true
    }
    
}

extension CreateAccountViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBAction func addAccountPictureTapped(_ sender: UIButton) {
        //IBAction is located here because it requires these extensions
        ImageUtil.getNewImageForAccount(from: self, sender: sender)
    }
 
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //Handle the image picked by the user
        guard let userPictureRetrieved = info[.editedImage] as? UIImage else {
            print("ERROR: No image retrieved")
            return
        }
        
        userPicture = userPictureRetrieved
        
        picker.dismiss(animated: true) {
            //Display picture to the user on the button
            self.addAccountPictureButton.contentMode = .scaleToFill
            self.addAccountPictureButton.setImage(self.userPicture, for: .normal)
            self.addAccountPictureButton.layer.cornerRadius = self.addAccountPictureButton.frame.size.width / 2
            self.addAccountPictureButton.clipsToBounds = true
        }
    }
    
}
