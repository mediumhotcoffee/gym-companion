//
//  AccountViewController.swift
//  gym-companion-app
//
//  Created by Marcos Silva on 6/26/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class AccountViewController: UIViewController {
    
    //MARK: - Firebase Variables
    var handle: AuthStateDidChangeListenerHandle?
    var databaseReference: DatabaseReference!
    var userId: String?
    var firebaseUser: FirebaseAuth.User?
    
    //MARK: - User Info Variables
    var firstName: String?
    var lastName: String?
    var isTrainer: Bool?
    var paid: Bool?
    var userTrainer: String?
    var passwordReceived: String?
    var traineeIDs: [String]?
    
    //MARK: - Views
    //Labels
    @IBOutlet weak private var nameLabel: UILabel!
    @IBOutlet weak private var emailLabel: UILabel!
    @IBOutlet weak private var trainerLabel: UILabel!
    @IBOutlet weak private var logoutLabel: UILabel!
    @IBOutlet weak private var editAccountLabel: UILabel!
    @IBOutlet weak private var upgradeLabel: UILabel!
    //Buttons
    @IBOutlet weak private var addTraineeButton: UIButton!
    @IBOutlet weak private var addTraineeIconButton: UIButton!
    @IBOutlet weak private var settingsButton: UIButton!
    @IBOutlet weak private var settingsIconButton: UIButton!
    @IBOutlet weak private var logoutButton: UIButton!
    @IBOutlet weak private var editAccountButton: UIButton!
    @IBOutlet weak private var upgradeButton: UIButton!
    //ActivityIndicator
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    //ImageView
    @IBOutlet weak var userImageView: UIImageView!
    
    //MARK: - Lifecycle Methods
    override func viewWillAppear(_ animated: Bool) {
        //Setup
        ImageUtil.loadUserPictureOn(imageView: userImageView, userId: Auth.auth().currentUser?.uid ?? "")
        
        //Add log in status listener
        handle = Auth.auth().addStateDidChangeListener({ (auth, user) in
            //Verify that a user exists/is logged in
            guard let user = user else {
                //Ask to open the LogInViewController if the user is not logged
                //in
                
                //Notify the user that they are not logged in
                let loginStateAlertController = UIAlertController(title: NSLocalizedString("account-not-found", comment: ""),
                                                                  message: NSLocalizedString("would-you-like-to-log-in-or-create-an-account", comment: ""),
                                                                  preferredStyle: .alert)
                
                //Login action
                loginStateAlertController.addAction(UIAlertAction(title: NSLocalizedString("login", comment: ""),
                                                                  style: .default,
                                                                  handler: { (action) -> Void in
                    let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                    self.present(viewcontroller!, animated: true, completion: nil)
                }))
                
                //Create Account action
                loginStateAlertController.addAction(UIAlertAction(title: NSLocalizedString("create-account", comment: ""),
                                                                  style: .default,
                                                                  handler: { (action) -> Void in
                    let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "CreateAccountViewController") as? CreateAccountViewController
                    self.present(viewcontroller!, animated: true, completion: nil)
                }))
                
                //Cancel action
                loginStateAlertController.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: { (action) -> Void in
                    self.dismiss(animated: true, completion: nil)
                }))
                
                //Show alert
                self.present(loginStateAlertController, animated: true)
                
                return
            }
            
            //Show that the app is loading data
            self.activityIndicator.startAnimating()
            self.activityIndicator.isHidden = false
            
            //Get database reference
            self.databaseReference = Database.database().reference()
            
            //Get user reference
            self.firebaseUser = user
        
            //Load user info if they are logged in
            self.userId = user.uid
            self.databaseReference.child("users").child(self.userId ?? "").observeSingleEvent(of: .value, with: { (snapshot) in
                //Get user info
                let value = snapshot.value as? NSDictionary
                
                self.firstName = value?["firstName"] as? String ?? ""
                self.lastName = value?["lastName"] as? String ?? ""
                self.isTrainer = value?["isTrainer"] as? Bool ?? false
                self.userTrainer = value?["trainerID"] as? String ?? ""
                self.paid = value?["paid"] as? Bool ?? false
                self.traineeIDs = value?["trainees"] as? [String] ?? []
                
                //Load and show user info if they are logged in
                self.hideOrShowUI(shouldShow: true)
                self.updateUIWithUserData(firstName: self.firstName!, lastName: self.lastName!, email: user.email, isTrainer: self.isTrainer!)
                
                //Remove spinner once data loads
                self.activityIndicator.stopAnimating()
            }) { (error) in
                //TODO: Handle error better
                print(error.localizedDescription)
            }
        })
    }
    
    override func viewDidLoad() {
        //Do any additional setup after loading the view
        super.viewDidLoad()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //Remove log in status listener
        Auth.auth().removeStateDidChangeListener(handle!)
    }

    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //Inform if we are on account edit mode
        switch segue.identifier {
        case Segue.toEditAccountSegue.stringValue:
            if let destination = segue.destination as? EditAccountViewController {
                //Send current account info to the EditAccount view controller
                
                //Prepare to fill out TextFields with user info
                destination.firstName = firstName ?? ""
                destination.lastName = lastName ?? ""
                destination.email = Auth.auth().currentUser?.email ?? ""
                destination.isTrainer = isTrainer ?? false
                
                //Send user ID
                destination.userID = userId
            }
        case Segue.toAddTraineeSegue.stringValue:
            if let destination = segue.destination as? AddTraineeViewController, let userId = userId {
                destination.trainerId = userId
            }
        case Segue.toShowQrCodeSegue.stringValue:
            if let destination = segue.destination as? QrCodeViewController {
                destination.userId = userId
            }
        case Segue.toUpgradeAccount.stringValue:
            if let destination = segue.destination as? UpgradeAccountViewController,
                let isTrainer = isTrainer, let paid = paid
            {
                destination.currentUserIsTrainer = isTrainer
                destination.paid = paid
            }
        case Segue.toSettingsSegue.stringValue:
            //Send data to Settings screen
            if let destination = segue.destination as? SettingsViewController {
                destination.passwordReceived = passwordReceived
            }
        default:
            break
        }
    }
    
    @IBAction func unwindAfterTraineeIsAdded(segue: UIStoryboardSegue) {
        if let source = segue.source as? AddTraineeViewController,
            let trainerID = userId,
            let traineeID = source.traineeID {
            
            FirebaseAccountsUtil.addTraineeToTrainer(trainerID: trainerID, traineeID: traineeID)
        }
    }
    
    @IBAction func unwindAfterUserIsCreatedOrEdited(segue: UIStoryboardSegue) {
        //Update to show user's data after account creation or editing
        if segue.source is CreateAccountViewController {
            if let source = segue.source as? CreateAccountViewController {
                hideOrShowUI(shouldShow: true)
                
                //FIXME: Email is not updating after unwind segue
                if let firstName = source.firstName, let lastName = source.lastName, let email = source.email {
                    updateUIWithUserData(firstName: firstName, lastName: lastName, email: email, isTrainer: source.isTrainer)
                }
            }
        }
    }
    
    @IBAction func unwindAfterUserIsLoggedIn(segue: UIStoryboardSegue) {
        //Get the user's password
        if segue.source is LoginViewController {
            if let source = segue.source as? LoginViewController {
                hideOrShowUI(shouldShow: true)
                
                //Get the password that was sent so we can possible save it to
                //the Keychain
                passwordReceived = source.passwordToSend
            }
        }
    }
    
    //MARK: - Screen Management
    override var shouldAutorotate: Bool {
        return false
    }
    
    //MARK: - Actions
    @IBAction func accountTapped(_ sender: UIButton) {
        //Return to normal navigation
        self.dismiss(animated: true)
    }
    
    @IBAction func addTraineeTapped(_ sender: UIButton) {
        //Add a trainee to the trainer
        if let isTrainer = isTrainer {
            //TODO: Check if paid or under limit
            if isTrainer {
                if let paid = paid, paid {
                    
                    performSegue(withIdentifier: Segue.toAddTraineeSegue.stringValue, sender: self)
                }
                else if let numTrainees = traineeIDs?.count, numTrainees < 3 {
                    
                    performSegue(withIdentifier: Segue.toAddTraineeSegue.stringValue, sender: self)
                }
                else {
                    
                    let alert: UIAlertController = AlertHelper.getLimitAlert(listType: .trainees, delegate: self)
                    present(alert, animated: true, completion: nil)
                }
            } else {
                performSegue(withIdentifier: Segue.toShowQrCodeSegue.stringValue, sender: self)
            }
        }
    }
    
    @IBAction func logoutTapped(_ sender: Any) {
        //Log the user out
        do {
            try Auth.auth().signOut()
            
            let logoutConfirmationAlertController = UIAlertController(title: NSLocalizedString("logout", comment: ""),
                                                                      message: NSLocalizedString("would-you-like-to-logout", comment: ""),
                                                                      preferredStyle: .alert)
            
            //Logout action
            logoutConfirmationAlertController.addAction(UIAlertAction(title: NSLocalizedString("logout", comment: ""),
                                                                      style: .destructive,
                                                                      handler: { (action) -> Void in
                //Transition back when login out
                self.dismiss(animated: true)
            }))
            
            //Cancel action
            logoutConfirmationAlertController.addAction(UIAlertAction(title: "Cancel",
                                                                      style: .cancel))
            
            //Show alert
            present(logoutConfirmationAlertController, animated: true, completion: nil)
        } catch {
            //TODO: Handle sign out errors better
            print(error.localizedDescription)
        }
    }
    
    @IBAction func editAccountTapped(_ sender: UIButton) {
        //Go to the Edit Account screen
        performSegue(withIdentifier: Segue.toEditAccountSegue.stringValue, sender: self)
    }
    
    @IBAction func settingsTapped(_ sender: UIButton) {
        //Go to Settings screen
        performSegue(withIdentifier: Segue.toSettingsSegue.stringValue, sender: self)
    }
    
    //MARK: - Custom Methods
    func hideOrShowUI(shouldShow: Bool) {
        //Shows or hides the UI
        if shouldShow {
            //Labels
            nameLabel.isHidden = false
            emailLabel.isHidden = false
            trainerLabel.isHidden = false
            logoutLabel.isHidden = false
            editAccountLabel.isHidden = false
            //Buttons
            logoutButton.isHidden = false
            editAccountButton.isHidden = false
            addTraineeButton.isHidden = false
            addTraineeIconButton.isHidden = false
            settingsButton.isHidden = false
            settingsIconButton.isHidden = false
            
            if let paid = paid {
                if paid {
                    // Hide upgrade button since user is already upgraded
                    upgradeLabel.isHidden = true
                    upgradeButton.isHidden = true
                } else {
                    // Show upgrade button
                    upgradeLabel.isHidden = false
                    upgradeButton.isHidden = false
                }
            }
            
            if let isTrainer = isTrainer {
                if isTrainer {
                    //Show regular button
                    addTraineeButton.isHidden = false
                } else {
                    //Show QR code button
                    addTraineeButton.setTitle("Show QR Code", for: .normal)
                    addTraineeButton.isHidden = false
                }
            }
        } else {
            //Labels
            nameLabel.isHidden = true
            emailLabel.isHidden = true
            trainerLabel.isHidden = true
            logoutLabel.isHidden = true
            editAccountLabel.isHidden = true
            //Buttons
            logoutButton.isHidden = true
            editAccountButton.isHidden = true
            addTraineeButton.isHidden = true
            upgradeLabel.isHidden = true
            upgradeButton.isHidden = true
            addTraineeButton.isHidden = true
            addTraineeIconButton.isHidden = true
            settingsButton.isHidden = true
            settingsIconButton.isHidden = true
        }
    }
    
    func updateUIWithUserData(firstName: String, lastName: String, email: String?, isTrainer: Bool) {
        //Show the user's data on the UI
        nameLabel.text = firstName + " " + lastName
        
        if let email = email {
            emailLabel.text = email
        } else {
            emailLabel.text = NSLocalizedString("no-email-available", comment: "")
        }
        
        if isTrainer {
            trainerLabel.text = NSLocalizedString("welcome-trainer", comment: "")
        } else {
            //TODO: In the future display the trainer's name
            if let uT = userTrainer {
                if uT == "" || uT == "none" {
                    trainerLabel.text = NSLocalizedString("you-currently-dont-have-a-trainer", comment: "")
                } else {
                    //Get trainer's name
                    databaseReference.child("users").child(uT).observeSingleEvent(of: .value, with: { (snapshot) in
                        //Get trainer name
                        let value = snapshot.value as? NSDictionary
                        let firstName = value?["firstName"] as? String ?? ""
                        let lastName = value?["lastName"] as? String ?? ""
                        
                        self.trainerLabel.text = "Your Trainer is \(firstName) \(lastName)"
                    }) { (error) in
                        print(error.localizedDescription)
                    }
                }
            } else {
                trainerLabel.text = NSLocalizedString("you-currently-dont-have-a-trainer", comment: "")
            }
        }
    }
    
}

extension AccountViewController: FirebaseAccountDelegate {
    
    func userPulled(user: User, current: Bool) {
        //Nothing else needs to happen here
    }

    func signInSelected(_ sender: UIAlertAction) {
        //Nothing else needs to happen here
    }

    func upgradeTapped(sender: UIAlertAction) {
        performSegue(withIdentifier: Segue.toUpgradeAccount.stringValue, sender: sender)
    }
    
}
