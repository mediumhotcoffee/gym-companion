//
//  QrCodeViewController.swift
//  gym-companion-app
//
//  Created by Marcos Silva on 7/21/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

class QrCodeViewController: UIViewController {

    //MARK: - Variables
    var userId: String?
    
    //MARK: - Views
    @IBOutlet weak var qrCodeImageView: UIImageView!
    
    //MARK: - Lifecycle Methods
    override func viewDidLoad() {
        //Do any additional setup after loading the view
        super.viewDidLoad()

        //Load QR code
        if let userId = userId {
            if let qrCodeImage = generateQRCode(from: userId) {
                qrCodeImageView.image = qrCodeImage
            }
        }
    }
    
    //MARK: - Actions
    @IBAction func doneTapped(_ sender: UIButton) {
        //Go to the previous screen
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Screen Management
    override var shouldAutorotate: Bool {
        //Prevent the screen from auto rotating
        return false
    }
    
    //MARK: - Custom Methods
    func generateQRCode(from string: String) -> UIImage? {
        //Generates a QR code from a string
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }

}
