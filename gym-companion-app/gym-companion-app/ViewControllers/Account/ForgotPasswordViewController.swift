//
//  ForgotPasswordViewController.swift
//  gym-companion-app
//
//  Created by Marcos Silva on 6/27/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseAuth

class ForgotPasswordViewController: UIViewController {
    
    //MARK: - Views
    //TextField
    @IBOutlet private weak var emailTextField: UITextField!
    //Buttons
    @IBOutlet private weak var cancelButton: UIButton!
    @IBOutlet private weak var submitButton: UIButton!
    //ActivityIndicator
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        //Do any additional setup after loading the view
    }
    
    //MARK: - Actions
    @IBAction func cancelTapped(_ sender: Any) {
        //Go back to the previous screen
        dismiss(animated: true)
    }
    
    @IBAction func submitTapped(_ sender: Any) {
        //Try to send password reset email
        
        //Block the UI to avoid multiple attempts
        blockUI(true)
        
        //Try to extract email
        let formValidationUtil = FormValidationUtil(formViewController: self, emailTextField: emailTextField)
        
        guard let email: String = formValidationUtil.emailExtractor() else {
            //Reenable the UI if we can't get an email
            self.blockUI(false)
            
            return
        }
        
        Auth.auth().sendPasswordReset(withEmail: email) { error in
            if let error = error {
                //Handle Auth errors
                
                let firebaseAuthValidationUtil = FirebaseAuthValidationUtil(formViewController: self)
                firebaseAuthValidationUtil.notifyOfFirebaseAuthError(error)
                
                //Reenable the UI if an error occurs
                self.blockUI(false)
                
                return
            }
            
            //Notify the user if the operation was successful
            let confirmationAlertController = UIAlertController(title: NSLocalizedString("reset-email-sent", comment: ""),
                                                                message: NSLocalizedString("your-password-reset-email-was-sent", comment: ""),
                                                                preferredStyle: .alert)
            
            //OK action
            confirmationAlertController.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""),
                                                                style: .default,
                                                                handler: { (action) in
                self.dismiss(animated: true)
            }))
            
            //Show alert
            self.present(confirmationAlertController, animated: true)
        }
    }
    
    //MARK: - Screen Management
    override var shouldAutorotate: Bool {
        return false
    }
    
    func blockUI(_ shouldBlockUI: Bool) {
        //Blocks or unblocks the UI
        if shouldBlockUI {
            //Block buttons
            submitButton.isEnabled = false
            cancelButton.isEnabled = false
            //Start activity indicator
            activityIndicator.startAnimating()
            activityIndicator.isHidden = false
            //Dismiss keyboard
            emailTextField.resignFirstResponder()
        } else {
            //Unblock buttons
            submitButton.isEnabled = true
            cancelButton.isEnabled = true
            //Stop activity indicator
            activityIndicator.stopAnimating()
            activityIndicator.isHidden = true
        }
    }
    
}
