//
//  QrCodeScannerViewController.swift
//  gym-companion-app
//
//  Created by Marcos Silva on 7/21/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import AVFoundation

class QrCodeScannerViewController: UIViewController {
    
    //MARK: - Variables
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var userId: String?
    
    //MARK: - Lifecycle Methods
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (captureSession?.isRunning == false) { captureSession.startRunning() }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Start QR code capture process
        view.backgroundColor = UIColor.black
        
        //Define the session
        captureSession = AVCaptureSession()
        
        //Get a reference to the camera
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        
        //Add video as an input to the session
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        //Set QR output as output from the session
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        //Add camera layer to the main view
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        
        //Add a cancel button to the main view
        addCancelButtonToView()
        addInstructionsToView()
        
        //Start the session
        captureSession.startRunning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) { captureSession.stopRunning() }
    }
    
    //MARK: - Custom Methods
    func failed() {
        //Let the usert know that a QR code scanner is not available
        let alert = UIAlertController(title: "Scanning Not Supported",
                                      message: "Your device does not support QR code scanning. Please use a device with a camera",
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""),
                                      style: .default))
        
        present(alert, animated: true)
        
        captureSession = nil
    }
    
    func found(code: String) {
        //Handle the QR code if found
        userId = code
        
        performSegue(withIdentifier: Segue.unwindToAddTraineeSegue.stringValue, sender: self)
    }
    
    func addCancelButtonToView() {
        //Create a Cancel button to dismiss the view
        let cancelButton = UIButton(type:.custom)
        let cancelLabel = UILabel(frame: cancelButton.frame)
        
        //Set text on the Cancel label
        cancelLabel.text = NSLocalizedString("cancel", comment: "")
        cancelLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        cancelLabel.textColor = .white
        
        //Set button's image
        cancelButton.setImage(UIImage(named: "bigRedButtonShape"), for: .normal)
        
        //Set button's action
        cancelButton.addTarget(self, action: #selector(cancelButtonAction), for: .touchUpInside)
        
        //Add Cancel button and label to the main view
        view.addSubview(cancelButton)
        view.addSubview(cancelLabel)
        
        //Set constraints for the Cancel button
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        let cancelButtonCenterConstraint = cancelButton.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let cancelButtonBottomConstraint = cancelButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -60)
        
        //Set constraints for the Cancel label
        cancelLabel.translatesAutoresizingMaskIntoConstraints = false
        let cancelLabelCenterXConstraint = cancelLabel.centerXAnchor.constraint(equalTo: cancelButton.centerXAnchor)
        let cancelLabelCenterYConstraint = cancelLabel.centerYAnchor.constraint(equalTo: cancelButton.centerYAnchor)
        
        //Add button's and label's contraints to main view
        view.addConstraints([cancelButtonCenterConstraint,
                             cancelButtonBottomConstraint,
                             cancelLabelCenterXConstraint,
                             cancelLabelCenterYConstraint])
    }
    
    @objc func cancelButtonAction(sender: UIButton!) {
        //Action for Cancel button
        self.dismiss(animated: true)
    }
    
    func addInstructionsToView() {
        //Create floating instructions for the QR code scanner
        
        //Create a label
        let instructionsLabel = UILabel(frame: view.frame)
        
        //Set text on the label
        instructionsLabel.text = "Point your camera toward the QR code to add a trainee"
        instructionsLabel.textAlignment = .center
        instructionsLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        instructionsLabel.textColor = .white
        instructionsLabel.numberOfLines = 2
        
        //Add Instructions label to the view
        view.addSubview(instructionsLabel)
        
        //Set constraints for the Instructions label
        instructionsLabel.translatesAutoresizingMaskIntoConstraints = false
        let instructionsLabelTopConstraint = instructionsLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 60)
        let instructionsLabelLeadingConstraint = instructionsLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20)
        let instructionsLabelTrailingConstraint = instructionsLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
        
        //Add Instruction label's contraints to main view
        view.addConstraints([instructionsLabelTopConstraint,
                             instructionsLabelLeadingConstraint,
                             instructionsLabelTrailingConstraint])
    }
    
    //MARK: - Screen Management
    override var prefersStatusBarHidden: Bool {
        //Hide the status bar
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        //Allow portrait only (the camera looks weird on landscape)
        return .portrait
    }
    
}

//MARK: - Extensions
extension QrCodeScannerViewController: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        //Stop the capture session
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            
            guard let stringValue = readableObject.stringValue else { return }
            
            //Play a sound
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            
            //Perform an action with the code that was found
            found(code: stringValue)
        }
    }
    
}
