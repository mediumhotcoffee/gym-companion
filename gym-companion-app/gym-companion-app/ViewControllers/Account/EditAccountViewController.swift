//
//  EditAccountViewController.swift
//  gym-companion-app
//
//  Created by Marcos Felipe Costa da Silva on 7/15/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class EditAccountViewController: UIViewController {

    //MARK: - Views
    //TextFields
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    //Buttons
    @IBOutlet weak var addAccountPictureButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var deleteAccountButton: UIButton!
    //Switch
    @IBOutlet weak var isTrainerSwitch: UISwitch!
    //ActivityIndicator
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: - Firebase Variables
    var handle: AuthStateDidChangeListenerHandle?
    var databaseReference: DatabaseReference!
    var storageReference: StorageReference!
    var userID: String?
    
    //MARK: - Variables
    //Form variables
    var firstName: String? = nil
    var lastName: String? = nil
    var email: String? = nil
    var password: String? = nil
    var isTrainer: Bool = false
    var userPicture: UIImage? = nil
    //Other variables
    var userWasEdited: Bool = false
    var authUpdatesSucceeded: Bool = true
    
    //MARK: - Lifecycle Methods
    override func viewWillAppear(_ animated: Bool) {
        //Get references
        databaseReference = Database.database().reference()
        storageReference = Storage.storage().reference()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Do any additional setup after loading the view
        
        //Fill out form with existing data
        fillOutForm()
        
        //Load current image
        ImageUtil.loadUserPictureOn(button: addAccountPictureButton, userId: Auth.auth().currentUser?.uid ?? "")
        
        //Setup TextField delegates
        (firstNameTextField.delegate, lastNameTextField.delegate, emailTextField.delegate, passwordTextField.delegate, confirmPasswordTextField.delegate) = (self, self, self, self, self)
    }

    //MARK: - Actions
    @IBAction func cancelTapped(_ sender: UIButton) {
        //Go to previous screen
        dismiss(animated: true)
    }
    
    @IBAction func updateAccountTapped(_ sender: UIButton) {
        //Try to edit the user
        
        //Block UI while this runs
        blockUI(true)
        
        //Get data from form
        let formValidationUtil: FormValidationUtil =
            FormValidationUtil(formViewController: self,
                               firstNameTextField: firstNameTextField,
                               lastNameTextField: lastNameTextField,
                               emailTextField: emailTextField,
                               passwordTextField: passwordTextField,
                               confirmPasswordTextField: confirmPasswordTextField)
        
        guard let firstName = formValidationUtil.firstNameExtractor(),
            let lastName = formValidationUtil.lastNameExtractor(),
            let email = formValidationUtil.emailExtractor() else {
            //Unblock the UI if extraction fails
            blockUI(false)
            return
        }
        
        //Get type of user (trainer or trainee)
        if isTrainerSwitch.isOn {
            isTrainer = true
        } else {
            isTrainer = false
        }
        
        //Edit current user on Firebase Database
        guard let userID = userID else { return }
        
        //Get Firebase Realtime Database reference
        let userReference = databaseReference.child("users/\(userID)")
        
        //Get updates to make
        let childUpdates: [String:String] = [
            "firstName": firstName,
            "lastName": lastName,
            "email": email
        ]
        
        //Update data on Firebase Realtime Database
        userReference.updateChildValues(childUpdates)
        
        //Edit email
        Auth.auth().currentUser?.updateEmail(to: email) { (error) in
            if let error = error {
                //TODO: Handle errors better
                print(error.localizedDescription)
                
                //Ask for login if user has not logged in recently
                if let errorCode = AuthErrorCode(rawValue: error._code), errorCode == .requiresRecentLogin {
                    self.showRecentLoginRequiredAlert()
                }
                
                //Reenable UI if error occurs
                self.blockUI(false)
                
                self.authUpdatesSucceeded = false
            }
        }
        
        //Edit password
        if !(passwordTextField.text!.isEmpty) &&
            !(confirmPasswordTextField.text!.isEmpty) {
            guard let currentPassword = formValidationUtil.passwordExtractor(),
                let newPassword = formValidationUtil.passwordExtractor() else {
                    blockUI(false)
                    return
            }
            
            Auth.auth().currentUser?.updatePassword(to: newPassword, completion: { (error) in
                if let error = error {
                    //TODO: Handle errors better
                    print(error.localizedDescription)

                    //Ask for login if user has not logged in recently
                    if let errorCode = AuthErrorCode(rawValue: error._code), errorCode == .requiresRecentLogin {
                        self.showRecentLoginRequiredAlert()
                    }

                    //Reenable UI if error occurs
                    self.blockUI(false)
                    
                    self.authUpdatesSucceeded = false
                }
            })
        }
        
        //Update picture
        if let pictureToSave = self.userPicture {
            ImageUtil.saveImageToFirebaseStorage(storageReference: storageReference, picture: pictureToSave, userID: Auth.auth().currentUser?.uid ?? "")
        }
        
        //Close after edits are done
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            //FIXME: Make the operation wait for 0.5 seconds
            //(not ideal, but there is no time to fix it now)
            if self.authUpdatesSucceeded {
                self.dismiss(animated: true)
            }
        }
    }
    
    @IBAction func deleteAccountTapped(_ sender: UIButton) {
        //Delete the account
        
        //Block UI while method runs
        blockUI(true)
        
        let deletionAlertController = UIAlertController(title: "Delete Account",
                                                        message: "Do you want to delete your account? This action is irreversible and all your data will be lost.",
                                                        preferredStyle: .alert)
        
        //Delete action
        deletionAlertController.addAction(UIAlertAction(title: "Delete",
                                                        style: .destructive,
                                                        handler: { (action) -> Void in
            let user = Auth.auth().currentUser
            
            //Delete account from Auth
            user?.delete { error in
                if let error = error {
                    print("\nERROR:\n" +
                        "\(String(describing: self))\n" +
                        "\(error.localizedDescription)\n")
                    
                    //Reenable UI since there was an error
                    self.blockUI(false)
                    
                    if let errorCode = AuthErrorCode(rawValue: error._code), errorCode == .requiresRecentLogin {
                        //Notify the user that they need to log in recently
                        let alert = UIAlertController(title: "Recent log in required",
                                                      message: "This operation is sensitive and requires recent authentication. Please log in again",
                                                      preferredStyle: .actionSheet)
                        
                        //Login button
                        alert.addAction(UIAlertAction(title: "Login",
                                                      style: .default,
                                                      handler: { (action) -> Void in
                            let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                            self.present(viewcontroller!, animated: true, completion: nil)
                        }))
                        
                        //Cancel button
                        alert.addAction(UIAlertAction(title: "Cancel",
                                                      style: .cancel,
                                                      handler: { (action) -> Void in
                            self.dismiss(animated: true)
                        }))
                        
                        //Show alert
                        self.present(alert, animated: true)
                        
                        return
                    }
                } else {
                    //Delete account data from database if Auth delete was
                    //successful
                    self.databaseReference.child("users").child(self.userID ?? "")
                        .setValue(nil, withCompletionBlock: { (error, databaseReference) in
                            if let error = error {
                                print("\nERROR: Data could not be deleted\n"
                                    + "\(error.localizedDescription)")
                            } else {
                                print("\nSUCCESS: Data deleted from database\n")
                            }
                        })
                    
                    //Delete account's picture from Firebase Storage if
                    //Auth delete was successful
                    ImageUtil.deleteImageOnFirebaseStorage(storageReference: self.storageReference, button: self.addAccountPictureButton, userId: Auth.auth().currentUser?.uid ?? "")
                    
                    //Show confirmation that account was deleted
                    let confirmationAlertController = UIAlertController(title: "Account Deleted", message: "Your account was deleted. Sorry to see you go.", preferredStyle: .alert)
                    
                    //OK action
                    confirmationAlertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    
                    self.present(confirmationAlertController, animated: true)
                }
            }
        }))
        
        //Cancel action
        deletionAlertController.addAction(UIAlertAction(title: "Cancel",
                                                        style: .cancel,
                                                        handler: { (action) -> Void in
            self.blockUI(false)
            deletionAlertController.dismiss(animated: true)
        }))
        
        //Show alert
        present(deletionAlertController, animated: true)
    }
    
    //MARK: - Screen Management
    override var shouldAutorotate: Bool {
        //Prevent the screen from auto rorataion
        return false
    }
    
    func blockUI(_ shouldBlockUI: Bool) {
        if shouldBlockUI {
            //Block buttons
            cancelButton.isEnabled = false
            updateButton.isEnabled = false
            addAccountPictureButton.isEnabled = false
            deleteAccountButton.isEnabled = false
            //Block switch
            isTrainerSwitch.isEnabled = false
            //Show activity indicator
            activityIndicator.startAnimating()
            activityIndicator.isHidden = false
            //Block TextFields
            firstNameTextField.isEnabled = false
            lastNameTextField.isEnabled = false
            emailTextField.isEnabled = false
            passwordTextField.isEnabled = false
            confirmPasswordTextField.isEnabled = false
            //Dismiss keyboard
            firstNameTextField.resignFirstResponder()
            lastNameTextField.resignFirstResponder()
            emailTextField.resignFirstResponder()
            passwordTextField.resignFirstResponder()
            confirmPasswordTextField.resignFirstResponder()
        } else {
            //Unblock buttons
            cancelButton.isEnabled = true
            updateButton.isEnabled = true
            addAccountPictureButton.isEnabled = true
            deleteAccountButton.isEnabled = true
            //Block switch
            isTrainerSwitch.isEnabled = true
            //Show activity indicator
            activityIndicator.stopAnimating()
            activityIndicator.isHidden = true
            //Block TextFields
            firstNameTextField.isEnabled = true
            lastNameTextField.isEnabled = true
            emailTextField.isEnabled = true
            passwordTextField.isEnabled = true
            confirmPasswordTextField.isEnabled = true
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //Makes TextFields dismiss the keyboard when something else is
        //tapped
        firstNameTextField.resignFirstResponder()
        lastNameTextField.resignFirstResponder()
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        confirmPasswordTextField.resignFirstResponder()
    }
    
    //MARK: - Custom methods
    func fillOutForm() {
        //Fill out form with existing data from account
        guard let firstName = firstName else { return }
        guard let lastName = lastName else { return }
        guard let email = email else { return }
    
        firstNameTextField.text = firstName
        lastNameTextField.text = lastName
        emailTextField.text = email
        
        if isTrainer {
            isTrainerSwitch.isOn = true
        } else {
            isTrainerSwitch.isOn = false
        }
    }
    
    func showRecentLoginRequiredAlert() {
        //Notify the user that they need to log in recently
        let alert = UIAlertController(title: "Recent log in required",
                                      message: "This operation is sensitive and requires recent authentication. Please log in again",
                                      preferredStyle: .actionSheet)
        
        //Login action
        alert.addAction(UIAlertAction(title: "Login",
                                      style: .default,
                                      handler: { (action) -> Void in
            let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
            self.present(viewcontroller!, animated: true)
        }))
        
        //Cancel action
        alert.addAction(UIAlertAction(title: "Cancel",
                                      style: .cancel,
                                      handler: { (action) -> Void in
            self.dismiss(animated: true)
        }))
        
        //Show alert
        self.present(alert, animated: true)
    }
    
}

//MARK: - Extensions
extension EditAccountViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //Move to the next TextField when filling out the form
        switch textField {
        case firstNameTextField:
            lastNameTextField.becomeFirstResponder()
        case lastNameTextField:
            emailTextField.becomeFirstResponder()
        case emailTextField:
            passwordTextField.becomeFirstResponder()
        case passwordTextField:
            confirmPasswordTextField.becomeFirstResponder()
        case confirmPasswordTextField:
            confirmPasswordTextField.resignFirstResponder()
        default:
            break
        }
        
        return true
    }
    
}

extension EditAccountViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //Action is here because it requires these extensions
    
    @IBAction func addAccountPictureTapped(_ sender: UIButton) {
        //IBAction is located here because it requires these extensions
        
        //Delete the current picture
        ImageUtil.deleteImageOnFirebaseStorage(storageReference: storageReference, button: addAccountPictureButton, userId: Auth.auth().currentUser?.uid ?? "")
        
        //Get new picture
        ImageUtil.getNewImageForAccount(from: self, sender: sender)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //Handle the image picked by the user
        guard let userPictureRetrieved = info[.editedImage] as? UIImage else {
            print("ERROR: No image retrieved")
            return
        }
        
        userPicture = userPictureRetrieved
        
        picker.dismiss(animated: true) {
            //Display picture to the user on the button
            self.addAccountPictureButton.contentMode = .scaleToFill
            self.addAccountPictureButton.setImage(self.userPicture, for: .normal)
            self.addAccountPictureButton.layer.cornerRadius = self.addAccountPictureButton.frame.size.width / 2
            self.addAccountPictureButton.clipsToBounds = true
        }
    }
    
}
