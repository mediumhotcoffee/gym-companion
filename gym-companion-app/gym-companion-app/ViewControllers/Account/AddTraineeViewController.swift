//
//  AddTraineeViewController.swift
//  gym-companion-app
//
//  Created by Marcos Felipe Costa da Silva on 7/15/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import AVFoundation

class AddTraineeViewController: UIViewController {

    //MARK: - Views
    //Buttons
    @IBOutlet weak var findButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var scanQrCodeButton: UIButton!
    //ActivityIndicator
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    //TextField
    @IBOutlet weak var traineeEmailTextField: UITextField!
    
    //MARK: - Variables
    var trainerId: String?
    var handle: AuthStateDidChangeListenerHandle?
    var trainerEmail: String?
    
    var traineeID: String?
    
    //MARK: - Firebase Varibles
    var databaseReference: DatabaseReference!
    
    //MARK: - Lifecycle Methods
    override func viewWillAppear(_ animated: Bool) {
        //Add log in status listener
        handle = Auth.auth().addStateDidChangeListener({ (auth, user) in
            guard let user = user else { return }
            
            //Get the trainer's email
            self.trainerEmail = user.email
        })
        
        //Get database reference
        self.databaseReference = Database.database().reference()
    }
    
    override func viewDidLoad() {
        //Do any additional setup after loading the view
        super.viewDidLoad()
    }

    override func viewWillDisappear(_ animated: Bool) {
        //Remove log in status listener
        Auth.auth().removeStateDidChangeListener(handle!)
    }
    
    //MARK: - Screen Management
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //Make TextField dismiss the keyboard when something else is
        //tapped
        traineeEmailTextField.resignFirstResponder()
    }
    
    func blockUI(_ shouldBlockUI: Bool) {
        if shouldBlockUI {
            //Block buttons
            cancelButton.isEnabled = false
            findButton.isEnabled = false
            scanQrCodeButton.isEnabled = false
            //Block text field
            traineeEmailTextField.isEnabled = false
            //Show ActivityIndicator
            activityIndicator.startAnimating()
            activityIndicator.isHidden = false
        } else {
            //Unblock buttons
            cancelButton.isEnabled = true
            findButton.isEnabled = true
            scanQrCodeButton.isEnabled = true
            //Unblock text field
            traineeEmailTextField.isEnabled = true
            //Hide ActivityIndicator
            activityIndicator.stopAnimating()
            activityIndicator.isHidden = true
        }
    }
    
    //MARK: - Actions
    @IBAction func cancelTapped(_ sender: UIButton) {
        //Go back to the previous screen
        dismiss(animated: true)
    }
    
    @IBAction func findTapped(_ sender: UIButton) {
        //Try to get email and validate it
        let f = FormValidationUtil(formViewController: self, emailTextField: traineeEmailTextField)
        let emailToFind = f.emailExtractor()
        
        //Block UI
        blockUI(true)
        
        //Try to find the user on the database
        self.databaseReference.child("users").observeSingleEvent(of: .value, with: { (snapshot) in
            let numOfUsers: Int = snapshot.children.allObjects.count - 1
            var currentUserBeingChecked = 0
            
            //Get users
            for case let child as DataSnapshot in snapshot.children {
                currentUserBeingChecked += 1
                
                guard let dict = child.value as? [String:Any] else {
                    print("ERROR")
                    return
                }
                
                guard let firstName = dict["firstName"] as? String else { return }
                guard let lastName = dict["lastName"] as? String else { return }
                guard let email = dict["email"] as? String else { return }
                
                if emailToFind == self.trainerEmail {
                    //Notify trainer that they can't add themselves as trainees
                    let alert = UIAlertController(title: NSLocalizedString("cannot-add-self", comment: ""),
                                                  message: NSLocalizedString("you-cannot-add-yourself-as-a-trainee", comment: ""),
                                                  preferredStyle: .alert)
                    
                    //OK action
                    alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""),
                                                  style: .default,
                                                  handler: { (action) -> Void in
                        //Restore the UI so the trainer can search again
                        self.blockUI(false)
                    }))
                    
                    //Show alert
                    self.present(alert, animated: true)
                    
                    //Stop the loop
                    break
                } else if emailToFind == email {
                    //If a user with that email is found
                    
                    //Get the user's ID
                    let userID = child.key
                    
                    //Confirm if the trainer wants to add the trainee
                    let userFoundAlertController = UIAlertController(title: NSLocalizedString("found", comment: ""),
                                                                     message: "Would you like to add \(firstName) \(lastName) as a trainee?",
                                                                     preferredStyle: .alert)
                    
                    //OK button
                    let okButton = UIAlertAction(title: NSLocalizedString("ok", comment: ""),
                                                 style: .default,
                                                 handler: { (action) -> Void in
                        //Add trainee to trainer
                        //TODO: Unwind segue to immediatelly update the AccountVC
                        self.databaseReference.child("users/\(userID)").updateChildValues(["trainerID": self.trainerId!])
                        
                        self.traineeID = userID
                        self.performSegue(withIdentifier: Segue.toPassBackTrainee.stringValue, sender: sender)
                    })
                    userFoundAlertController.addAction(okButton)
                    
                    //Cancel action
                    userFoundAlertController.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""),
                                                                     style: .cancel,
                                                                     handler: { (action) -> Void in
                        //Reenable UI
                        self.blockUI(false)
                    }))
                    
                    self.present(userFoundAlertController, animated: true)
                } else if currentUserBeingChecked == numOfUsers {
                    //Final user being checked
                    
                    //Notify trainer that no trainee with that email was found
                    let userNotFoundAlertController = UIAlertController(title: NSLocalizedString("not-found", comment: ""),
                                                                        message: NSLocalizedString("no-user-with-this-email-was-found", comment: ""),
                                                                        preferredStyle: .alert)
                    
                    //OK action
                    userNotFoundAlertController.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""),
                                                                        style: .default,
                                                                        handler: { (action) -> Void in
                        //Restore the UI so the trainer can search again
                        self.blockUI(false)
                    }))
                    
                    //Show alert
                    self.present(userNotFoundAlertController, animated: true)
                } else {
                    //Continue to the next user
                    continue
                }
            }

            //If no email is entered, reenable UI
            self.blockUI(false)
        }) { (error) in
            //TODO: Handle error better
            print(error.localizedDescription)
        }
    }
 
    @IBAction func scanQrCodeTapped(_ sender: UIButton) {
        //Try to get camera permission and open QR code scanner
        proceedWithCameraAccess(identifier: Segue.toQrCodeScannerSegue.stringValue)
    }
    
    //MARK: - Navigation
    @IBAction func unwindAfterQrCodeIsFound(segue: UIStoryboardSegue) {
        if segue.source is QrCodeScannerViewController {
            if let source = segue.source as? QrCodeScannerViewController {
                //Do something once user ID is received from scanner
                
                //Check if user exists
                guard let userId = source.userId else { return }
                
                databaseReference.child("users").child(userId).observeSingleEvent(of: .value, with: { (snapshot) in
                    //Verify if user is in Firebase Realtime Database
                    let value = snapshot.value as? NSDictionary
                    guard let firstName = value?["firstName"] as? String, let
                        lastName = value?["lastName"] as? String else {
                            //If user is not on database alert the trainer
                            let alert = UIAlertController(title: NSLocalizedString("user-not-found", comment: ""),
                                                          message: NSLocalizedString("we-could-not-find-a-user-with-this-qr-code", comment: ""),
                                                          preferredStyle: .alert)
                            
                            //OK action
                            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""),
                                                          style: .default))
                            
                            //Show alert
                            self.present(alert, animated: true)
                            
                            return
                    }
                    
                    //If trainee exists, add to trainer
                    
                    //Confirm if the trainer wants to add the trainee
                    let userFoundAlertController = UIAlertController(title: NSLocalizedString("found", comment: ""),
                                                                     message: "Would you like to add \(firstName) \(lastName) as a trainee?",
                                                                     preferredStyle: .alert)
                    
                    //OK action
                    userFoundAlertController.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""),
                                                                     style: .default,
                                                                     handler: { (action) -> Void in
                        //Add trainee to trainer
                        self.databaseReference.child("users/\(userId)").updateChildValues(["trainerID": self.trainerId!])
                        
                        self.dismiss(animated: true)
                    }))
                    
                    //Cancel action
                    userFoundAlertController.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""),
                                                                     style: .cancel,
                                                                     handler: { (action) -> Void in
                                                                        //Reenable UI
                                                                        self.blockUI(false)
                    }))
                    
                    self.present(userFoundAlertController, animated: true, completion: nil)
                }) { (error) in
                    //TODO: Handle error better
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    //MARK: - Custom Methods
    func proceedWithCameraAccess(identifier: String) {
        //Handler in .requestAccess is needed to process user's answer to
        //permission request
        AVCaptureDevice.requestAccess(for: .video) { success in
            if success {
                //If request is granted
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: identifier, sender: nil)
                }
            } else {
                //If request is denied
                
                //Create Alert
                let alert = UIAlertController(title: NSLocalizedString("camera-permission-needed", comment: ""),
                                              message: NSLocalizedString("camera-access-is-necessary-to-use-the-qr-code-scanner", comment: ""),
                                              preferredStyle: .alert)
                
                //Ok action
                alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""),
                                              style: .default,
                                              handler: { action in
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                }))
                
                //Show alert
                self.present(alert, animated: true)
            }
        }
    }
    
}
