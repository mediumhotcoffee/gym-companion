//
//  UpgradeAccountViewController.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/23/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import PassKit

class UpgradeAccountViewController: UIViewController {
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var paymentButton: PKPaymentButton?
    let supportedNetworks: [PKPaymentNetwork] = [.amex, .discover, .masterCard, .visa]
    
    var currentUserIsTrainer: Bool!
    var paid: Bool!
    
    var paymentSuccess: Bool = false
    
    override func viewDidLoad() {
        //Do any additional setup after loading the view
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if paid {
            showPaidUI()
        } else if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: supportedNetworks) {
            setDescriptionMessage()
            setUpApplePayButton()
        } else {
            showNoApplePayUI()
        }
    }
    
    func getPaymentRequest() -> PKPaymentRequest {
        let request: PKPaymentRequest = PKPaymentRequest()
        request.countryCode = "US"
        request.currencyCode = "USD"
        request.merchantIdentifier = "merchant.com.helix.fit-plus-app-dev"
        request.merchantCapabilities = [.capability3DS, .capabilityCredit, .capabilityDebit]
        request.supportedNetworks = supportedNetworks
        request.paymentSummaryItems = [PKPaymentSummaryItem(label: "Subscription", amount: NSDecimalNumber(string: "24.99"))]
        
        return request
    }
    
    func setDescriptionMessage() {
        if currentUserIsTrainer {
            showTrainerUpgradeUI()
        } else {
            showTraineeUpgradeUI()
        }
    }
    
    func setUpApplePayButton() {
        paymentButton = PKPaymentButton(paymentButtonType: .checkout, paymentButtonStyle: .black)
        
        if let button = paymentButton {
            
            button.addTarget(self, action: #selector(checkoutTapped(_:)), for: .touchUpInside)
            
            self.view.addSubview(button)
            
            button.translatesAutoresizingMaskIntoConstraints = false
            
            let paymentButtonCenterXConstraint = button.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
            let paymentButtonBottomConstraint = button.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -60)
            let paymentButtonTrailingConstraint = button.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20)
            let paymentButtonLeadingConstraint = button.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20)
            let paymentButtonHeightConstraint = button.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.05)
            let paymentButtonTopConstraint = button.topAnchor.constraint(equalTo: descriptionTextView.bottomAnchor, constant: 15)
            
            self.view.addConstraints([
                paymentButtonTopConstraint,
                paymentButtonBottomConstraint,
                paymentButtonLeadingConstraint,
                paymentButtonTrailingConstraint,
                paymentButtonCenterXConstraint,
                paymentButtonHeightConstraint
                ])
        }
    }
    
    private func showNoApplePayUI() {
        headerLabel.text = UpgradeAccountViewController.noApplePayHeader
        descriptionTextView.text = UpgradeAccountViewController.noApplePayDescription
    }
    
    private func showPaidUI() {
        headerLabel.text = UpgradeAccountViewController.paidHeader
        descriptionTextView.text = UpgradeAccountViewController.paidDescription
        
        if let button = paymentButton {
            UIView.animate(withDuration: 0.3, animations: {
                button.alpha = 0
            }) { (bool) in
                button.isHidden = true
            }
        }
    }

    private func showSuccessUI() {
        if let button = paymentButton {
            UIView.animate(withDuration: 0.3, animations: {
                button.alpha = 0
            }) { (bool) in
                button.isHidden = true
            }
            
            UIView.transition(with: headerLabel, duration: 0.3, options: .transitionCrossDissolve, animations: {
                self.headerLabel.text = UpgradeAccountViewController.successHeader
            })
            
            UIView.transition(with: descriptionTextView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                self.descriptionTextView.text = UpgradeAccountViewController.successDescription
            })
        }
    }
    
    private func showTraineeUpgradeUI() {
        
        descriptionTextView.text = UpgradeAccountViewController.traineeDescription
    }
    
    private func showTrainerUpgradeUI() {
        
        descriptionTextView.text = UpgradeAccountViewController.trainerDescription
    }
}

//MARK: - Extensions
extension UpgradeAccountViewController {
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    @objc func checkoutTapped(_ sender: PKPaymentButton) {
        let request: PKPaymentRequest = getPaymentRequest()
        
        if let viewController = PKPaymentAuthorizationViewController(paymentRequest: request) {
            viewController.delegate = self
            present(viewController, animated: true)
        }
    }
    
}

extension UpgradeAccountViewController: PKPaymentAuthorizationViewControllerDelegate {
    
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        // Not sending transaction anywhere. Just faking success for demo.
        paymentSuccess = true
        FirebaseAccountsUtil.upgradePaidStatus(paid: paymentSuccess)
        completion(PKPaymentAuthorizationResult(status: .success, errors: []))
    }
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        
        paymentAuthorizationViewController(controller, didAuthorizePayment: PKPayment()) { (PKPaymentAuthorizationResult) in
            self.paymentSuccess = true
            FirebaseAccountsUtil.upgradePaidStatus(paid: self.paymentSuccess)
        }
        
        if paymentSuccess {
            // Update UI to reflect success.
            showSuccessUI()
        }
        
        // Dismiss the bottom sheet when transaction completes.
        dismiss(animated: true)
    }
    
}

extension UpgradeAccountViewController {
    
    static let traineeDescription: String = "Upgrading your trainee account is a monthly subscription that removes all limits from your account. You will no longer be restricted to only 3 routines and 1 gym bag.\r\n\r\nAdditionally, you will be able to add as many exercises to a routine as you like, as well as add an unlimited amount of items to a gym bag."
    static let trainerDescription: String = "Upgrading your trainer account is a monthly subscription of $24.99 that removes all limits from your account. You will no longer be restricted to only 3 trainees.\r\n\r\nAdditionally, you will be able to add as many exercises to a routine as you like, as well as add an unlimited amount of items to a gym bag."
    static let successHeader: String = "Success!"
    static let successDescription: String = "Your payment was successful. All limits have been lifted from your account."
    static let noApplePayHeader: String = "Apple Pay Not Available"
    static let noApplePayDescription: String = "Apple Pay is required to upgrade your account."
    static let paidHeader: String = "You Rock"
    static let paidDescription: String = "Your account is already upgraded. Thank you for using our app!"
    
}
