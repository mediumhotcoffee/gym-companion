//
//  LoginViewController.swift
//  gym-companion-app
//
//  Created by Marcos Silva on 6/26/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import FirebaseAuth
import LocalAuthentication

class LoginViewController: UIViewController {

    //MARK: - Views
    //TextFields
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    //Buttons
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var biometricIdButton: UIButton!
    //ActivityIndicator
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: - Variables
    var passwordToSend: String?
    
    //MARK: - Lifecycle Methods
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        isTouchIdOrFaceIdDevice()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //Setup delegates for TextFields
        (emailTextField.delegate, passwordTextField.delegate) = (self, self)
        
//        //Show proper biometric ID button
//        isTouchIdOrFaceIdDevice()
        
//        //Open keyboard for email TextField
//        emailTextField.becomeFirstResponder()
    }
    
    //MARK: - Actions
    @IBAction func cancelTapped(_ sender: UIButton) {
        //Go back to previous screen
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        //Try to log the user in
        
        //Block the UI so the user can't call the operation multiple times
        blockUI(true)
        
        //Try to extract email and password
        let formValidationUtil = FormValidationUtil(formViewController: self, emailTextField: emailTextField, passwordTextField: passwordTextField)
        
        //Try to get email
        guard let email = formValidationUtil.emailExtractor() else {
            blockUI(false)
            return
        }
        
        //Try to get password
        guard let password = formValidationUtil.passwordExtractor() else {
            blockUI(false)
            return
        }
        
        //Try to log the user in through FirebaseAuth
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if let error = error {
                //Handle FirebaseAuth errors
                let firebaseAuthValidationUtil = FirebaseAuthValidationUtil(formViewController: self)
                firebaseAuthValidationUtil.notifyOfFirebaseAuthError(error)
                
                //Reenable the UI if an error occurs
                self.blockUI(false)
                
                return
            }

            //Set the password to send
            self.passwordToSend = password
            
            //If log in was successful, segue back
            self.performSegue(withIdentifier: Segue.unwindAfterUserIsLoggedIn.stringValue, sender: self)
        }
    }
    
    @IBAction func biometricAuthenticationTapped(_ sender: UIButton) {
        let context = LAContext()
        var error: NSError?
        
        //Can we use biometric authentication
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            //Create message for TouchID users (FaceID message is defined on
            //plist file)
            let reason = "Fit+ needs FaceID to log you in."
            
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { [weak self] success, authenticationError in
                DispatchQueue.main.async {
                    if success {
                        //Load email saved in Keychain
                        if let email = KeychainWrapper.standard.string(forKey: "email") {
                            self?.emailTextField.text = email
                        }
                        
                        //Load password saved in Keychain
                        if let password = KeychainWrapper.standard.string(forKey: "password") {
                            self?.passwordTextField.text = password
                        }
                    } else {
                        //User did not pass TouchID/FaceID authentication
                        let alert = UIAlertController(title: NSLocalizedString("authentication-failed", comment: ""),
                                                      message: NSLocalizedString("you-could-not-be-verified", comment: ""),
                                                      preferredStyle: .alert)
                        
                        //OK action
                        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""),
                                                      style: .default))
                        
                        //Show alert
                        self?.present(alert, animated: true)
                    }
                }
            }
        } else {
            //No biometric unlocking available on the device
            let alert = UIAlertController(title: NSLocalizedString("authentication-not-available", comment: ""),
                                          message: NSLocalizedString("your-device-does-not-support-biometric-authentication", comment: ""),
                                          preferredStyle: .alert)
            
            //OK action
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""),
                                          style: .default))
            
            //Show alert
            present(alert, animated: true)
        }
    }
    
    //MARK: - Custom Methods
    func isTouchIdOrFaceIdDevice() {
        //Find out if device supports Face ID or TouchID
        let context = LAContext.init()
        var error: NSError?
        
        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            if context.biometryType == LABiometryType.faceID {
                //Device has FaceID
                let image = UIImage(named: "faceID")?.withRenderingMode(.alwaysTemplate)
                biometricIdButton.setImage(image, for: .normal)
                biometricIdButton.tintColor = UIColor.red
            } else if context.biometryType == LABiometryType.touchID {
                //Device has TouchID
                let image = UIImage(named: "touchID")?.withRenderingMode(.alwaysTemplate)
                biometricIdButton.setImage(image, for: .normal)
                biometricIdButton.tintColor = UIColor.red
            }
            
            biometricIdButton.tintColor = UIColor.appDarkColor
        } else {
            //If user disabled biometric login, hide button
            biometricIdButton.isHidden = true
        }
    }
    
    //MARK: - Screen Management
    override var shouldAutorotate: Bool {
        return false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //Dismiss keyboard when clicking outside buttons or text fields
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    func blockUI(_ shouldBlockUI: Bool) {
        //Blocks or unblocks the UI
        if shouldBlockUI {
            //Block buttons
            loginButton.isEnabled = false
            cancelButton.isEnabled = false
            forgotPasswordButton.isEnabled = false
            //Show activity indicator
            activityIndicator.startAnimating()
            activityIndicator.isHidden = false
            //Dismiss keyboard
            emailTextField.resignFirstResponder()
            passwordTextField.resignFirstResponder()
        } else {
            //Block buttons
            loginButton.isEnabled = true
            cancelButton.isEnabled = true
            forgotPasswordButton.isEnabled = true
            //Hide activity indicator
            activityIndicator.stopAnimating()
            activityIndicator.isHidden = true
        }
    }
    
}

//MARK: - Extensions
extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            //Move to the PasswordTextField when hitting return
            passwordTextField.becomeFirstResponder()
        } else {
            //Try to log the user in
            loginTapped(textField)
            textField.resignFirstResponder()
        }
        
        return true
    }
    
}
