//
//  SettingsViewController.swift
//  gym-companion-app
//
//  Created by Marcos Felipe Costa da Silva on 7/26/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import LocalAuthentication
import FirebaseAuth

class SettingsViewController: UIViewController {

    //MARK: - Variables
    //Firebase
    var handle: AuthStateDidChangeListenerHandle?
    var userId: String?
    var firebaseUser: FirebaseAuth.User?
    //Keychain
    var passwordReceived: String?
    
    //MARK: - Views
    
    //MARK: - Lifecycle Methods
    override func viewWillAppear(_ animated: Bool) {
        //Add log in status listener
        handle = Auth.auth().addStateDidChangeListener({ (auth, user) in
            //Verify that a user exists/is logged in
            guard let user = user else {
                //Ask to open the LogInViewController if the user is not logged
                //in
                
                return
            }
            
            //Get user reference
            self.firebaseUser = user
            
            //Load user info if they are logged in
            self.userId = user.uid
        })
    }
    
    override func viewDidLoad() {
        //Do any additional setup after loading the view
        super.viewDidLoad()

        //TODO: Remove this test
        //FIXME: Create a check for a login
        //print("passwordReceived: \(passwordReceived)")
    }
    
    //MARK: - Actions
    @IBAction func setReminderTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func enableBiometricAuthValueChanged(_ sender: UISwitch) {
        if sender.isOn {
            let context = LAContext()
            var error: NSError?
            
            //Can we use biometric authentication
            if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
                //Create message for TouchID users (FaceID message is defined on
                //plist file)
                let reason = NSLocalizedString("fit+-needs-touchid", comment: "")

                context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { [weak self] success, authenticationError in
                    DispatchQueue.main.async {
                        if success {
                            //Save credentials to Keychain
                            if let user = self?.firebaseUser, let email = user.email, let password = self?.passwordReceived {
                                KeychainWrapper.standard.set(email, forKey: "email")
                                KeychainWrapper.standard.set(password, forKey: "password")
                            }
                            
                            var message: String? = NSLocalizedString("you-can-now-log-in", comment: "")
                            
                            if context.biometryType == .faceID {
                                message = NSLocalizedString("you-can-now-log-in-with-faceid", comment: "")
                            } else if context.biometryType == .touchID {
                                message = NSLocalizedString("you-can-now-log-in-with-touchid", comment: "")
                            }
                            
                            let alert = UIAlertController(title: NSLocalizedString("credentials-saved", comment: ""),
                                                          message: message,
                                                          preferredStyle: .alert)
                            
                            //OK action
                            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""),
                                                          style: .default))
                            
                            self?.present(alert, animated: true)
                        } else {
                            //User did not pass TouchID/FaceID authentication
                            let alert = UIAlertController(title: NSLocalizedString("authentication-failed", comment: ""),
                                message: NSLocalizedString("you-could-not-be-verified", comment: ""),
                                preferredStyle: .alert)
                            
                            //OK action
                            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default))
                            
                            self?.present(alert, animated: true)
                        }
                    }
                }
            } else {
                //No biometric unlocking available on the device
                let alert = UIAlertController(title: "Authentication Not Available",
                                              message: NSLocalizedString("your-device-does-not-support-biometric-authentication", comment: ""),
                                              preferredStyle: .alert)
                
                //OK action
                alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default))
                present(alert, animated: true)
            }
        }
    }

    @IBAction func doneTapped(_ sender: UIButton) {
        //Go back the previous screen
        dismiss(animated: true)
    }
    
}
