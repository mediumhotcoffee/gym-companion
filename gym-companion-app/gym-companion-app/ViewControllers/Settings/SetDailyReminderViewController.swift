//
//  SetDailyReminderViewController.swift
//  gym-companion-app
//
//  Created by Marcos Felipe Costa da Silva on 7/26/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit
import UserNotifications

class SetDailyReminderViewController: UIViewController {

    //MARK: - Variables
    //Time
    let date = Date()
    let calendar = Calendar.current
    var hourToSave: Int?
    var minutesToSave: Int?
    //Notification center
    var notificationCenter: UNUserNotificationCenter?
    
    //MARK: - Views
    
    //MARK: - Lifecycle Methods
    override func viewDidLoad() {
        //Do any additional setup after loading the view
        super.viewDidLoad()
        
        //Set current time as the time for the notification
        hourToSave = calendar.component(.hour, from: date)
        minutesToSave = calendar.component(.minute, from: date)
        
        //Setup notification center
        notificationCenter = UNUserNotificationCenter.current()

    }
    
    //MARK: - Actions
    @IBAction func reminderTimeChanged(_ sender: UIDatePicker) {
        notificationCenter?.requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            if !granted {
                //Permission was denied
                let alert = UIAlertController(title: NSLocalizedString("permission-denied", comment: ""),
                                              message: NSLocalizedString("permission-for-notifications-was-denied", comment: ""),
                                              preferredStyle: .alert)
                
                //Settings action
                alert.addAction(UIAlertAction(title: NSLocalizedString("settings", comment: ""),
                                              style: .default,
                                              handler: { (action) in
                    //Open the Settings for the app
                    if let appSettings = URL(string: UIApplication.openSettingsURLString) {
                        UIApplication.shared.open(appSettings, options: [:], completionHandler: nil)
                    }
                }))
                
                //Cancel
                alert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""),
                                              style: .cancel))
                
                self.present(alert, animated: true, completion: nil)
                
                return
            }
        }
        
        //Get time for the notification
        let date = sender.date
        let calendar = Calendar.current
        hourToSave = calendar.component(.hour, from: date)
        minutesToSave = calendar.component(.minute, from: date)
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        //Go back to the previous screen
        dismiss(animated: true)
    }
    
    @IBAction func saveTapped(_ sender: UIButton) {
        if let hourToSave = hourToSave, let minutesToSave = minutesToSave, let notificationCenter = notificationCenter  {
            notificationCenter.removeAllDeliveredNotifications()
            
            //Set the content for the notification
            let content = UNMutableNotificationContent()
            content.title = NSLocalizedString("gym-reminder", comment: "")
            content.body = NSLocalizedString("time-to-go-to-the-gym", comment: "")
            content.categoryIdentifier = "alarm"
            content.sound = UNNotificationSound.default
            
            //Set time
            var dateComponents = DateComponents()
            dateComponents.hour = hourToSave
            dateComponents.minute = minutesToSave
            
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)

            //To test, uncomment this line
            //let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
            
            let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
            
            //Set notification
            notificationCenter.add(request) { (error) in
                if let error = error {
                    print(error.localizedDescription)
                }
            }
            
            dismiss(animated: true)
        }
    }
    
    @IBAction func deleteReminderTapped(_ sender: UIButton) {
        notificationCenter?.removeAllDeliveredNotifications()
        notificationCenter?.removeAllPendingNotificationRequests()
        
        //Go back to previous screen
        dismiss(animated: true)
    }
    
}
