//
//  PullRoutineExercisesDelegate.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/30/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

protocol PullRoutineExercisesDelegate {
    
    func routineExerciseObjectsPulled(routine: Routine, exercises: [Exercise], browseObjectType: BrowseObjectType, totalToPull: Int)
}
