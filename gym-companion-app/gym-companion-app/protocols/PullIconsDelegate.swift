//
//  PullIconsDelegate.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/15/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

protocol PullIconsDelegate {
    
    func iconLocationsPulled(iconLocations: [String])
    func iconImagesPulled(_ iconImage: (key: String, image: UIImage))
    func singleIconImagePulled(iconImage: UIImage)
}
