//
//  ShareableCellDelegate.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/25/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

protocol TableViewCellObjectDelegate {
    
    func objectSelected(object: Any, objectType: BrowseObjectType)
    func objectSelected(indexPath: IndexPath)
    func shareTapped(_ sender: UIButton, indexPath: IndexPath)
    func cellDeleted(indexPath: IndexPath)
}
