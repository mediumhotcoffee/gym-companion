//
//  HeaderBarDelegate.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 6/26/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

protocol HeaderBarDelegate {
    
    func accountTapped(_ sender: UIButton)
}
