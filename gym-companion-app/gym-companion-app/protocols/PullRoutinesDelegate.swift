//
//  PullRoutinesDelegate.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/17/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

protocol PullRoutinesDelegate {
    
    func routineIDsPulled(routineIDs: [String])
    func routineObjectsPulled(routines: [Routine], browseObjectType: BrowseObjectType, totalToPull: Int)
}
