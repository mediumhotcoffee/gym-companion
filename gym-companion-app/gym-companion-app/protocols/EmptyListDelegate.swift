//
//  EmptyListAddButtonDelegate.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 6/25/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

protocol EmptyListDelegate {
    
    func showAddOptions(_ sender: UIButton)
}
