//
//  SaveRoutineDelegate.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/18/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

protocol SaveRoutineDelegate {
    
    func savedRoutineToUserRoot(_ saved: Bool)
    func savedRoutineToRoutinesRoot(_ saved: Bool)
    func routineUpdated(_ saved: Bool)
    func savedRoutineLocally(_ saved: Bool)
}
