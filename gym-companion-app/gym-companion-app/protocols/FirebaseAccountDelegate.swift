//
//  FirebaseAuthDelegate.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/25/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

protocol FirebaseAccountDelegate {
    
    func userPulled(user: User, current: Bool)
    func signInSelected(_ sender: UIAlertAction)
    func upgradeTapped(sender: UIAlertAction)
}
