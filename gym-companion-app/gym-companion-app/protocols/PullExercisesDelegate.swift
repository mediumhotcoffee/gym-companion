//
//  PullExercisesDelegate.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/18/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

protocol PullExercisesDelegate {
    
    func exerciseIDsPulled(exerciseIDs: [String])
    func exerciseObjectsPulled(exercises: [Exercise], browseObjectType: BrowseObjectType, totalToPull: Int)
}
