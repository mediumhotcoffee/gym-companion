//
//  SaveExerciseDelegate.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/15/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

protocol SaveExerciseDelegate {
    
    func savedExerciseToExerciseRoot(exercise: Exercise, _ saved: Bool)
    func savedExerciseToUserRoot(_ saved: Bool)
    func updatedExercise(_ updated: Bool)
}
