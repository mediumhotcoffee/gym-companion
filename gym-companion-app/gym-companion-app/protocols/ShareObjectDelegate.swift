//
//  ShareObjectDelegate.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/26/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

protocol ShareObjectDelegate {
    
    func sent(_ sent: Bool, object: Any, to user: User?)
}
