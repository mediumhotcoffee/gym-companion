//
//  UITableViewActionSheetDelegate.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/14/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

protocol UITableViewActionSheetDelegate {
    
    func addToList(selectedIndexPaths: [IndexPath])
}
