//
//  SaveGymBagDelegate.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/12/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

protocol SaveGymBagDelegate {
    
    func savedBagToUserRoot(_ saved: Bool, gymBag: GymBag)
    func savedBagToGymBagsRoot(_ saved: Bool, gymBag: GymBag)
    func bagUpdated(_ updated: Bool, gymBag: GymBag)
    func savedBagLocally(_ saved: Bool, gymBag: GymBag)
}
