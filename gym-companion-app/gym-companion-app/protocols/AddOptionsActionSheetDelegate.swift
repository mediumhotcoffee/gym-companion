//
//  AddOptionsActionSheetDelegate.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/20/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

protocol AddOptionsActionSheetDelegate {
    
    func browseSelected(sender: UIButton)
    func createSelected(sender: UIButton)
}
