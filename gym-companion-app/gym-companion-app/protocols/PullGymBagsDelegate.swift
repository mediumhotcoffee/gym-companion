//
//  PullGymBagsDelegate.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/20/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

protocol PullGymBagsDelegate {
    
    func gymBagIDsPulled(gymBagIDs: [String])
    func gymBagObjectsPulled(gymBags: [GymBag], browseObjectType: BrowseObjectType, totalToPull: Int)
}
