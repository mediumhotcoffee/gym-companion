//
//  UITextFieldExtension.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 6/30/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

extension UITextField {
    
    // Found and modified from: https://medium.com/nyc-design/swift-4-add-icon-to-uitextfield-48f5ebf60aa1
    // Sets an icon for the textfield, the size of the icon, and a padding of about 10px, center aligned with 5px.
    // Use a 20x20 image.
    func setIcon(_ imageIcon: UIImage?) {
        
        // Make sure the imageIcon isn't nil.
        if let image = imageIcon {
            
            // Set the image for the icon.
            let iconView: UIImageView = UIImageView(frame:
                CGRect(x: 10, y: 5, width: 20, height: 20))
            iconView.image = image
            
            // Gives padding to the image.
            let iconContainerView: UIView = UIView(frame:
                CGRect(x: 20, y: 0, width: 30, height: 30))
            
            iconContainerView.addSubview(iconView)
            
            // Sets the leftView icon to the image and visible.
            leftView = iconContainerView
            leftViewMode = .always
        }
    }
}
