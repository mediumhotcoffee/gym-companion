//
//  UIColorExtension.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 6/27/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var appDarkColor: UIColor {
        
        get {
            
            return UIColor(red: 52/255, green: 73/255, blue: 94/255, alpha: 1)
        }
    }
    
    static var appGreen: UIColor {
        
        get {
            
            return UIColor(red: 46/255, green: 204/255, blue: 113/255, alpha: 1)
        }
    }
    
    static var appRed: UIColor {
        
        get {
            
            return UIColor(red: 231/255, green: 76/255, blue: 60/255, alpha: 1)
        }
    }
    
    static var appWhite: UIColor {
        
        get {
            
            return UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1)
        }
    }
    
    static var appTransparentWhite: UIColor {
        
        get {
            
            return UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 0.2)
        }
    }
}
