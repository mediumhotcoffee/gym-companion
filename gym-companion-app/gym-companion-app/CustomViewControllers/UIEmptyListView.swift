//
//  UIEmptyListView.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 6/25/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import UIKit

class UIEmptyListView: UIView {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    var delegate: EmptyListDelegate!
    
    // MARK: - Initializers
    
    // Allows to init view from code.
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    // Allows to init view from xib or storyboard.
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Custom Methods
    func setupView(listType: ListType) {
        var header: String
        var message: String
        var button: String
        
        switch listType {
        case .routine:
            header = NSLocalizedString("no-routines-no-problem", comment: "")
            message = NSLocalizedString("add-routines-to-get-started", comment: "")
            button = NSLocalizedString("add-routine", comment: "")
        case .exercise:
            header = NSLocalizedString("no-exercises-no-problem", comment: "")
            message = NSLocalizedString("add-exercises-to-get-started", comment: "")
            button = NSLocalizedString("add-exercise", comment: "")
        case .bag:
            header = NSLocalizedString("no-bags-no-problem", comment: "")
            message = NSLocalizedString("add-bags-to-get-started", comment: "")
            button = NSLocalizedString("add-bag", comment: "")
        case .bagItem:
            header = NSLocalizedString("no-bag-items-no-problem", comment: "")
            message = NSLocalizedString("add-bag-items-to-get-started", comment: "")
            button = NSLocalizedString("add-bag-item", comment: "")
        case .trainees:
            print("AlertHelper.swift | getAddOptionsActionSheet(listType, sender, delegate)\rError: Unexpected list type. \(listType.stringValue) shouldn't be passed here.")
            header = ""
            message = ""
            button = ""
        }
        
        headerLabel.text = header
        messageLabel.text = message
        addButton.setTitle(button, for: .normal)
    }
    
    // Returns a UIEmptyListView from the custom nib file.
    static func emptyViewFromNib() -> UIEmptyListView {
        // Returns a UIEmptyListView from the custom nib file.
        let nib = UINib(nibName: "UIEmptyListView", bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as! UIEmptyListView
    }
    
    // MARK: - Actions
    @IBAction func addTapped(_ sender: UIButton) {
        //TODO: Add some animation to the button
        delegate.showAddOptions(sender)
    }
}
