//
//  HiitSection.swift
//  gym-companion-app
//
//  Created by Marcos Felipe Costa da Silva on 7/19/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

enum HiitSection {
    case warmUp, lowIntensity, highIntensity, coolDown
}
