//
//  NibName.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/18/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

enum NibName {
    
    case collectionCell, shareableCollectionCell
    
    var stringValue: String {
        switch self {
        case .collectionCell: return "UICollectionObjectTableViewCell"
        case .shareableCollectionCell: return "UIShareableCollectionObjectTableViewCell"
        }
    }
}
