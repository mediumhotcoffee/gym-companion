//
//  Segue.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 6/26/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

enum Segue {
    case toCreateRoutine,
        toBrowseRoutines,
        toPassBackExercise,
        toPassBackBrowsableExercise,
        toCreateExercise,
        toBrowseExercises,
        toCreateGymBag,
        toBrowseGymBags,
        toRoutineTimer,
        toCreateBagItem,
        toBrowseBagItem,
        toPassBackBagItem,
        toPassBackBrowsableBagItem,
        toHiitTimerResult,
        toTimersUnwind,
        toHiitTimer,
        toSelectHiitTimes,
        toSelectRoutineTimer,
        toAddTraineeSegue,
        toShowQrCodeSegue,
        toEditAccountSegue,
        toUpgradeAccount,
        unwindToAddTraineeSegue,
        toPassBackTrainee,
        toWorkoutDetailsSegue,
        toQrCodeScannerSegue,
        toNewMeasurementSegue,
        unwindAfterUserIsLoggedIn,
        toSettingsSegue
    
    var stringValue: String {
        switch self {
        //Workout segues
        case .toCreateRoutine: return "segueToCreateRoutine"
        case .toBrowseRoutines: return "segueToBrowseRoutines"
        case .toPassBackExercise: return "segueToPassBackExercise"
        case .toPassBackBrowsableExercise: return "segueToPassBackBrowsableExercise"
        case .toCreateExercise: return "segueToCreateExercise"
        case .toBrowseExercises: return "segueToBrowseExercises"
        //Bag segues
        case .toCreateGymBag: return "segueToCreateGymBag"
        case .toBrowseGymBags: return "segueToBrowseGymBags"
        case .toCreateBagItem: return "segueToCreateBagItem"
        case .toBrowseBagItem: return "segueToBrowseGymBagItems"
        case .toPassBackBagItem: return "segueToPassBackBagItem"
        case .toPassBackBrowsableBagItem: return "segueToPassBackBrowsableBagItem"
        //Timer segues
        case .toRoutineTimer: return "segueToRoutineTimer"
        case .toSelectHiitTimes: return "toSelectHiitTimes"
        case .toSelectRoutineTimer: return "toSelectRoutineTimer"
        case .toHiitTimerResult: return "toHiitTimerResult"
        case .toHiitTimer: return "toHiitTimer"
        case .toTimersUnwind: return "toTimersUnwind"
        //Account segues
        case .toAddTraineeSegue: return "toAddTraineeSegue"
        case .toShowQrCodeSegue: return "toShowQrCodeSegue"
        case .toEditAccountSegue: return "toEditAccountSegue"
        case .unwindToAddTraineeSegue: return "unwindToAddTraineeSegue"
        case .toQrCodeScannerSegue: return "toQrCodeScannerSegue"
        case .unwindAfterUserIsLoggedIn: return "unwindAfterUserIsLoggedIn"
        case .toUpgradeAccount: return "segueToUpgradeAccount"
        case .toPassBackTrainee: return "segueToPassBackTrainee"
        //Stats segues
        case .toWorkoutDetailsSegue: return "toWorkoutDetailsSegue"
        case .toNewMeasurementSegue: return "toNewMeasurementSegue"
        case .toSettingsSegue: return "toSettingsSegue"
        }
    }
    
}
