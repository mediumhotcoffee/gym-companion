//
//  Identifier.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 6/26/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

enum Identifier {
    case accountViewController, reusableCell, collectionCell
    
    var stringValue: String {
        switch self {
        case .accountViewController: return "AccountViewController"
        case .reusableCell: return "cellID"
        case .collectionCell: return "collectionCellID"
        }
    }
    
}
