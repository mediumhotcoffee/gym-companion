//
//  ListType.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 6/26/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

enum ListType {
    case routine, exercise, bag, bagItem, trainees
    
    var stringValue: String {
        
        switch self {
        case .routine: return "routines"
        case .exercise: return "exercises"
        case .bag: return "bags"
        case .bagItem: return "bagItems"
        case .trainees: return "trainees"
        }
    }
}
