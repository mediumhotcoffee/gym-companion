//
//  MuscleGroup.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 6/27/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

enum WorkoutCategory: CaseIterable {
    
    case abs, arms, back, cardio, chest, legs, neck, shoulders
    
    var categoryName: String {
        
        switch self {
            
        case .abs: return "Abs"
        case .arms: return "Arms"
        case .back: return "Back"
        case .cardio: return "Cardio"
        case .chest: return "Chest"
        case .legs: return "Legs"
        case .neck: return "Neck"
        case .shoulders: return "Shoulders"
        }
    }
    
    static func getCategoryFromString(categoryString: String) -> WorkoutCategory? {
        
        switch categoryString {
        case "Abs": return .abs
        case "Arms": return .arms
        case "Back": return .back
        case "Cardio": return .cardio
        case "Chest": return .chest
        case "Legs": return .legs
        case "Neck": return .neck
        case "Shoulders": return .shoulders
        default:
            
            print("WorkoutCategory.swift | getCategoryFromString(): Error -> Invalid Enum String")
            return nil
        }
    }
}
