//
//  BrowseObjectType.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/17/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

enum BrowseObjectType {
    case user, browsable
}
