//
//  FirebaseRoots.swift
//  gym-companion-app
//
//  Created by Tommy Kent Olsen, Jr on 7/12/19.
//  Copyright © 2019 Helix. All rights reserved.
//

import Foundation

enum FirebaseDatabaseRoot {
    case users, routines, browsableRoutines, exercises, browsableExercises, gymBags, browsableBags
    
    var stringValue: String {
        switch self {
        case .users: return "users"
        case .routines: return "routines"
        case .browsableRoutines: return "browsableRoutines"
        case .exercises: return "exercises"
        case .browsableExercises: return "browsableExercises"
        case .gymBags: return "gymBags"
        case .browsableBags: return "browsableBags"
        }
    }
}
